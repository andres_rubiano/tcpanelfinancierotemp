
// endpoints
// Calidad: http://10.20.2.8:9062
// Produccion http://10.20.2.8:9050

export const TC_BASE_URL = 'http://192.168.0.9:7701/PanelServices';
export const API_URL = `${TC_BASE_URL}/CodegenServices/ServicioPanelTarifas.svc//JSON/`;
export const LIQ_URL = `${TC_BASE_URL}/CodegenServices/LiquidationService.svc/JSON/`;
export const FACT_URL = `${TC_BASE_URL}/CodegenServices/ServicioFacturacion.svc/JSON/`;
export const RATING_URL = `${TC_BASE_URL}/CodegenServices/RatingService.svc//JSON/`;
export const TC_URL  = `${TC_BASE_URL}/TC/Rating/frmServiceRatesPq.asp`;

// global parameters
export const PORCENTAJE_ANCHO = 2.085;
export const PORCENTAJE_ALTO = 1.55;
