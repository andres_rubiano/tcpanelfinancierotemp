import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'view401.component.html',
  styles: [`.no-access {
              min-height: 600px;padding: 0;
              vertical-align: middle;
              margin: 0 auto !important; 
              margin-top:50px !important
            }`]  
})
export class View401Component {

  constructor( ) { }

}
