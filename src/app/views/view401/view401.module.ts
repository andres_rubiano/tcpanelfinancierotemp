//core
import { NgModule } from '@angular/core';
//spx modulos
import { SharedModule } from 'app/shared.module';
import { View401RoutingModule } from './view401-routing.module';
//spx componentes
import { View401Component } from './view401.component';

@NgModule({
  imports: [
    View401RoutingModule,
    SharedModule
  ],
  declarations: [View401Component]
})
export class View401Module { }
