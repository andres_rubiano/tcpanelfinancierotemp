//core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//terceros
import { DxDataGridModule, DxDropDownBoxModule, DxTextBoxModule, DxListModule, DxLoadIndicatorModule, DxFormModule, DxPopupModule, DxAutocompleteModule, DxButtonModule } from 'devextreme-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//spx Modulos
import { SharedModule } from 'app/shared.module';
import { LiquidacionRoutingModule } from './liquidacion-routing.module';
//spx servicios
import { CostosService } from 'app/servicios/costos.service';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
//spx componentes
import { FiltroManifiestosComponent } from 'app/componentes-suppla/filtros/manifiestos/filtro-manifiestos.component';
import { TablaManifiestosComponent } from 'app/componentes-suppla/tablas/tabla-manifiestos/tabla-manifiestos.component';
import { DetalleViajePopupComponent } from 'app/componentes-suppla/popups/detalle-viaje/detalle-viaje-popup.component';
import { TablaDetalleViajeCostosComponent } from 'app/componentes-suppla/tablas/tabla-detalle-viaje-costos/tabla-detalle-viaje-costos.component';
import { TablaDetalleViajeIngresosComponent } from 'app/componentes-suppla/tablas/tabla-detalle-viaje-ingresos/tabla-detalle-viaje-ingresos.component';
import { TablaManifiestosSelecComponent } from 'app/componentes-suppla/tablas/tabla-manifiestos-selec/tabla-manifiestos-selec.component';
import { FiltroCostosComponent } from 'app/componentes-suppla/filtros/costos/filtro-costos.component';
import { TablaCostosComponent } from 'app/componentes-suppla/tablas/tabla-costos/tabla-costos.component';
import { TablaCostosSelecComponent } from 'app/componentes-suppla/tablas/tabla-costos-selec/tabla-costos-selec.component';
import { FiltroLiquidacionesComponent } from 'app/componentes-suppla/filtros/liquidaciones/filtro-liquidaciones.component';
import { TablaLiquidacionesComponent } from 'app/componentes-suppla/tablas/tabla-liquidaciones/tabla-liquidaciones.component';
import { TablaDetalleLiquidacionesComponent } from 'app/componentes-suppla/tablas/tabla-detalle-liquidaciones/tabla-detalle-liquidaciones.component';
import { CostosProveedorPopupComponent } from 'app/componentes-suppla/popups/costos-proveedor/costos-proveedor.popup';
import { TablaCostosProveedorComponent } from 'app/componentes-suppla/tablas/tabla-costos-proveedor/tabla-costos-proveedor.component';
import { FiltroCostosProveedorComponent } from 'app/componentes-suppla/filtros/costos-proveedor/filtro-costos-proveedor.component';
import { LiquidacionComponent } from './liquidacion.component';
import { TablaDistribucionCostosProveedorComponent } from 'app/componentes-suppla/tablas/tabla-distribucion-costos-proveedor/tabla-distribucion-costos-proveedor.component';
import { CrearNotaProvPopupComponent } from 'app/componentes-suppla/popups/crear-nota-prov/crear-nota-prov.popup';
import { TablaLiquidacionGeneralProvComponent } from 'app/componentes-suppla/tablas/tabla-liquidacion-general-prov/tabla-liquidacion-general-prov';
import { TablaDetalleLiquidacionesNotasComponent } from 'app/componentes-suppla/tablas/tabla-detalle-liquidaciones-notas/tabla-detalle-liquidaciones-notas.component';
import { TablaDetalleLiquidacionesDistriCostosComponent } from 'app/componentes-suppla/tablas/tabla-detalle-liquidaciones-distri-costos/tabla-detalle-liquidaciones-distri-costos.component';
import { TablaDetalleLiquidacionesDocPagoComponent } from 'app/componentes-suppla/tablas/tabla-detalle-liquidaciones-doc-pago/tabla-detalle-liquidaciones-doc-pago.component';

@NgModule({
  imports: [
    LiquidacionRoutingModule,
    NgbModule,
    FormsModule,
    DxListModule,
    DxPopupModule,
    DxDataGridModule,
    DxDropDownBoxModule,
    DxTextBoxModule,
    DxFormModule,
    DxLoadIndicatorModule,
    DxAutocompleteModule,
    DxButtonModule,
    CommonModule,
    SharedModule
  ],
  providers: [ManifiestosService, CostosService, LiquidacionesService, TablaDetalleLiquidacionesComponent],
  declarations: [LiquidacionComponent, FiltroManifiestosComponent, TablaManifiestosComponent,
    TablaManifiestosSelecComponent, DetalleViajePopupComponent, TablaDetalleViajeCostosComponent,
    TablaDetalleViajeIngresosComponent, FiltroCostosComponent, TablaCostosComponent,
    TablaCostosSelecComponent, FiltroLiquidacionesComponent, TablaLiquidacionesComponent,
    TablaDetalleLiquidacionesComponent, CostosProveedorPopupComponent, TablaCostosProveedorComponent,
    FiltroCostosProveedorComponent, CrearNotaProvPopupComponent, TablaDistribucionCostosProveedorComponent,
    TablaLiquidacionGeneralProvComponent, TablaDetalleLiquidacionesNotasComponent,
    TablaDetalleLiquidacionesDistriCostosComponent, TablaDetalleLiquidacionesDocPagoComponent]
})
export class LiquidacionModule { }
