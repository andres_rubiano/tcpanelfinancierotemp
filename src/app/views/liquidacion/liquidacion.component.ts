import { AuthenticacionService } from 'app/servicios/autenticacion.service';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { CostosService } from 'app/servicios/costos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { DxTabPanelComponent } from 'devextreme-angular';

@Component({
  templateUrl: 'liquidacion.component.html'
})
export class LiquidacionComponent implements OnInit {

  manifPorLiquidar: Array<any>;
  manifSeleccionados: Array<any>;
  costosPorLiquidar: Array<any>;
  costosSeleccionados: Array<any>;
  liqPorLiquidar: Array<any>;
  liqSeleccionadas: Array<any>;
  liqDetalle: Array<any>;
  liqGeneral: any;
  liqNum = '';
  tabDetalle: any;

  // tamaños tablas segun splitter
  widthTablaManifiestos = 0;
  widthTablaManifiestosSel = 0;
  widthTablaCostos = 0;
  widthTablaCostosSel = 0;
  widthTablaLiq = 0;
  widthTablaLiqDet = 0;
  widthTablaDocs = 0;
  widthTablaDist = 0;
  widthTablaNotas = 0;

  constructor(public as: AuthenticacionService, public rt: ActivatedRoute,
    public router: Router, public ms: ManifiestosService, public cs: CostosService,
    public ls: LiquidacionesService) {
    this.onInitializedtabDetalle = this.onInitializedtabDetalle.bind(this);
  }

  ngOnInit(): void {

    this.rt.params.subscribe(prms => {
      // console.log("prms['sesion']");
      this.as.ValidarSesion(prms['sesion']).then(res => {
        if (!res) {
          this.router.navigate(['401']);
          console.error('Acceso no autorizado');
          return;
        }

        this.ms.manifPorLiquidar.subscribe(val => {
          if (val.length === 0) {
            notify('No se encontraron manifiestos para estos criterios de busqueda!', 'Info', 2000);
          }
          this.manifPorLiquidar = val;
        });
        this.cs.costosPorLiquidar.subscribe(val => {
          if (val.length === 0) {
            notify('No se encontraron costos para estos criterios de busqueda!', 'Info', 2000);
          }
          this.costosPorLiquidar = val;
        });
        this.ls.liqPorLiquidar.subscribe(val => {
          if (val.length === 0) {
            notify('No se encontraron liquidaciones para estos criterios de busqueda!', 'Info', 2000);
          }
          this.liqPorLiquidar = val;
        });

        this.ls.detalleLiquidacionNum.subscribe(val => this.liqNum = val);
        this.ls.detalleLiquidacion.subscribe((detalle: any) => {
          this.liqGeneral = detalle;
          this.liqDetalle = detalle.DetallesLiquidacionDto;

          if (this.tabDetalle) {
            this.tabDetalle.option('selectedIndex', 0);
          }

        });
        this.ms.manifSeleccionados.subscribe(list => this.manifSeleccionados = list);
        this.cs.costosSeleccionados.subscribe(list => this.costosSeleccionados = list);
        this.ls.liqSeleccionadas.subscribe(list => this.liqSeleccionadas = list);
      });
    });
  }

  onInitializedtabDetalle(e) {
    this.tabDetalle = e.component;
  }

  CalcularTamanoManif(e) {
    if (e.primary > 0) {
      this.widthTablaManifiestos = e.primary - 10;
      this.widthTablaManifiestosSel = e.secondary - 10;
    }
  }
  CalcularTamanoCos(e) {
    if (e.primary > 0) {
      this.widthTablaCostos = e.primary - 10;
      this.widthTablaCostosSel = e.secondary - 10;
    }
  }
  CalcularTamanoLiq(e) {
    if (e.primary > 0) {
      this.widthTablaLiq = e.primary - 10;
      this.widthTablaLiqDet = this.widthTablaNotas = this.widthTablaDist = this.widthTablaDocs = e.secondary - 30;
      }
  }
}
