// core
import { NgModule } from '@angular/core';
// spx modulos
import { FacturacionRoutingModule } from './facturacion-routing.module';
// spx componentes
import { FacturacionComponent } from './facturacion.component';
import { SharedModule } from 'app/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DxListModule, DxPopupModule, DxFormModule, DxDataGridModule, DxDropDownBoxModule, DxTextBoxModule, DxLoadIndicatorModule, DxAutocompleteModule } from 'devextreme-angular';
import { CommonModule } from '@angular/common';
import { TablaGuiasComponent } from 'app/componentes-suppla/tablas/tabla-guias/tabla-guias.component';
import { FiltroGuiasComponent } from 'app/componentes-suppla/filtros/guias/filtro-guias.component';
import { TablaGuiasSelecComponent } from 'app/componentes-suppla/tablas/tabla-guias-selec/tabla-guias-selec.component';
import { FiltroRemesasComponent } from 'app/componentes-suppla/filtros/remesas/filtro-remesas.component';
import { FiltroFacturasComponent } from 'app/componentes-suppla/filtros/facturas/filtro-facturas.component';
import { TablaRemesasComponent } from 'app/componentes-suppla/tablas/tabla-remesas/tabla-remesas.component';
import { TablaRemesasSelecComponent } from 'app/componentes-suppla/tablas/tabla-remesas-selec/tabla-remesas-selec.component';
import { TablaFacturasComponent } from 'app/componentes-suppla/tablas/tabla-facturas/tabla-facturas.component';
import { GuiasService } from 'app/servicios/guias.service';
import { RemesasService } from 'app/servicios/remesas.service';
import { TablaDetalleGuiasIngresosComponent } from 'app/componentes-suppla/tablas/tabla-detalle-guias-ingresos/tabla-detalle-guias-ingresos.component';
import { TablaDetalleRemesasIngresosComponent } from 'app/componentes-suppla/tablas/tabla-detalle-remesas-ingresos/tabla-detalle-remesas-ingresos.component';
import { FacturasService } from 'app/servicios/facturas.service';
import { TablaDetalleFacturasComponent } from 'app/componentes-suppla/tablas/tabla-detalle-facturas/tabla-detalle-facturas.component';
import { TablaGuiasClienteComponent } from 'app/componentes-suppla/tablas/tabla-guias-cliente/tabla-guias-cliente.component';
import { GuiasClientePopupComponent } from 'app/componentes-suppla/popups/guias-cliente/guias-cliente.popup';
import { FiltroGuiasClienteComponent } from 'app/componentes-suppla/filtros/guias-cliente/filtro-guias-cliente.component';
import { FiltroRemesasClienteComponent } from 'app/componentes-suppla/filtros/remesas-cliente/filtro-remesas-cliente.component';
import { TablaRemesasClienteComponent } from 'app/componentes-suppla/tablas/tabla-remesas-cliente/tabla-remesas-cliente.component';
import { RemesasClientePopupComponent } from 'app/componentes-suppla/popups/remesas-cliente/remesas-cliente.popup';
import { CrearNotaCliPopupComponent } from 'app/componentes-suppla/popups/crear-nota-cli/crear-nota-cli.popup';
import { TablaFacturaGeneralCliComponent } from 'app/componentes-suppla/tablas/tabla-factura-general-cli/tabla-factura-general-cli';
import { TablaDetalleFacturaNotasComponent } from 'app/componentes-suppla/tablas/tabla-detalle-factura-notas/tabla-detalle-factura-notas.component';
import { TablaDetalleFacturaDocCobroComponent } from 'app/componentes-suppla/tablas/tabla-detalle-factura-doc-cobro/tabla-detalle-factura-doc-cobro.component';
import { TablaDistribucionIngresosClienteComponent } from 'app/componentes-suppla/tablas/tabla-distribucion-ingresos-cliente/tabla-distribucion-ingresos-cliente.component';

@NgModule({
  imports: [
    FacturacionRoutingModule,
    NgbModule,
    FormsModule,
    DxListModule,
    DxPopupModule,
    DxDataGridModule,
    DxDropDownBoxModule,
    DxTextBoxModule,
    DxFormModule,
    DxLoadIndicatorModule,
    DxAutocompleteModule,
    CommonModule,
    SharedModule
  ],
  declarations: [FacturacionComponent, TablaGuiasComponent, FiltroGuiasComponent,
    FiltroRemesasComponent, FiltroFacturasComponent, TablaGuiasSelecComponent,
    TablaRemesasComponent, TablaRemesasSelecComponent, TablaFacturasComponent,
    TablaDetalleGuiasIngresosComponent, TablaDetalleRemesasIngresosComponent,
    TablaDetalleFacturasComponent, TablaGuiasClienteComponent, GuiasClientePopupComponent,
    FiltroGuiasClienteComponent, FiltroRemesasClienteComponent, TablaRemesasClienteComponent,
    RemesasClientePopupComponent, TablaDistribucionIngresosClienteComponent,
    CrearNotaCliPopupComponent, TablaFacturaGeneralCliComponent, TablaDetalleFacturaNotasComponent,
    TablaDetalleFacturaDocCobroComponent],
    providers: [GuiasService, RemesasService, FacturasService]
})
export class FacturacionModule { }
