import { Component, OnInit, ViewChild } from '@angular/core';
import { GuiasService } from 'app/servicios/guias.service';
import { RemesasService } from 'app/servicios/remesas.service';
import { AuthenticacionService } from 'app/servicios/autenticacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { FacturasService } from 'app/servicios/facturas.service';
import { FiltroGuiasComponent } from 'app/componentes-suppla/filtros/guias/filtro-guias.component';
import { FiltroRemesasComponent } from 'app/componentes-suppla/filtros/remesas/filtro-remesas.component';

@Component({
    templateUrl: './facturacion.component.html'
})
export class FacturacionComponent implements OnInit {

    @ViewChild('filtroGuias') filtroGuias: FiltroGuiasComponent;
    @ViewChild('filtroRemesas') filtroRemesas: FiltroRemesasComponent;
    guiasPorFacturar: Array<any>;
    guiasSeleccionadas: Array<any>;
    remesasPorFacturar: Array<any>;
    remesasSeleccionadas: Array<any>;
    facturasPorFacturar: Array<any>;
    detalleFacturas: Array<any>;
    factNumero: string;
    factGeneral: any;
    tabDetalle: any;

    // tamaños tablas segun splitter
    widthTablaGuias = 0;
    widthTablaGuiasSel = 0;
    widthTablaRemesas = 0;
    widthTablaRemesasSel = 0;
    widthTablaFacturas = 0;
    widthTablaFacturasSel = 0;
    widthTablaFacturasNotas = 0;
    widthTablaFacturasDocs = 0;

    constructor(public as: AuthenticacionService, public rt: ActivatedRoute, public gs: GuiasService,
        public router: Router, public rs: RemesasService, public fs: FacturasService) {
        this.onInitializedtabDetalle = this.onInitializedtabDetalle.bind(this);
    }

    ngOnInit() {

        this.rt.params.subscribe(prms => {
            // console.log("prms['sesion']");
            this.as.ValidarSesion(prms['sesion']).then(res => {
                if (!res) {
                    this.router.navigate(['401']);
                    console.error('Acceso no autorizado');
                    return;
                }

                this.gs.guiasList.subscribe(lst => {
                    if (lst.length === 0) {
                        notify('No se encontraron guias para estos criterios de busqueda!', 'Info', 2000);
                    }
                    this.guiasPorFacturar = lst;
                });

                this.rs.remesasList.subscribe(lst => {
                    if (lst.length === 0) {
                        notify('No se encontraron guias para estos criterios de busqueda!', 'Info', 2000);
                    }
                    this.remesasPorFacturar = lst;
                });

                this.fs.facturasList.subscribe((lst: any) => {
                    if (lst !== undefined && lst.length === 0) {
                        notify('No se encontraron facturas para estos criterios de busqueda!', 'Info', 2000);
                    }
                    this.facturasPorFacturar = lst;
                });

                this.fs.detalleFacturas.subscribe((data: any) => {
                    if (data.Detalle.length === 0) {
                        notify('No se encontraron detalles para esta factura!', 'Info', 2000);
                    }
                    this.detalleFacturas = data.Detalle;
                    this.factGeneral = data;

                    if (this.tabDetalle) {
                        this.tabDetalle.option('selectedIndex', 0);
                    }
                });

                this.gs.guiasSeleccionadas.subscribe(lst => this.guiasSeleccionadas = lst);
                this.rs.remesasSeleccionadas.subscribe(lst => this.remesasSeleccionadas = lst);
                this.fs.detalleFacturaNumero.subscribe(val => this.factNumero = val);
            });
        });
    }

    onInitializedtabDetalle(e) {
        this.tabDetalle = e.component;
    }

    CalcularTamanoGuias(e) {
        if (e.primary > 0) {
            this.widthTablaGuias = e.primary - 10;
            this.widthTablaGuiasSel = e.secondary - 10;
        }
    }
    CalcularTamanoRemesas(e) {
        if (e.primary > 0) {
            this.widthTablaRemesas = e.primary - 10;
            this.widthTablaRemesasSel = e.secondary - 10;
        }
    }
    CalcularTamanoFacturas(e) {
        if (e.primary > 0) {
            this.widthTablaFacturas = e.primary - 10;
            this.widthTablaFacturasSel = this.widthTablaFacturasNotas = this.widthTablaFacturasDocs = e.secondary - 30;
        }
    }

    RefreshDataGuias(e: boolean) {
        if (e) {
            this.filtroGuias.Buscar(this.filtroGuias.filtroModel);
        }
    }

    RefreshDataRemesas(e: boolean) {
        if (e) {
            this.filtroRemesas.Buscar(this.filtroRemesas.filtroModel);
        }
    }
}
