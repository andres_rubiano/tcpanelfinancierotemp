import { AuthenticacionService } from 'app/servicios/autenticacion.service';
import { TipoBusquedaEnum } from 'app/enumeraciones/tipo-busqueda.enum';
import { ProveedoresService } from 'app/servicios/proveedores.service';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ClientesService } from 'app/servicios/clientes.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { debounce } from 'rxjs/operator/debounce';
import { TablaClientesComponent } from 'app/componentes-suppla/tablas/tabla-clientes/tabla-clientes.component';

@Component({
  templateUrl: 'tarifas.component.html',
  styleUrls: ['./tarifas.component.scss']
})
export class TarifasComponent implements OnInit {

  @ViewChild('tablaCli') tablaCli: TablaClientesComponent;
  tipoBusquedaCliente = TipoBusquedaEnum.Cliente;
  tipoBusquedaProveedor = TipoBusquedaEnum.Proveedor;
  tarifasList: any;
  clientesLista: any;
  proveedoresLista: any;
  modelosCliLista: any;
  articulosLista: any;
  articulosCliLista: any;
  modelosProvLista: any;
  articulosProvLista: any;
  tarifasCliLista: any;
  tarifasCliCant: number;
  tarifasProvLista: any;
  tarifasProvCant: number;
  filasNumeroCli: number;
  filasNumeroProv: number;
  heightTablaCli: number;
  heightTablaProv: number;
  ocultarTblCli: boolean;
  ocultarTblProv: boolean;

  constructor(public as: AuthenticacionService, public ts: TarifasService, public rt: ActivatedRoute,
    public cs: ClientesService, public ps: ProveedoresService, public ms: ModelosService, public ars: ArticulosService,
    public router: Router) {
    this.rt.params.subscribe(prms => {
      // console.log("prms['sesion']");
      // console.log(prms['sesion']);
      this.as.ValidarSesion(prms['sesion']).then(res => {
        if (!res) {
          this.router.navigate(['401']);
          console.error('Acceso no autorizado');
          return;
        }
      });
    });

    ts.tarifasCliList.subscribe(val => {
      this.tarifasList = val;
    });
  }

  ngOnInit(): void {
    this.cs.clientesLista.subscribe(val => this.tablaCli.clientesLista = val);
    this.ps.proveedoresLista.subscribe(val => {

      if (val.length === 0) {
        notify('No se encontraron registros para estos criterios de busqueda!', 'Info', 2000);
      }

      this.proveedoresLista = val;
    });
    this.ms.modelosCliLista.subscribe(val => {
      this.modelosCliLista = val;
    });

    this.ars.articulosCliLista.subscribe(val => {
      this.articulosCliLista = val;
    });
    this.ms.modelosProvLista.subscribe(val => {
      this.modelosProvLista = val;
    });

    this.ars.articulosProvLista.subscribe(val => {
      this.articulosProvLista = val;
    });

    this.ts.tarifasCliList.subscribe(list => {
      setTimeout(() => {
        if (list !== undefined) {
          this.tarifasCliLista = list;
          this.tarifasCliCant = list.length;
        }
      }, 500);
    });
    this.ts.tarifasProvList.subscribe(list => {
      //console.log('llegaron tarifas', list);
      
      if (list !== undefined) {
        this.tarifasProvLista = list;
        this.tarifasProvCant = list.length;
      }
    });
  }

  OcultarTblCli(e: boolean) {
    if (e) {
      this.ocultarTblCli = e;
      this.filasNumeroCli = 25;
      this.heightTablaCli = window.screen.height / 1.6

    } else {
      this.ocultarTblCli = false;
      this.filasNumeroCli = 15;
      this.heightTablaCli = window.screen.height / 2.5
    }
  }

  OcultarTblProv(e: boolean) {
    if (e) {
      this.ocultarTblProv = e;
      this.filasNumeroProv = 25;
      this.heightTablaProv = window.screen.height / 1.6

    } else {
      this.ocultarTblProv = false;
      this.filasNumeroProv = 15;
      this.heightTablaProv = window.screen.height / 2.5
    }
  }

  CalcularTamanoTar(e) {
    // console.log(e);

  }
}
