// core
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
// Terceros
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DxDataGridModule, DxDropDownBoxModule, DxTooltipModule, DxTemplateModule, DxPopupModule, DxFormModule, DxAutocompleteModule } from "devextreme-angular";
// spx modulos
import { SharedModule } from 'app/shared.module';
import { TarifasRoutingModule } from './tarifas-routing.module';
// spx servicios
import { ClientesService } from 'app/servicios/clientes.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ProveedoresService } from 'app/servicios/proveedores.service';
import { TarifasService } from 'app/servicios/tarifas.service';

// spx componentes
import { HeaderPrincipalComponent } from 'app/componentes-suppla/header-principal/header-principal.component';
import { TarifasComponent } from './tarifas.component';
import { TablaTarifasProvComponent } from 'app/componentes-suppla/tablas/tabla-tarifas-prov/tabla-tarifas-prov.component';
import { TablaArticulosProvComponent } from 'app/componentes-suppla/tablas/tabla-articulos-prov/tabla-articulos-prov.component';
import { TablaClientesComponent } from 'app/componentes-suppla/tablas/tabla-clientes/tabla-clientes.component';
import { FiltroClientesProveedoresComponent } from 'app/componentes-suppla/filtros/clientes-proveedores/filtro-clientes-proveedores.component';
import { TablaModelosCliComponent } from 'app/componentes-suppla/tablas/tabla-modelos-cli/tabla-modelos-cli.component';
import { TablaModelosProvComponent } from 'app/componentes-suppla/tablas/tabla-modelos-prov/tabla-modelos-prov.component';
import { TarifasFiltroCliComponent } from 'app/componentes-suppla/filtros/tarifas-filtro-cli/tarifas-filtro-cli.component';
import { TarifasFiltroProvComponent } from 'app/componentes-suppla/filtros/tarifas-filtro-prov/tarifas-filtro-prov.component';
import { TablaProveedoresComponent } from 'app/componentes-suppla/tablas/tabla-proveedores/tabla-proveedores.component';
import { TablaArticulosCliComponent } from 'app/componentes-suppla/tablas/tabla-articulos-cli/tabla-articulos-cli.component';
import { TablaTarifasCliComponent } from 'app/componentes-suppla/tablas/tabla-tarifas-cli/tabla-tarifas-cli.component';
import { AccionesTarifasCliComponent } from 'app/componentes-suppla/acciones-cli/acciones-tarifas-cli.component';
import { AccionesTarifasProvComponent } from 'app/componentes-suppla/acciones-prov/acciones-tarifas-prov.component';

@NgModule({
  imports: [
    TarifasRoutingModule,
    NgbModule,
    FormsModule,
    DxDataGridModule,
    DxDropDownBoxModule,
    DxTooltipModule,
    DxTemplateModule,
    DxPopupModule,
    DxFormModule,
    DxAutocompleteModule,
    CommonModule,
    SharedModule
  ],
  providers: [ClientesService, ProveedoresService, ModelosService, ArticulosService,
    TarifasService, TarifasFiltroCliComponent, TarifasFiltroProvComponent,
    TablaModelosCliComponent
  ],
  declarations: [
    TarifasComponent, TablaClientesComponent, TablaModelosCliComponent, TablaModelosProvComponent,
    TablaArticulosCliComponent, TablaArticulosProvComponent, TablaTarifasCliComponent,
    TablaTarifasProvComponent, TablaProveedoresComponent, FiltroClientesProveedoresComponent,
    TarifasFiltroCliComponent, TarifasFiltroProvComponent, AccionesTarifasCliComponent,
    AccionesTarifasProvComponent]
})
export class TarifasModule { }
