import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'valNegNoParentesis' })
export class ValorNegativoSinParentesisPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        if (value) {
            return value.charAt(0) === '(' ?
                '-' + value.substring(1, value.length).replace(')', '') :
                value;
        }
    }
}
