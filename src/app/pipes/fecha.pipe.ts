import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'fecha'})
export class FechaPipe implements PipeTransform {
    transform(value: string): any {
        return new Date(parseInt(value.replace('/Date(', ''))).toDateString();
    }
}
