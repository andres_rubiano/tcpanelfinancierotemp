import { ClienteListaInterface } from 'app/interfaces/cliente-lista.interface';
import { RespuestaAPI_Interface } from 'app/interfaces/respuestaAPI.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class ProveedoresService {

    proveedoresLista = new Subject<Array<any>>();
    buscandoProveedores = new Subject<boolean>();
    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {

    }

    CargarProveedoresLista(codigo = '', nombre = '', esquema = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const URL = API_URL + 'ObtenerConfiguracionDeEsquemasProveedor';
                const body = {
                    filtroCliente: {
                        ProveedorCodigo: codigo,
                        ProveedorNombre: nombre,
                        EsquemaCodigo: esquema,
                        Usuario: this.authSrvc.GetCurrentUser()
                    }
                }

                this._http.post(URL, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((res: any) => {
                        const proveedoresFechaOk = [];
                        res.ObtenerConfiguracionDeEsquemasProveedorResult.DTO.forEach(pro => {
                            let proveedorFechaOkTemp = new ClienteListaInterface();
                            proveedorFechaOkTemp = pro;
                            proveedorFechaOkTemp.FechaInicial = this.us.ConvertJsonDateToString(pro.FechaInicial);
                            proveedorFechaOkTemp.FechaFinal = this.us.ConvertJsonDateToString(pro.FechaFinal);
                            proveedoresFechaOk.push(proveedorFechaOkTemp)
                        });
                        resolve(proveedoresFechaOk);
                    }, (error) => {
                        console.error('Error obteniendo proveedores: ' + error.message);
                        notify('Error obteniendo proveedores: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                reject(error);
                console.error(error);
            }
        });
    }
}
