import { FiltroLiquidacionesInterface } from 'app/interfaces/filtro-liquidaciones.interface';
import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { LIQ_URL } from 'app/config/config';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class LiquidacionesService {

    liqPorLiquidar = new Subject<any[]>();
    liqSeleccionadas = new Subject<any[]>();
    buscandoLiquidaciones = new Subject<boolean>();
    buscandoLiquidacionesDet = new Subject<boolean>();
    detalleLiquidacionNum = new Subject<string>();
    detalleLiquidacionId = new Subject<string>();
    detalleLiquidacionProvSelected = new Subject<string>();
    detalleLiquidacionProvSelectedNombre = new Subject<string>();
    detalleLiquidacion = new Subject<Array<any>>();
    estadoLiquidacion = new Subject<EstadosLiquidacionEnum>();
    refrescarLiquidaciones = new Subject<boolean>();
    refrescarLiquidacionesDet = new Subject<boolean>();
    totalLiquidacionesDet = new Subject<number>();
    infoCrearNotaProv = new Subject<any>();

    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {

    }

    ObtenerLiquidaciones(filtro: FiltroLiquidacionesInterface): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'ObtenerLiquidaciones';
                const body = {
                    filtroLiquidaciones: {
                        FechaInicial: filtro.FechaInicial || '',
                        FechaFinal: filtro.FechaFinal || '',
                        ProveedorCodigo: filtro.ProveedorCodigo || '',
                        ProveedorNombre: filtro.ProveedorNombre || ''
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log('body liq new');
                // console.log(body);

                this._http.post(url, body, {
                    headers: new HttpHeaders()
                        .set('Cache-Control', 'no-cache')
                        .set('Pragma', 'no-cache')
                }).subscribe((res: any) => {
                    // console.log('liquidaciones', res.ObtenerLiquidacionesResult.DTO);
                    const liqFechaOk = [];
                    res.ObtenerLiquidacionesResult.DTO.forEach(liq => {
                        const liqTemp = liq;
                        liqTemp.FechaDeCreación = this.us.ConvertJsonDateToString(liq.FechaDeCreación);
                        liqFechaOk.push(liqTemp);
                    });

                    this.liqPorLiquidar.next(liqFechaOk);
                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo liquidaciones: ' + error.message);
                    notify('Error obteniendo liquidaciones: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    DetalleLiquidacion(liquidacionNumero: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                // tslint:disable-next-line:max-line-length
                const url = `${LIQ_URL}ObtenerLiquidacion?liquidacionNumero=${liquidacionNumero}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;

                this._http.get(url, {
                    headers: new HttpHeaders()
                        .set('Cache-Control', 'no-cache')
                        .set('Pragma', 'no-cache')
                }).subscribe((res: any) => {
                    console.log('detalle Liq', res.ObtenerLiquidacionResult.DTO);
                    // const liqDetFechaOk = [];
                    // res.ObtenerLiquidacionResult.DTO.forEach(det => {
                    //     const liqDetTemp = det;
                    //     liqDetTemp.ViajeFecha = this.us.ConvertJsonDateToString(det.ViajeFecha);
                    //     liqDetFechaOk.push(liqDetTemp);
                    // });
                    console.log('detalle', res.ObtenerLiquidacionResult.DTO);

                    resolve(res.ObtenerLiquidacionResult.DTO);
                }, (error) => {
                    console.error('Error obteniendo detalle de liquidaciones: ' + error.message);
                    notify('Error obteniendo detalle de liquidaciones: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    AnularLiquidacion(liqNum: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'AnularLiquidacion';
                const body = {
                    liquidacionNumero: liqNum,
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                // console.log("body anular liq");
                // console.log(body);

                this._http.post(url, body, {
                    headers: new HttpHeaders().set('content-type', 'application/json')
                }).subscribe((res: any) => {
                    // console.log("anular liquidacion resp");
                    // console.log(res.AnularLiquidacionResult);

                    resolve(res.AnularLiquidacionResult);
                }, (error) => {
                    console.error('Error anulando liquidaciones: ' + error.message);
                    notify('Error anulando liquidaciones: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    CerrarLiquidacion(liqNum: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'CerrarLiquidacion';
                const body = {
                    liquidacionNumero: liqNum,
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                // console.log("body cerrar liq");
                // console.log(body);

                this._http.post(url, body, {
                    headers: new HttpHeaders().set('content-type', 'application/json')
                }).subscribe((res: any) => {
                    // console.log("cerrar liquidacion resp");
                    // console.log(res.CerrarLiquidacionResult);

                    resolve(res.CerrarLiquidacionResult);
                }, (error) => {
                    console.error('Error cerrando liquidaciones: ' + error.message);
                    notify('Error cerrando liquidaciones: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    ObtenerInfoNotaProveedor(costoId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                // tslint:disable-next-line:max-line-length
                const url = LIQ_URL + `ObtenerInformacionParaCrearNotaProveedor?costoId=${costoId}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;

                // console.log('body ObtenerInformacionParaCrearNotaProveedor liq');
                // console.log(url);

                this._http.get(url).subscribe((res: any) => {
                    // console.log('ObtenerInformacionParaCrearNotaProveedor resp');
                    // console.log(res.ObtenerInformacionParaCrearNotaProveedorResult);

                    resolve(res.ObtenerInformacionParaCrearNotaProveedorResult);
                }, (error) => {
                    console.error('Error ObtenerInformacionParaCrearNotaProveedor: ' + error.message);
                    notify('Error ObtenerInformacionParaCrearNotaProveedor: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    CrearNotaProveedor(notaBody): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'CrearNotaProveedor';
                const body = notaBody;
                // console.log('body CrearNotaProveedor liq');
                // console.log(notaBody);

                this._http.post(url, body, {
                    headers: new HttpHeaders().set('content-type', 'application/json')
                }).subscribe((res: any) => {
                    // console.log('CrearNotaProveedor resp', res.CrearNotaProveedor);
                    resolve(res.CrearNotaProveedorResult);
                }, (error) => {
                    console.error('Error CrearNotaProveedor: ' + error.message);
                    notify('Error CrearNotaProveedor: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    EliminarNotaProveedor(notaNumero): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                // tslint:disable-next-line:max-line-length
                const url = LIQ_URL + `AnularNotaProveedor?notaProveedorNumero=${notaNumero}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;

                this._http.get(url).subscribe((res: any) => {
                    // console.log('AnularNotaProveedor resp', res);
                    resolve(res.AnularNotaProveedorResult);
                }, (error) => {
                    console.error('Error AnularNotaProveedor: ' + error.message);
                    notify('Error AnularNotaProveedor: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    ActualizarReferenciaExterna(liqNum: string, refExterna: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL +
                    // tslint:disable-next-line:max-line-length
                    `ActualizarReferenciaExterna?liquidacionNumero=${liqNum}&referenciaExterna=${refExterna}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;
                this._http.get(url).subscribe((res: any) => {
                    resolve(res.ActualizarReferenciaExternaResult);
                }, (error) => {
                    console.error('Error actualizando referencia: ' + error.message);
                    notify('Error actualizando referencia: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }
}
