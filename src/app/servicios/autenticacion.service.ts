import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Http } from '@angular/Http';
import { CatalogosService } from './catalogos.service';

@Injectable()
export class AuthenticacionService {

    private _currentUser: string;
    constructor(public _http?: Http) { }

    ValidarSesion(sesion: string): Promise<boolean> {

        const success = false;
        const url = `${API_URL}ValidarSesion?sesioncodigo=${sesion}`;
        return new Promise((resolve, reject) => {
            try {
                this._http.get(url).subscribe(res => {
                    // console.log("res.json().ValidarSesionResult.DTO");
                    // console.log(res.json().ValidarSesionResult.DTO);
                    if (res.json().ValidarSesionResult.DTO === 'True') {
                        // console.log('auth', res.json().ValidarSesionResult.Message.TransactionNumber);
                        this._currentUser = res.json().ValidarSesionResult.Message.TransactionNumber;
                        // console.log('valida sesion user', this._currentUser);
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }, (error) => {
                    console.error('Error validando sesion: ' + error);
                    notify('Error validando sesion: ' + error, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    GetCurrentUser(): string {
        return this._currentUser;
    }

    GetInitialCurrentUser(): Promise<string> {
        return new Promise((resolve, reject) => {
            resolve(this._currentUser);
        });
    }
}
