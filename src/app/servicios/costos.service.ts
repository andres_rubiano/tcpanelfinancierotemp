import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LIQ_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class CostosService {

    costosPorLiquidar = new Subject<any[]>();
    costosPorProveedor = new Subject<any[]>();
    costosSeleccionados = new Subject<any[]>();
    costosSeleccionadosValor = new Subject<number>();
    buscandoCostos = new Subject<boolean>();
    buscandoCostosProv = new Subject<boolean>();

    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {

    }

    ObtenerCostos(filtro: FiltroCostosInterface, isCostosProveedor: boolean = false): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'ObtenerCostosParaLiquidar';
                const body = {
                    filtroCostos: {
                        FechaInicial: filtro.FechaInicial || '',
                        FechaFinal: filtro.FechaFinal || '',
                        ProveedorCodigo: filtro.ProveedorCodigo || '',
                        ProveedorNombre: filtro.ProveedorNombre || '',
                        ArticuloDeTarifaCodigo: filtro.ArticuloDeTarifaCodigo || '',
                        Vehiculo: filtro.Vehiculo || '',
                        CiudadOrigenCodigo: filtro.CiudadOrigenCodigo || '',
                        CiudadDestinoCodigo: filtro.CiudadDestinoCodigo || '',
                        Sucursal: filtro.Sucursal || ''
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log('body costos');
                // console.log(body);

                this._http.post(url, body).subscribe((res: any) => {
                    console.log('costos');
                    console.log(res.ObtenerCostosParaLiquidarResult.DTO);
                    const costosFechaOk = [];
                    res.ObtenerCostosParaLiquidarResult.DTO.forEach(costo => {
                        const costoTemp = costo;
                        costoTemp.FechaDeCreacion = this.us.ConvertJsonDateToString(costo.FechaDeCreacion || costo.FechaCreacion);
                        costosFechaOk.push(costoTemp);
                    });

                    if (isCostosProveedor) {
                        this.costosPorProveedor.next(costosFechaOk);
                    } else {
                        this.costosPorLiquidar.next(costosFechaOk);
                    }

                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo costos: ' + error.message);
                    notify('Error obteniendo costos: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    LiquidarCostos(costos: Array<string>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'CrearLiquidacion';
                const body = {
                    costosId: costos,
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                console.log('body liquidar costos');
                console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), }).subscribe(res => {
                        resolve(res);
                    }, (error) => {
                        console.error('Error liquidando costos: ' + error.message);
                        notify('Error liquidando costos: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                console.error('Error liquidando costos: ' + error);
                reject(error);
            }
        });
    }

    AdicionarCostosViaje(liqNum: string, costos: Array<string>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'AdicionarDetalleLiquidacion';
                const body = {
                    liquidacionNumero: liqNum,
                    costosId: costos,
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                // console.log("body adicionar costos viaje");
                // console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((res: any) => {
                        resolve(res.AdicionarDetalleLiquidacionResult.Message);
                    }, (error) => {
                        console.error('Error adicionando costos viaje: ' + error.message);
                        notify('Error adicionando costos viaje: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                console.error('Error al adicionar costos viaje: ' + error);
                reject(error);
            }
        });
    }

    RemoverCostos(costos: Array<string>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'RemoverDetalleLiquidacion';
                const body = {
                    detalleLiquidacionIds: costos,
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                console.log('body remover costos viaje');
                console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((res: any) => {
                        resolve(res.RemoverDetalleLiquidacionResult);
                    }, (error) => {
                        console.error('Error removiendo costos: ' + error.message);
                        notify('Error removiendo costos: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                console.error('Error al remover costos viaje: ' + error);
                reject(error);
            }
        });
    }
}
