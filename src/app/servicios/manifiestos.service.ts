import { FiltroManifiestoInterface } from 'app/interfaces/filtro-manifiesto.interface';
import { Http, RequestOptions, Headers } from '@angular/Http';
import { LIQ_URL } from 'app/config/config';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class ManifiestosService {

    manifPorLiquidar = new Subject<any[]>();
    manifSeleccionados = new Subject<any[]>();
    buscandoManifiestos = new Subject<boolean>();

    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {

    }

    ObtenerManifiestos(filtro: FiltroManifiestoInterface): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'ObtenerViajesConManifiestoParaLiquidar';
                const body = {
                    filtro: {
                        FechaInicial: filtro.FechaInicial || '',
                        FechaFinal: filtro.FechaFinal || '',
                        PoseedorCodigo: filtro.PoseedorCodigo || '',
                        PoseedorNombre: filtro.PoseedorNombre || '',
                        Vehiculo: filtro.Vehiculo || '',
                        CiudadOrigenCodigo: filtro.CiudadOrigenCodigo || '',
                        CiudadDestinoCodigo: filtro.CiudadDestinoCodigo || '',
                        SucursalCodigo: filtro.Sucursal || ''
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log("body manifiestos");
                // console.log(body);

                this._http.post(url, body).subscribe((res: any) => {
                    // console.log("manifiestos");
                    // console.log(res.ObtenerViajesConManifiestoParaLiquidarResult);

                    const manifFechaOk = [];
                    res.ObtenerViajesConManifiestoParaLiquidarResult.DTO.forEach(viaje => {
                        const viajeTemp = viaje;
                        viajeTemp.FechaDeCreacion = this.us.ConvertJsonDateToString(viaje.FechaDeCreacion);
                        manifFechaOk.push(viajeTemp);
                    });

                    this.manifPorLiquidar.next(manifFechaOk);
                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo manifiestos: ' + error);
                    notify('Error obteniendo manifiestos: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    LiquidarManifiestos(viajes: Array<string>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = LIQ_URL + 'LiquidarViajeConManifiesto';
                const body = { viajes: viajes, codigoUsuario: this.authSrvc.GetCurrentUser() };
                console.log('body liquidar manifiestos');
                console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') })
                    .subscribe((res: any) => {
                        // console.log("res.LiquidarViajeConManifiesto");
                        // console.log(res.LiquidarViajeConManifiestoResult);

                        resolve(res.LiquidarViajeConManifiestoResult);
                    }, (error) => {
                        console.error('Error liquidando manifiestos: ' + error.message);
                        notify('Error liquidando manifiestos: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                console.error('Error liquidando manifiestos: ' + error);
                reject(error);
            }
        });
    }

    DetalleViaje(viajeNumero: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = `${LIQ_URL}ObtenerDetalleViaje?viajeNumero=${viajeNumero}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;
                console.log("Url : " + url)
                this._http.get(url
                    , {
                    headers: new HttpHeaders()
                        .set('Cache-Control', 'no-cache')
                        .set('Pragma', 'no-cache')
                }
            ).subscribe((res: any) => {
                    // console.log("detalle");
                    // console.log(res.json().ObtenerDetalleViajeResult.DTO);
                    resolve(res.ObtenerDetalleViajeResult.DTO);
                }, (error) => {
                    console.error('Error obteniendo detalle viaje: ' + error.message);
                    notify('Error obteniendo detalle viaje: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }
}
