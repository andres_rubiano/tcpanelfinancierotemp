import { SucursalInterface } from 'app/interfaces/sucursal.interface';
import { VehiculoInterface } from 'app/interfaces/vehiculo.interface';
import { EmpaqueInterface } from 'app/interfaces/empaque.interface';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import { RutaInterface } from 'app/interfaces/ruta.interface';
import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Http } from '@angular/Http';
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class CatalogosService {

    horaDeCargue: Date;
    ciudades = new Subject<Array<CiudadInterface>>();
    vehiculos = new Subject<Array<VehiculoInterface>>();
    rutas = new Subject<Array<RutaInterface>>();
    empaques = new Subject<Array<EmpaqueInterface>>();
    sucursales = new Subject<Array<SucursalInterface>>();

    constructor(public _http: Http, private authSrvc: AuthenticacionService) {
    }

    CargarCatalogos() {
        setTimeout(() => {
            // console.log('carga catalogos');
            try {
                this.authSrvc.GetInitialCurrentUser().then(user => {
                    // console.log('user', user);

                    const url = `${API_URL}ObtenerDatosParaFiltrosDeTarifa?codigoUsuario=${user}`;
                    this._http.get(url).subscribe(data => {
                        // console.log('catalogos', data);
                        const global = data.json().ObtenerDatosParaFiltrosDeTarifaResult.DTO;
                        if (global) {
                            this.vehiculos.next(global.Vehiculos);
                            this.rutas.next(global.Rutas);
                            this.empaques.next(global.Empaques);
                            this.sucursales.next(global.Sucursales);
                            this.ciudades.next(global.Ciudades);
                        }
                    }, (error) => {
                        console.error('Error cargando catalogos:');
                        console.error(error);
                        notify('Error cargando catalogos: ' + error, 'Error', 10000);
                    });
                });


            } catch (error) {
                console.error('Error cargando catalogos');
                notify('Error cargando catalogos: ' + error, 'Error', 10000);
            }
        }, 500);
    }
}
