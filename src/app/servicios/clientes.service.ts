import { ClienteListaInterface } from 'app/interfaces/cliente-lista.interface';
import { RespuestaAPI_Interface } from 'app/interfaces/respuestaAPI.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, RequestOptions, Headers } from '@angular/Http';
import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class ClientesService {

    clientesLista = new Subject<Array<any>>();
    buscandoClientes = new Subject<boolean>();
    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {

    }

    CargarClientesLista(codigo: string = '', nombre: string = '', esquema: string = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const URL = API_URL + 'ObtenerConfiguracionDeEsquemasCliente';
                const body = {
                    filtroCliente: {
                        ClienteCodigo: codigo,
                        ClienteNombre: nombre,
                        EsquemaCodigo: esquema,
                        Usuario: this.authSrvc.GetCurrentUser()
                    }
                }
                // console.log('body clientes');
                // console.log(body);

                this._http.post(URL, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((res: any) => {
                        const clientesFechaOk = [];
                        res.ObtenerConfiguracionDeEsquemasClienteResult.DTO.forEach(cli => {
                            let clienteFechaOkTemp = new ClienteListaInterface();
                            clienteFechaOkTemp = cli;
                            clienteFechaOkTemp.FechaInicial = this.us.ConvertJsonDateToString(cli.FechaInicial);
                            clienteFechaOkTemp.FechaFinal = this.us.ConvertJsonDateToString(cli.FechaFinal);
                            clientesFechaOk.push(clienteFechaOkTemp)
                        });
                        resolve(clientesFechaOk);
                    }, (error) => {
                        console.error('Error obteniendo clientes: ' + error.message);
                        notify('Error obteniendo clientes: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                reject(error);
                console.error(error);
            }
        });
    }
}
