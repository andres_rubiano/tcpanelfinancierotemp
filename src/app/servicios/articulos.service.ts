import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class ArticulosService {

    articulosCliLista = new Subject<Array<any>>();
    articulosProvLista = new Subject<Array<any>>();
    selectedRowCli = new Subject<number>();
    selectedRowProv = new Subject<number>();

    constructor(public _http: HttpClient, private authSrvc: AuthenticacionService) {

    }

    ObtenerTarifasPorArticulo(filtro: any): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = API_URL + 'ObtenerTarifasPorArticulo';
                const body = {
                    filtrosTarifasPorArticulo: {
                        EsquemaCodigo: filtro.EsquemaCodigo,
                        TipoEsquema: filtro.TipoEsquema,
                        ClienteCodigo: filtro.ClienteCodigo,
                        ProveedorCodigo: filtro.ProveedorCodigo,
                        ModeloCodigo: filtro.ModeloCodigo,
                        ArticuloCodigo: filtro.ArticuloCodigo,
                        CiudadOrigenCodigo: '',
                        CiudadDestinoCodigo: '',
                        TipoVehiculoCodigo: '',
                        TipoRutaCodigo: '',
                        EmpaqueOficialCodigo: '',
                        RangoMinimo: '0',
                        RangoMaximo: '0',
                        Valor: '0',
                        FechaInicial: '/Date(1496404800000)/',
                        FechaFinal: '/Date(1496404800000)/',
                        Estado: '0'
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                // console.log("body obtener excel por articulo");
                // console.log(body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((resp) => {
                        // console.log("resp obtener excel por articulo");
                        // console.log(resp);
                        resolve(resp);
                    }, (error) => {
                        console.error('Error obteniendo tarifas por articulo: ' + error.message);
                        notify('Error obteniendo tarifas por articulo: ' + error.message, 'Error', 10000);
                    });

            } catch (error) {
                reject(error);
            }
        });
    }
}
