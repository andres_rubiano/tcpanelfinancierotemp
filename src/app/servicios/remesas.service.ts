import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { FiltroRemesasInterface } from 'app/interfaces/filtro-remesas.interface';
import { FACT_URL } from 'app/config/config';
import { HttpClient } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { HttpHeaders } from '@angular/common/http';
import { FacturasService } from 'app/servicios/facturas.service';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class RemesasService {
    remesas: Array<any>;
    remesasCli: Array<any>;
    remesasList = new Subject<Array<any>>();
    remesasCliente = new Subject<Array<any>>();
    remesasSeleccionadas = new Subject<any[]>();
    remesasSeleccionadosValor = new Subject<number>();
    buscandoRemesas = new Subject<boolean>();
    buscandoRemesasCli = new Subject<boolean>();
    clienteCodigoSelec: string;

    constructor(public _http: HttpClient, public us: Utils, private fs: FacturasService, private authSrvc: AuthenticacionService) {
        this.remesasList.subscribe(lst => this.remesas = lst);
        this.remesasCliente.subscribe(lst => this.remesasCli = lst);
        this.fs.detalleFacturaCliSelectedCod.subscribe(val => this.clienteCodigoSelec = val);
    }

    ObtenerRemesas(filtro: FiltroRemesasInterface, isRemesasCli = false): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'ObtenerRemesasParaFacturar';
                const body = {
                    filtroRemesas: {
                        CiudadDestinoCodigo: filtro.CiudadDestinoCodigo || '',
                        CiudadOrigenCodigo: filtro.CiudadOrigenCodigo || '',
                        ClienteCodigo: filtro.ClienteCodigo || '',
                        ClienteNombre: filtro.ClienteNombre || '',
                        FechaFinal: filtro.FechaFinal || '',
                        FechaInicial: filtro.FechaInicial || '',
                        LineaNegocioCodigo: filtro.LineaNegocioCodigo || '',
                        LineaNegocioDescripcion: filtro.LineaNegocioDescripcion || '',
                        ReferenciaAdicional: filtro.ReferenciaAdicional || ''
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log('body remesas');
                // console.log(body);

                this._http.post(url, body).subscribe((res: any) => {
                    // console.log("remesas result");
                    // console.log(res.ObtenerRemesasParaFacturarResult.DTO);
                    if (res.ObtenerRemesasParaFacturarResult.Message.TypeEnum === 3) {
                        notify('Error obteniendo remesas. Descripción: '
                            + res.ObtenerRemesasParaFacturarResult.Message.Message, 'Error', 7000);
                        this.buscandoRemesas.next(false);
                        return;
                    }
                    const remesasFechaOk = [];
                    res.ObtenerRemesasParaFacturarResult.DTO.forEach(remesa => {
                        const remesaTemp = remesa;
                        remesaTemp.FechaCreacion = this.us.ConvertJsonDateToString(remesa.FechaDeCreacion || remesa.FechaCreacion);
                        remesasFechaOk.push(remesaTemp);
                    });

                    if (!isRemesasCli) {
                        this.remesasList.next(remesasFechaOk);
                    } else {
                        this.remesasCliente.next(remesasFechaOk);
                    }
                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo remesas: ' + error.message);
                    notify('Error obteniendo remesas: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    DetalleRemesasIngresos(remesaNum, isRemesasCli = false): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                if (!isRemesasCli) {
                    this.remesas.forEach((remesa: any) => {
                        if (remesa.Numero === remesaNum) {
                            resolve(remesa.Ingresos);
                        }
                    });
                } else {
                    this.remesasCli.forEach((remesa: any) => {
                        if (remesa.Numero === remesaNum) {
                            resolve(remesa.Ingresos);
                        }
                    });
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    EstablecerRemesasNoFacturables(remesas: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'EstablecerRemesasComoNoFacturable';
                const body = {remesas: remesas, codigoUsuario: this.authSrvc.GetCurrentUser()};
                // console.log('body no fact remesas', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                    resolve(resp);
                })
            } catch (error) {
                reject(error);
                console.error('Error estableciendo remesas no facturables. Descripción: ' + error);
                notify('Error estableciendo remesas no facturables. Descripción: ' + error, 'Error', 7000);
            }
        });
    }

    FacturarRemesas(remesas: string[], observacion = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'FacturarRemesas';
                const body = {remesas: remesas, observacion: observacion, codigoUsuario: this.authSrvc.GetCurrentUser()};
                // console.log('body fact remesas', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                    resolve(resp);
                });
            } catch (error) {
                reject(error);
                console.error('Error facturando remesas. Descripción: ' + error);
                notify('Error facturando remesas. Descripción: ' + error, 'Error', 7000);
            }
        });
    }
}
