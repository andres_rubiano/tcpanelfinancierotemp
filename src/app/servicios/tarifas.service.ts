import { FiltroRequeridoInterface } from 'app/interfaces/filtro-requerido.interface';
import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { TarifaInterface } from 'app/interfaces/tarifa.interface';
import { API_URL, RATING_URL } from 'app/config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class TarifasService {

    tarifasCliList = new Subject<Array<TarifaInterface>>();
    tarifasProvList = new Subject<Array<TarifaInterface>>();
    buscandoTarifasCli = new Subject<boolean>();
    buscandoTarifasProv = new Subject<boolean>();
    isCollapsedObs = new Subject<boolean>();
    tipoEsquema = new Subject<TipoEsquemaEnum>();
    clienteCodigoSelected = new Subject<string>();
    proveedorCodigoSelected = new Subject<string>();
    esquemaCliCodigoSelected = new Subject<string>();
    esquemaProvCodigoSelected = new Subject<string>();
    modeloCliCodigoSelected = new Subject<string>();
    modeloProvCodigoSelected = new Subject<string>();
    articuloCliCodigoSelected = new Subject<string>();
    articuloProvCodigoSelected = new Subject<string>();
    filtrosTarifasCli = new Subject<Array<FiltroRequeridoInterface>>();
    filtrosTarifasProv = new Subject<Array<FiltroRequeridoInterface>>();
    tarifasSelectedCli = new Subject<Array<TarifaInterface>>();
    tarifasSelectedProv = new Subject<Array<TarifaInterface>>();
    exportarTarifas = new Subject<boolean>();
    nuevaBusquedaCli = new Subject<boolean>();
    nuevaBusquedaProv = new Subject<boolean>();

    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) { }

    ObtenerTarifas(filtro: TarifaInterface): Promise<Array<any>> {
        return new Promise((resolve, reject) => {
            try {
                const URL = API_URL + 'ObtenerTarifas';
                const body = {
                    filtrosTarifas: {
                        EsquemaCodigo: filtro.EsquemaCodigo,
                        TipoEsquema: filtro.ClienteCodigo ? TipoEsquemaEnum.Cliente : TipoEsquemaEnum.Proveedor,
                        ClienteCodigo: filtro.ClienteCodigo || '',
                        ProveedorCodigo: filtro.ProveedorCodigo || '',
                        ModeloCodigo: filtro.ModeloCodigo,
                        ArticuloCodigo: filtro.ArticuloCodigo,
                        CiudadOrigenCodigo: filtro.CiudadOrigenCodigo || '',
                        CiudadDestinoCodigo: filtro.CiudadDestinoCodigo || '',
                        TipoVehiculoCodigo: filtro.TipoVehiculoCodigo,
                        TipoRutaCodigo: filtro.TipoRutaCodigo,
                        EmpaqueOficialCodigo: filtro.EmpaqueOficialCodigo,
                        FechaInicial: filtro.FechaInicial || '/Date(1496404800000)/',
                        FechaFinal: filtro.FechaFinal || '/Date(1514678400000)/',
                        Estado: filtro.Estado
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log("body tarifas Cli");
                // console.log(body);

                this._http.post(URL, body).subscribe((res: any) => {
                    const tarifasFechaOk = [];

                    if (res.ObtenerTarifasResult.Message.TypeEnum === 3) {
                        notify('Error cargando tarifas. Descripción: ' + res.ObtenerTarifasResult.Message.Message,
                        'Error', 7000);
                        this.buscandoTarifasCli.next(false);
                        return;
                    }

                    res.ObtenerTarifasResult.DTO.forEach(tar => {
                        let tarifaTemp = new TarifaInterface();
                        tarifaTemp = tar;
                        tarifaTemp.FechaInicial = this.us.ConvertJsonDateToString(tar.FechaInicial);
                        tarifaTemp.FechaFinal = this.us.ConvertJsonDateToString(tar.FechaFinal);
                        tarifaTemp.Estado = tar.Estado === 1 ? 'Activo' : 'Inactivo';
                        tarifasFechaOk.push(tarifaTemp);
                    });
                    resolve(tarifasFechaOk);
                }, (error) => {
                    console.error('Error obteniendo tarifas: ' + error.message);
                    notify('Error obteniendo tarifas: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    // Coincidencia exacta
    RequeridosFiltrosPorTarifa(modeloCod: string, articuloCod: string): Promise<Array<FiltroRequeridoInterface>> {
        return new Promise((resolve, reject) => {
            try {
                const append = `?codigoUsuario=${this.authSrvc.GetCurrentUser()}&modeloCodigo=${modeloCod}&articuloCodigo=${articuloCod}`;
                const URL = `${API_URL}CoincidenciaFiltrosPorTarifa${append}`;

                this._http.get(URL).subscribe((res: any) => {
                    resolve(res.CoincidenciaFiltrosPorTarifaResult.DTO);
                }, (error) => {
                    console.error('Error obteniendo requeridos por tarifas: ' + error.message);
                    notify('Error obteniendo requeridos por tarifas: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    VencerTarifas(fecha: string, tarifasIds: Array<number>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = RATING_URL + 'VencerTarifas';
                let tarifasString = '';
                tarifasIds.forEach(tar => {
                    tarifasString.length === 0
                        ? tarifasString = tar.toString()
                        : tarifasString += ',' + tar.toString()
                });
                const body = `{"tarifasId": [${tarifasString}],
                 "fechaVencimiento": "${this.us.GetJsonDateFromYYYYMMDD(2, fecha)}",
                 "codigoUsuario": "${this.authSrvc.GetCurrentUser()}"}`;

                // console.log("vencer body");
                // console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe(res => {
                        resolve(res);
                    }, (error) => {
                        console.error('Error venciendo tarifas: ' + error.message);
                        notify('Error venciendo tarifas: ' + error.message, 'Error', 10000);
                    });
            } catch (error) {
                console.error('Error venciendo tarifas: ' + error);
                reject(error);
            }
        });
    }
}
