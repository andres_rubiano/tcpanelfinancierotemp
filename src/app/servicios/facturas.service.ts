import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FACT_URL } from 'app/config/config';
import { FiltroFacturasInterface } from 'app/interfaces/filtro-facturas.interface';
import { EstadosFacturacionEnum } from 'app/enumeraciones/estados-facturacion.enum';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class FacturasService {

    facturasList = new Subject<Array<any>>();
    facturas: Array<any>;
    buscandoFacturas = new Subject<boolean>();
    buscandoFacturasDet = new Subject<boolean>();
    detalleFacturas = new Subject<Array<any>>();
    detalleFacturaNumero = new Subject<string>();
    estadoFacturacion = new Subject<EstadosFacturacionEnum>();
    detalleFacturaId = new Subject<string>();
    detalleFacturaCliSelectedCod = new Subject<string>();
    detalleFacturaCliSelectedNombre = new Subject<string>();
    refrescarFacturas = new Subject<boolean>();
    refrescarDetalle = new Subject<boolean>();
    totalFacturasDet = new Subject<number>();

    constructor(public _http: HttpClient, public us: Utils, private authSrvc: AuthenticacionService) {
        this.facturasList.subscribe(lst => this.facturas = lst);
    }

    ObtenerFacturas(filtro: FiltroFacturasInterface): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'ObtenerFacturas';
                const body = {
                    'filtroFacturas': {
                        'ClienteCodigo': filtro.ClienteCodigo || '',
                        'ClienteNombre': filtro.ClienteNombre || '',
                        'FechaInicial': filtro.FechaInicial || '',
                        'FechaFinal': filtro.FechaFinal || ''
                    },
                    'codigoUsuario': this.authSrvc.GetCurrentUser()
                }
                // console.log('body facturas', body);

                this._http.post(url, body, {
                    headers: new HttpHeaders()
                        .set('Cache-Control', 'no-cache')
                        .set('Pragma', 'no-cache')
                }).subscribe((res: any) => {
                    console.log('facturas result', res.ObtenerFacturasResult.DTO);
                    if (res.ObtenerFacturasResult.Message.TypeEnum === 3) {
                        notify('Error obteniendo facturas. Descripcion: ' + res.ObtenerFacturasResult.Message.Message, 'Error', 7000);
                        this.buscandoFacturas.next(false);
                        return;
                    }
                    const facturasFechaOk = [];
                    res.ObtenerFacturasResult.DTO.forEach((fact: any) => {
                        const facturaTemp = fact;
                        facturaTemp.FechaCreacion = this.us.ConvertJsonDateToString(fact.FechaDeCreacion || fact.FechaCreacion);
                        facturaTemp.Fecha = this.us.ConvertJsonDateToString(fact.Fecha);

                        // if (fact.Total && (fact.Total as string).indexOf(',') > 0) {
                        //     facturaTemp.Total = (fact.Total as string).replace(',', '.');
                        // }
                        facturasFechaOk.push(facturaTemp);
                    });
                    // console.log('facturasFechaOk', facturasFechaOk);
                    this.facturasList.next(facturasFechaOk);
                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo facturas: ' + error.message);
                    notify('Error obteniendo facturas: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    DetalleFactura(facturaNum): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + `ObtenerFactura?facturaNumero=${facturaNum}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;
                this._http.get(url, {
                    headers: new HttpHeaders()
                        .set('Cache-Control', 'no-cache')
                        .set('Pragma', 'no-cache')
                }).subscribe((res: any) => {
                    if (res.ObtenerFacturaResult.Message.TypeEnum === 3) {
                        notify(`Error obteniendo detalle de factura No.${facturaNum}. Descripción: `
                            + res.ObtenerFacturaResult.Message.Message, 'Error', 7000);
                        return;
                    }
                    // console.log('res.ObtenerFacturaResult.DTO', res.ObtenerFacturaResult.DTO);

                    const facturasValoresSinComa = [];
                    res.ObtenerFacturaResult.DTO.Detalle.forEach((det: any) => {
                        const detTemp = det;
                        if (det.IngresoTotal && det.IngresoTotal.toString().indexOf(',') > 0) {
                            detTemp.IngresoTotal = det.IngresoTotal.toString().replace(',', '.');
                        }

                        if (det.IngresoUnitario && det.IngresoUnitario.toString().indexOf(',') > 0) {
                            detTemp.IngresoUnitario = det.IngresoUnitario.toString().replace(',', '.');
                        }
                        if (det.Observaciones) {
                            detTemp.Observaciones = det.Observaciones.toString().toLocaleLowerCase();
                        }
                        facturasValoresSinComa.push(detTemp);
                    });
                    res.ObtenerFacturaResult.DTO.Detalle = facturasValoresSinComa;

                    resolve(res.ObtenerFacturaResult.DTO);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    AgregarGuiasAFactura(factNumero: string, guias: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'AdicionarGuiaAFactura';
                const body = { facturaNumero: factNumero, guiaNumero: guias, codigoUsuario: this.authSrvc.GetCurrentUser() };
                console.log('body fact guias', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error agregando guias!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error agregando guias!. Descripcion: ', error);
            }
        });
    }

    AgregarRemesasAFactura(factNumero: string, remesas: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'AdicionarRemesaAFactura';
                const body = { facturaNumero: factNumero, remesaNumero: remesas, codigoUsuario: this.authSrvc.GetCurrentUser() };
                console.log('body fact remesas', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error agregando remesas!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error agregando remesas!. Descripcion: ', error);
            }
        });
    }

    RemoverGuiasAFactura(guias: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'RemoverDetalleFacturaOrigenGuia';
                const body = `{"detalleGuiaIds": [${guias}], "codigoUsuario": "${this.authSrvc.GetCurrentUser()}"}`;
                console.log('body remover guias', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error removiendo guias!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error removiendo guias!. Descripcion: ', error);
            }
        });
    }

    RemoverRemesasAFactura(remesas: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'RemoverDetalleFacturaOrigenRemesa';
                const body = `{"detalleFacturaIds": [${remesas}], "codigoUsuario": "${this.authSrvc.GetCurrentUser()}"}`;
                console.log('body remover remesas', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error removiendo remesas!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error removiendo remesas!. Descripcion: ', error);
            }
        });
    }

    AnularFactura(factNumero: string) {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'AnularFactura';
                const body = `{"facturaNumero": "${factNumero}", "codigoUsuario": "${this.authSrvc.GetCurrentUser()}"}`;
                console.log('body anular factura', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error anulando factura!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error anulando factura!. Descripcion: ', error);
            }
        });
    }

    VolverDefinitiva(factNumero: string) {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'EstablecerFacturaComoDefinitiva';
                const body = `{"facturaNumero": "${factNumero}", "codigoUsuario": "${this.authSrvc.GetCurrentUser()}"}`;
                console.log('body definitiva factura', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                notify('Error volviendo definitiva factura!. Descripcion: ' + error, 'Error', 7000);
                console.error('Error volviendo definitiva factura!. Descripcion: ', error);
            }
        });
    }

    CrearNotaCliente(notaBody): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'CrearNotaCliente';
                const body = notaBody;
                console.log('body CrearNotaCliente fact');
                console.log(notaBody);

                this._http.post(url, body, {
                    headers: new HttpHeaders().set('content-type', 'application/json')
                }).subscribe((res: any) => {
                    console.log('CrearNotaCliente resp', res.CrearNotaCliente);
                    resolve(res.CrearNotaClienteResult);
                }, (error) => {
                    console.error('Error CrearNotaCliente: ' + error.message);
                    notify('Error CrearNotaCliente: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    EliminarNotaCliente(notaNumero): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + `AnularNotaCliente?notaClienteNumero=${notaNumero}&codigoUsuario=${this.authSrvc.GetCurrentUser()}`;

                this._http.get(url).subscribe((res: any) => {
                    console.log('AnularNotaCliente resp', res);
                    resolve(res.AnularNotaClienteResult);
                }, (error) => {
                    console.error('Error AnularNotaCliente: ' + error.message);
                    notify('Error AnularNotaCliente: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }
}
