import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { FiltroGuiasInterface } from 'app/interfaces/filtro-guias.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FACT_URL } from 'app/config/config';
import { FacturasService } from 'app/servicios/facturas.service';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class GuiasService {

    guiasList = new Subject<Array<any>>();
    guiasCliente = new Subject<Array<any>>();
    guias: Array<any>;
    guiasCli: Array<any>;
    guiasSeleccionadas = new Subject<any[]>();
    guiasSeleccionadasCli = new Subject<any[]>();
    guiasSeleccionadosValor = new Subject<number>();
    buscandoGuias = new Subject<boolean>();
    buscandoGuiasCli = new Subject<boolean>();
    clienteCodigoSelec: string;

    constructor(public _http: HttpClient, public us: Utils, private fs: FacturasService,
        private authSrvc: AuthenticacionService) {
        this.guiasList.subscribe(lst => this.guias = lst);
        this.guiasCliente.subscribe(lst => this.guiasCli = lst);
        this.fs.detalleFacturaCliSelectedCod.subscribe(val => this.clienteCodigoSelec = val);
    }

    ObtenerGuias(filtro: FiltroGuiasInterface, isGuiasCli = false): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                if (isGuiasCli) { filtro.ClienteCodigo = this.clienteCodigoSelec }

                const url = FACT_URL + 'ObtenerGuiasParaFacturar';
                const body = {
                    filtroGuias: {
                        CiudadDestinoCodigo: filtro.CiudadDestinoCodigo || '',
                        CiudadOrigenCodigo: filtro.CiudadOrigenCodigo || '',
                        ClienteCodigo: filtro.ClienteCodigo || '',
                        ClienteNombre: filtro.ClienteNombre || '',
                        FechaFinal: filtro.FechaFinal || '',
                        FechaInicial: filtro.FechaInicial || '',
                        LineaNegocioCodigo: filtro.LineaNegocioCodigo || '',
                        LineaNegocioDescripcion: filtro.LineaNegocioDescripcion || '',
                        Referencia3: filtro.Referencia3 || ''
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                }
                // console.log('body guias');
                // console.log(body);

                this._http.post(url, body).subscribe((res: any) => {
                    // console.log("guias result");
                    // console.log(res.ObtenerGuiasParaFacturarResult.DTO);
                    if (res.ObtenerGuiasParaFacturarResult.Message.TypeEnum === 3) {
                        notify('Error obteniendo guias. Descripcion: ' + res.ObtenerGuiasParaFacturarResult.Message.Message, 'Error', 7000);
                        this.buscandoGuias.next(false);
                        return;
                    }
                    const guiasFechaOk = [];
                    res.ObtenerGuiasParaFacturarResult.DTO.forEach(guia => {
                        const guiaTemp = guia;
                        guiaTemp.FechaCreacion = this.us.ConvertJsonDateToString(guia.FechaDeCreacion || guia.FechaCreacion);
                        guiasFechaOk.push(guiaTemp);
                    });

                    if (!isGuiasCli) {
                        this.guiasList.next(guiasFechaOk);
                    } else {
                        this.guiasCliente.next(guiasFechaOk);
                    }

                    resolve(true);
                }, (error) => {
                    console.error('Error obteniendo guias: ' + error.message);
                    notify('Error obteniendo guias: ' + error.message, 'Error', 10000);
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    DetalleGuiaIngresos(guiaNum, isGuiasCli = false): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                if (!isGuiasCli) {
                    this.guias.forEach((guia: any) => {
                        if (guia.Numero === guiaNum) {
                            resolve(guia.Ingresos);
                        }
                    });
                } else {
                    this.guiasCli.forEach((guia: any) => {
                        if (guia.Numero === guiaNum) {
                            resolve(guia.Ingresos);
                        }
                    });
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    EstablecerGuiasNoFacturables(guias: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'EstablecerGuiasComoNoFacturable';
                const body = { guias: guias, codigoUsuario: this.authSrvc.GetCurrentUser() };
                // console.log('body no fact guias', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    })
            } catch (error) {
                reject(error);
                console.error('Error estableciendo guias no facturables. Descripcion: ' + error);
                notify('Error estableciendo guias no facturables. Descripcion: ' + error, 'Error', 7000);
            }
        });
    }

    FacturarGuias(guias: string[], observacion = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = FACT_URL + 'FacturarGuias';
                const body = { guias: guias, observacion: observacion, codigoUsuario: this.authSrvc.GetCurrentUser() };
                // console.log('body fact guias', body);
                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json') }).subscribe(resp => {
                        resolve(resp);
                    });
            } catch (error) {
                reject(error);
                console.error('Error facturando guias. Descripcion: ' + error);
                notify('Error facturando guias. Descripcion: ' + error, 'Error', 7000);
            }
        });
    }
}
