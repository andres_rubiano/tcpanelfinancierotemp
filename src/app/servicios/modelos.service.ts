import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from 'app/config/config';
import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Subject } from 'rxjs';
import { AuthenticacionService } from './autenticacion.service';

@Injectable()
export class ModelosService {

    modelosCliLista = new Subject<Array<any>>();
    modelosProvLista = new Subject<Array<any>>();
    selectedRowCli = new Subject<number>();
    selectedRowProv = new Subject<number>();

    constructor(public _http: HttpClient, private authSrvc: AuthenticacionService) {

    }

    ObtenerTarifasPorModelo(filtro: any): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const url = API_URL + 'ObtenerTarifasPorModelo';
                const body = {
                    filtrosTarifasPorModelo: {
                        EsquemaCodigo: filtro.EsquemaCodigo,
                        TipoEsquema: filtro.TipoEsquema,
                        ClienteCodigo: filtro.ClienteCodigo,
                        ProveedorCodigo: filtro.ProveedorCodigo,
                        ModeloCodigo: filtro.ModeloCodigo,
                        ArticuloCodigo: '',
                        CiudadOrigenCodigo: '',
                        CiudadDestinoCodigo: '',
                        TipoVehiculoCodigo: '',
                        TipoRutaCodigo: '',
                        EmpaqueOficialCodigo: '',
                        RangoMinimo: '0',
                        RangoMaximo: '0',
                        Valor: '0',
                        FechaInicial: '/Date(1496404800000)/',
                        FechaFinal: '/Date(1496404800000)/',
                        Estado: '0'
                    },
                    codigoUsuario: this.authSrvc.GetCurrentUser()
                };
                // console.log("body obtener excel por modelo");
                // console.log(body);

                this._http.post(url, body,
                    { headers: new HttpHeaders().set('content-type', 'application/json'), })
                    .subscribe((resp) => {
                        // console.log("resp obtener excel por modelo");
                        // console.log(resp);
                        resolve(resp);
                    }, (error) => {
                        console.error('Error obteniendo tarifas por modelo: ' + error.message);
                        notify('Error obteniendo tarifas por modelo: ' + error.message, 'Error', 10000);
                    });

            } catch (error) {
                reject(error);
            }
        });
    }
}