import { NgModule } from '@angular/core';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import { HeaderPrincipalComponent } from 'app/componentes-suppla/header-principal/header-principal.component';
import { PopupAccionConfirmacionComponent } from 'app/componentes-suppla/popups/accion-confirmacion/accion-confirmacion.component';
import { FechaPipe } from 'app/pipes/fecha.pipe';
import { ValorNegativoSinParentesisPipe } from 'app/pipes/valor-negativo-sin-parentesis.pipe';

@NgModule({
    declarations: [HeaderPrincipalComponent, PopupAccionConfirmacionComponent, FechaPipe, ValorNegativoSinParentesisPipe],
    exports: [HeaderPrincipalComponent, PopupAccionConfirmacionComponent, SplitPaneModule, FechaPipe, ValorNegativoSinParentesisPipe]
})

export class SharedModule { }
