export class TarifaInterface {
    Id: number;
    EsquemaCodigo: string;
    ClienteCodigo: string;
    ProveedorCodigo: string;
    ModeloCodigo: string;
    ArticuloCodigo: string;
    BatchNumero: string;
    CiudadOrigenCodigo: string;
    CiudadOrigenNombre: string;
    CiudadDestinoCodigo: string;
    CiudadDestinoNombre: string;
    TipoVehiculoCodigo: string;
    TipoVehiculoDescripcion: string;
    TipoRutaCodigo: string;
    TipoRutaDescripcion: string;
    EmpaqueOficialCodigo: string;
    EmpaqueOficialDescripcion: string;
    // RangoMinimo: number;
    // RangoMaximo: number;
    // Valor: number;
    FechaInicial: string;
    FechaFinal: string;
    Estado: string

    constructor(
        Id?: number,
        EsquemaCodigo?: string,
        ClienteCodigo?: string,
        ProveedorCodigo?: string,
        ModeloCodigo?: string,
        ArticuloCodigo?: string,
        BatchNumero?: string,
        CiudadOrigenCodigo?: any,
        CiudadOrigenNombre?: any,
        CiudadDestinoCodigo?: string,
        CiudadDestinoNombre?: string,
        TipoVehiculoCodigo?: string,
        TipoVehiculoDescripcion?: string,
        TipoRutaCodigo?: string,
        TipoRutaDescripcion?: string,
        EmpaqueOficialCodigo?: string,
        EmpaqueOficialDescripcion?: string,
        // RangoMinimo?: number,
        // RangoMaximo?: number,
        // Valor?: number,
        FechaInicial?: string,
        FechaFinal?: string,
        Estado?: string
    ){
        this.Id = Id;
        this.EsquemaCodigo = EsquemaCodigo;
        this.ClienteCodigo = ClienteCodigo;
        this.ProveedorCodigo = ProveedorCodigo;
        this.ModeloCodigo = ModeloCodigo;
        this.ArticuloCodigo = ArticuloCodigo;
        this.BatchNumero = BatchNumero;
        this.CiudadOrigenCodigo = CiudadOrigenCodigo;
        this.CiudadOrigenNombre = CiudadOrigenNombre;
        this.CiudadDestinoCodigo = CiudadDestinoCodigo;
        this.CiudadDestinoNombre = CiudadDestinoNombre;
        this.TipoVehiculoCodigo = TipoVehiculoCodigo;
        this.TipoVehiculoDescripcion = TipoVehiculoDescripcion;
        this.TipoRutaCodigo = TipoRutaCodigo;
        this.TipoRutaDescripcion = TipoRutaDescripcion;
        this.EmpaqueOficialCodigo = EmpaqueOficialCodigo;
        this.EmpaqueOficialDescripcion = EmpaqueOficialDescripcion;
        // this.RangoMinimo = RangoMinimo;
        // this.RangoMaximo = RangoMaximo;
        // this.Valor = Valor;
        this.FechaInicial = FechaInicial;
        this.FechaFinal = FechaFinal;
        this.Estado = Estado;
    }
}