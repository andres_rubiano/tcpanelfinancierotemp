export class FiltroManifiestoInterface{
    FechaInicial:string;
    FechaFinal:string;
    PoseedorCodigo:string;
    PoseedorNombre:string;
    Vehiculo:string;
    Sucursal:any;
    CiudadOrigenCodigo:any;
    CiudadDestinoCodigo:any;

    constructor(_FechaInicial?:string,
        _FechaFinal?:string,
        _PoseedorCodigo?:string,
        _PoseedorNombre?:string,
        _Vehiculo?:string,
        _Sucursal?:any,
        _CiudadOrigenCodigo?:any,
        _CiudadDestinoCodigo?:any){
            this.FechaInicial = _FechaInicial;
            this.FechaFinal = _FechaFinal;
            this.PoseedorCodigo = _PoseedorCodigo;
            this.PoseedorNombre = _PoseedorNombre;
            this.Vehiculo = _Vehiculo;
            this.Sucursal = _Sucursal;
            this.CiudadOrigenCodigo = _CiudadOrigenCodigo;
            this.CiudadDestinoCodigo = _CiudadDestinoCodigo;
    }
}