export class FiltroTarifaInterface {    
    CiudadOrigenCodigo: string;
    CiudadDestinoCodigo: string;
    TipoVehiculoCodigo: string;
    TipoRutaCodigo: string;
    EmpaqueOficialCodigo: string;
    FechaInicial: string;
    FechaFinal: string;
    Estado: string

    constructor(
        CiudadOrigenCodigo?: string,
        CiudadDestinoCodigo?: string,
        TipoVehiculoCodigo: string = "",
        TipoRutaCodigo: string = "",
        EmpaqueOficialCodigo: string = "",
        FechaInicial?: string,
        FechaFinal?: string,
        Estado: string = "1"){
            this.CiudadOrigenCodigo = CiudadOrigenCodigo;
            this.CiudadDestinoCodigo = CiudadDestinoCodigo;
            this.TipoVehiculoCodigo = TipoVehiculoCodigo;
            this.TipoRutaCodigo = TipoRutaCodigo;
            this.EmpaqueOficialCodigo = EmpaqueOficialCodigo;
            this.FechaInicial = FechaInicial;
            this.FechaFinal = FechaFinal;
            this.Estado = Estado;
    }
}