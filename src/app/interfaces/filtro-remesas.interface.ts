import { FiltroFacturasInterface } from 'app/interfaces/filtro-facturas.interface';

export class FiltroRemesasInterface extends FiltroFacturasInterface {

    LineaNegocioCodigo: string;
    LineaNegocioDescripcion: string;
    CiudadOrigenCodigo: any;
    CiudadDestinoCodigo: any;
    ReferenciaAdicional: string;

    constructor(_FechaInicial?: string,
        _FechaFinal?: string,
        _ClienteCodigo: string = '',
        _ClienteNombre: string = '',
        _LineaNegocioCodigo: string = '',
        _LineaNegocioDescripcion: string = '',
        _CiudadOrigenCodigo?: any,
        _CiudadDestinoCodigo?: any,
        _ReferenciaAdicional: string = '') {
        super();
        this.FechaInicial = _FechaInicial;
        this.FechaFinal = _FechaFinal;
        this.ClienteCodigo = _ClienteCodigo;
        this.ClienteNombre = _ClienteNombre;
        this.LineaNegocioCodigo = _LineaNegocioCodigo;
        this.LineaNegocioDescripcion = _LineaNegocioDescripcion;
        this.CiudadOrigenCodigo = _CiudadOrigenCodigo;
        this.CiudadDestinoCodigo = _CiudadDestinoCodigo;
        this.ReferenciaAdicional = _ReferenciaAdicional;

    }

}

