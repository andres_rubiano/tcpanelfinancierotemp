export class FiltroCostosInterface {
    FechaInicial: any;
    FechaFinal: any;
    ProveedorCodigo: string;
    ProveedorNombre: string;
    ArticuloDeTarifaCodigo: string;
    Vehiculo: string;
    CiudadOrigenCodigo: any;
    CiudadDestinoCodigo: any;
    Sucursal: any;

    constructor(_FechaInicial = '',
        _FechaFinal = '',
        _ProveedorCodigo = '',
        _ProveedorNombre = '',
        _ArticuloDeTarifaCodigo = '',
        _Vehiculo = '',
        _CiudadOrigenCodigo = '',
        _CiudadDestinoCodigo = '',
        _Sucursal = '') {
            this.FechaInicial = _FechaInicial;
            this.FechaFinal = _FechaFinal;
            this.ProveedorCodigo = _ProveedorCodigo;
            this.ProveedorNombre = _ProveedorNombre;
            this.ArticuloDeTarifaCodigo = _ArticuloDeTarifaCodigo;
            this.Vehiculo = _Vehiculo;
            this.CiudadOrigenCodigo = _CiudadOrigenCodigo;
            this.CiudadDestinoCodigo = _CiudadDestinoCodigo;
            this.Sucursal = _Sucursal;
    }
}
