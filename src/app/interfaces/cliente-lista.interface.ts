export class ClienteListaInterface {
    nombre: string;
    lineaNegocio: string;
    remesa: string;
    guia: string;
    esquema: string;
    FechaInicial: string;
    FechaFinal: string;

    constructor(
        nombre?: string,
        lineaNegocio?: string,
        remesa?: string,
        guia?: string,
        esquema?: string,
        FechaInicial?: string,
        FechaFinal?: string
    ) { 
        this.nombre = nombre;
        this.lineaNegocio = lineaNegocio;
        this.remesa = remesa;
        this.guia = guia;
        this.esquema = esquema;
        this.FechaInicial = FechaInicial;
        this.FechaFinal = FechaFinal;
    }
}