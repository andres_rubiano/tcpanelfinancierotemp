export class FiltroFacturasInterface {
    FechaInicial: any;
    FechaFinal: any;
    ClienteCodigo: string;
    ClienteNombre: string;

    constructor(_FechaInicial?: any,
        _FechaFinal?: any,
        _ClienteCodigo: string= '',
        _ClienteNombre: string= '') {

        this.FechaInicial = _FechaInicial;
        this.FechaFinal = _FechaFinal;
        this.ClienteCodigo = _ClienteCodigo;
        this.ClienteNombre = _ClienteNombre;
    }

}
