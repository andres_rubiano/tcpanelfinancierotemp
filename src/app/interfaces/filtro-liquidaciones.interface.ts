export class FiltroLiquidacionesInterface{
    FechaInicial:string;
    FechaFinal:string;
    ProveedorCodigo:string;
    ProveedorNombre:string;

    constructor(_FechaInicial:string = "",
        _FechaFinal:string = "",
        _ProveedorCodigo:string = "",
        _ProveedorNombre:string = "",){
            this.FechaInicial = _FechaInicial;
            this.FechaFinal = _FechaFinal;
            this.ProveedorCodigo = _ProveedorCodigo;
            this.ProveedorNombre = _ProveedorNombre;
    }
}