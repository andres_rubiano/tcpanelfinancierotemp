export class CiudadInterface {
    CiudadCodigo: string;
    CiudadDescripcion: string;
    DepartamentoDescripcion: string;

    constructor(codigo?: string, nombre?: string, dpto?: string) {
        this.CiudadCodigo = codigo;
        this.CiudadDescripcion = nombre;
        this.DepartamentoDescripcion = dpto;
    }
}