import { FiltroFacturasInterface } from 'app/interfaces/filtro-facturas.interface';

export class FiltroGuiasInterface extends FiltroFacturasInterface {

    LineaNegocioCodigo: string;
    LineaNegocioDescripcion: string;
    CiudadOrigenCodigo: any;
    CiudadDestinoCodigo: any;
    Referencia3: string;

    constructor(_FechaInicial?: any,
        _FechaFinal?: any,
        _ClienteCodigo: string = '',
        _ClienteNombre: string = '',
        _LineaNegocioCodigo: string = '',
        _LineaNegocioDescripcion: string = '',
        _CiudadOrigenCodigo?: any,
        _CiudadDestinoCodigo?: any,
        _Referencia3: string = '') {
            super();
        this.FechaInicial = _FechaInicial;
        this.FechaFinal = _FechaFinal;
        this.ClienteCodigo = _ClienteCodigo;
        this.ClienteNombre = _ClienteNombre;
        this.LineaNegocioCodigo = _LineaNegocioCodigo;
        this.LineaNegocioDescripcion = _LineaNegocioDescripcion;
        this.CiudadOrigenCodigo = _CiudadOrigenCodigo;
        this.CiudadDestinoCodigo = _CiudadDestinoCodigo;
        this.Referencia3 = _Referencia3;

    }

}
