export class Utils {

    public GetJsonDateFromYYYYMMDD(isOrigin: number, date: string): string {
        if (date.indexOf('-') === -1) {
            console.error('Formato de fecha errado para convertir en JsonDate');
            return '';
        }

        const fechaSplited = date.split('-');
        let fecha: number;
        if (isOrigin === 1) {// Fecha Origen
            fecha = Date.UTC(
                parseInt(fechaSplited[0]),
                parseInt(fechaSplited[1]) - 1,
                parseInt(fechaSplited[2])
            );
        } else { //Fecha Final
            fecha = Date.UTC(
                parseInt(fechaSplited[0]),
                parseInt(fechaSplited[1]) - 1,
                parseInt(fechaSplited[2]),
                23, 59, 0);
        }
        // console.log("fecha.getTime");
        // console.log(fecha.getTime());

        return `\/Date(${fecha})\/`;
    }

    public GetJsonDateFrom_jsDate(isOrigin: number, date: any): string {
        let fecha: number;
        if (isOrigin === 1) {// Fecha Origen
            fecha = Date.UTC(
                parseInt(date.year),
                parseInt(date.month) - 1,
                parseInt(date.day)
            );
        } else { //Fecha Final
            fecha = Date.UTC(
                parseInt(date.year),
                parseInt(date.month) - 1,
                parseInt(date.day),
                23, 59, 59);
        }
        return `\/Date(${fecha})\/`;
    }

    public ConvertJsonDateToString(jsonDate: string): string {

        if (jsonDate === undefined || jsonDate === null) {
            return '';
        }

        const monthNames = [
            'Ene', 'Feb', 'Mar',
            'Abr', 'May', 'Jun', 'Jul',
            'Ago', 'Sep', 'Oct',
            'Nov', 'Dic'
        ];
        const date = new Date(parseInt(jsonDate.replace('/Date(', '')));

        const monthIndex = date.getMonth();
        const year = date.getFullYear().toString();
        const hour = date.getHours().toString().length === 1 ? '0' + date.getHours().toString() : date.getHours().toString();
        const min = date.getMinutes().toString().length === 1 ? '0' + date.getMinutes().toString() : date.getMinutes().toString();
        const seg = date.getSeconds().toString().length === 1 ? '0' + date.getSeconds().toString() : date.getSeconds().toString();
        const finalDate = `${date.getDate().toString()}/${monthNames[monthIndex]}/${year} ${hour}:${min}:${seg}`;
        return finalDate;
    }

    public ValidaRangoFechas(fechaIni: any, fechaFin: any, dias: number = 365): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                let result = true;
                const dIni = new Date(fechaIni.year, fechaIni.month, fechaIni.day);
                const dFin = new Date(fechaFin.year, fechaFin.month, fechaFin.day);
                const diff = Math.abs(dIni.getTime() - dFin.getTime());
                const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                if (diffDays > dias) {
                    result = false;
                }

                resolve(result);
            } catch (error) {
                reject(error);
            }
        });
    }

    public ValidaFormatoFecha(fecha: any) {
        // console.log(fecha);
        if (!fecha.year || !fecha.month || !fecha.day) {
            return false; // no cumple formato datepicker
        }
        const fechaString = `${fecha.year}-${fecha.month.toString().length == 1 ? "0" + fecha.month : fecha.month}-${fecha.day.toString().length == 1 ? "0" + fecha.day : fecha.day}`;
        // console.log("fechaString");
        // console.log(fechaString);
        const regEx = /^\d{4}-\d{2}-\d{2}$/;
        if (!fechaString.match(regEx)) {return false; } // no cumple formato (yyyy-mm-dd)
        const d = new Date(fecha.year, fecha.month - 1, fecha.day);
        if (!d.getTime()) {return false; } // no es una fecha valida

        return true; // ok
    }

    public ValidaFormatoFacturacionEnBloque(cadena: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const regEx = /\s/g;
            if (cadena.match(regEx)) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    }
}
