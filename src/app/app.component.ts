import { CatalogosService } from 'app/servicios/catalogos.service';
import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

  constructor(cs: CatalogosService) {
  }

}
