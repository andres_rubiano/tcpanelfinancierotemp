import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// Import Containers
import {
  FullLayout,
  SimpleLayout
} from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '401',
    pathMatch: 'full',
  },  
  {
    path: '',
    component: FullLayout,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'facturacion/:sesion',
        loadChildren: './views/facturacion/facturacion.module#FacturacionModule'
      },
      {
        path: 'liquidacion/:sesion',
        loadChildren: './views/liquidacion/liquidacion.module#LiquidacionModule'
      },
      {
        path: 'tarifas/:sesion',
        loadChildren: './views/tarifas/tarifas.module#TarifasModule'
      },
      {
        path: '401',
        loadChildren: './views/view401/view401.module#View401Module'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
