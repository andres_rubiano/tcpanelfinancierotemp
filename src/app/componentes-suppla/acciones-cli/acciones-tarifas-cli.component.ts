import { TarifasService } from 'app/servicios/tarifas.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { Component, OnInit } from '@angular/core';
import { TC_URL, TC_BASE_URL } from '../../config/config';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'acciones-tarifas-cli',
    templateUrl: './acciones-tarifas-cli.component.html',
    styleUrls: ['./acciones-tarifas-cli.component.scss']
})
export class AccionesTarifasCliComponent implements OnInit {

    tarifasVencerIds: Array<number> = [];
    cantidadTarifasSelected: number = 0;
    codigoTarifaEditar: string;
    popupFechaVisible = false;
    tarifasCantidad: number = 0;
    hrefNuevo: string;
    tcBaseUrl = TC_BASE_URL;
    //parametros query string nueva tarifa
    esquemaSel: string = '';
    modeloSel: string = '';
    articuloSel: string = '';
    usuarioSel: string = '';

    constructor(public ts: TarifasService) { }

    ngOnInit() {
        this.ts.tarifasCliList.subscribe(lst => {
            this.tarifasCantidad = (lst != null && lst != undefined) ? lst.length : 0;
        });
        this.ts.tarifasSelectedCli.subscribe(val => {
            // console.log("tarifas selecc");
            // console.log(val);
            this.tarifasVencerIds = [];
            if (val[0] != undefined) {
                this.cantidadTarifasSelected = val.length;
                this.codigoTarifaEditar = val[0].Id.toString();
                val.forEach(tar => {
                    this.tarifasVencerIds.push(tar.Id);
                });
            } else {
                this.cantidadTarifasSelected = 0;
            }

        });
        this.ts.clienteCodigoSelected.subscribe(val => this.usuarioSel = val);
        this.ts.esquemaCliCodigoSelected.subscribe(val => this.esquemaSel = val);
        this.ts.modeloCliCodigoSelected.subscribe(val => this.modeloSel = val);
        this.ts.articuloCliCodigoSelected.subscribe(val => this.articuloSel = val);
    }

    NuevaTarifa() {
        this.hrefNuevo = `${TC_BASE_URL}/TC/Rating/frmServiceRatesPq.asp?TrrId=&Mode=N&RsmCode=${this.esquemaSel}&RmoCode=${this.modeloSel}&ArtCode=${this.articuloSel}&UcrCode=${this.usuarioSel}&TipoCliente=1`
        // console.log('url');
        // console.log(this.hrefNuevo);
        window.open(this.hrefNuevo, '_blank');
    }

    VencerPopup() {
        this.popupFechaVisible = true;
    }

    VencerTarifas(fecha) {

        // console.log("vencer to service");
        // console.log(fecha);
        // console.log(this.tarifasVencerIds);

        this.ts.VencerTarifas(fecha, this.tarifasVencerIds).then(resp => {
            // console.log("resp vencer");
            // console.log(resp);
            if (resp.VencerTarifasResult.TypeEnum.toString() === "1") {
                notify("Tarifas vencidas exitosamente!", "Success", 5000);
            } else {
                notify("Error venciendo tarifas! - Descripción: " + resp.VencerTarifasResult.Message, "Error", 5000);
            }

            this.popupFechaVisible = false;
        }, (res) => console.error(res));

    }

    ExportarExcel() {
        this.ts.exportarTarifas.next(true);
    }
}