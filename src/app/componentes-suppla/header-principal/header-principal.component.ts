import { Component, Input } from '@angular/core';

@Component({
    selector: 'header-principal',
    templateUrl: './header-principal.component.html',
    styleUrls: ['./header-principal.component.scss']
})
export class HeaderPrincipalComponent {

    @Input() tituloPagina: string;
    constructor() {
        
    }
}