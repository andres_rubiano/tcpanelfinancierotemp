import { FiltroRemesasInterface } from 'app/interfaces/filtro-remesas.interface';
import { FiltroGuiasInterface } from 'app/interfaces/filtro-guias.interface';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CatalogosService } from 'app/servicios/catalogos.service';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { RemesasService } from 'app/servicios/remesas.service';

@Component({
    selector: 'filtro-remesas',
    templateUrl: './filtro-remesas.component.html',
    styleUrls: ['./filtro-remesas.component.scss']
})
export class FiltroRemesasComponent implements OnInit {

    @ViewChild('ciuOrigen') ciuOrigen;
    @ViewChild('ciuDestino') ciuDestino;
    isCollapsed = false;
    filtroModel: FiltroRemesasInterface;
    ciudades: Array<CiudadInterface> = [];

    constructor(public cs: CatalogosService, public us: Utils, public rs:RemesasService) {
        cs.CargarCatalogos();
        this.filtroModel = new FiltroRemesasInterface();
    }

    ngOnInit() {
        this.cs.ciudades.subscribe(data => this.ciudades = data);

    }

    Buscar(model: FiltroRemesasInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            // console.log("model filtro remesas");
            // console.log(model);

            if (model.ClienteCodigo === '' && model.ClienteNombre === '') {
                notify('Debe suministrar un Cliente (Codigo y/o Nombre)', 'Warning', 5000);
                return;
            }

            this.isCollapsed = true;
            this.rs.buscandoRemesas.next(true);

            // console.log("model filtro remesas");
            // console.log(model);
            const filtro = new FiltroRemesasInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                model.ClienteCodigo, model.ClienteNombre, model.LineaNegocioCodigo, model.LineaNegocioDescripcion,
                model.CiudadOrigenCodigo, model.CiudadDestinoCodigo, model.ReferenciaAdicional);
            // console.log("filtro remesas");
            // console.log(filtro);

            this.rs.ObtenerRemesas(filtro).then(() => {
                this.rs.buscandoRemesas.next(false);
                this.rs.remesasSeleccionadas.next([]);
            },
                (error) => notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000));
        } catch (error) {
            notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000)
        }
    }

    CiudadSelectedOri(e) {
        // console.log(e);
        this.ciuOrigen.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    CiudadSelectedDes(e) {
        // console.log(e);
        this.ciuDestino.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }
}
