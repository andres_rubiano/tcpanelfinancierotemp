import { FiltroManifiestoInterface } from 'app/interfaces/filtro-manifiesto.interface';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { SucursalInterface } from 'app/interfaces/sucursal.interface';
import { CatalogosService } from 'app/servicios/catalogos.service';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'filtro-manifiestos',
    templateUrl: './filtro-manifiestos.component.html',
    styleUrls: ['./filtro-manifiestos.component.scss']
})
export class FiltroManifiestosComponent implements OnInit {

    @ViewChild('ciuOrigen') ciuOrigen;
    @ViewChild('ciuDestino') ciuDestino;
    @ViewChild('sucursal') sucursal;
    filtroModel: FiltroManifiestoInterface;

    private alert = new Subject<string>();
    isCollapsed = false;

    sucursales: Array<SucursalInterface> = [];
    ciudades: Array<CiudadInterface> = [];
    sucursalSelected: string;
    _gridSelectedRowKeysS: number[] = [3];

    constructor(public cs: CatalogosService, public ms: ManifiestosService, public us: Utils) {
        cs.CargarCatalogos();
        this.filtroModel = new FiltroManifiestoInterface();
    }

    ngOnInit() {
        this.cs.ciudades.subscribe(data => this.ciudades = data);
        this.cs.sucursales.subscribe(data => this.sucursales = data);
    }

    Buscar(model: FiltroManifiestoInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            this.isCollapsed = true;
            this.ms.buscandoManifiestos.next(true);
            // console.log('model filtro manif', model);
            const filtro = new FiltroManifiestoInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                model.PoseedorCodigo, model.PoseedorNombre, model.Vehiculo,
                this.sucursal.selectedItem == null || this.sucursal.selectedItem.SubCode === undefined ?
                    '' : this.sucursal.selectedItem.SubCode,
                this.ciuOrigen.selectedItem == null || this.ciuOrigen.selectedItem.CiudadCodigo === undefined ?
                    '' : this.ciuOrigen.selectedItem.CiudadCodigo,
                this.ciuDestino.selectedItem == null || this.ciuDestino.selectedItem.CiudadCodigo === undefined ?
                    '' : this.ciuDestino.selectedItem.CiudadCodigo);
            // console.log('filtro manifiesto', filtro);

            this.ms.ObtenerManifiestos(filtro).then(() => {
                this.ms.buscandoManifiestos.next(false);
                this.ms.manifSeleccionados.next([]);
            },
                (error) => notify('Error obteniendo manifiestos! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo manifiestos! - Descripcion: ' + error, 'Error', 6000));
        } catch (error) {
            notify('Error obteniendo manifiestos! - Descripcion: ' + error, 'Error', 6000);
            this.ms.buscandoManifiestos.next(false);
        }

    }

    CiudadSelectedOri(e) {
        // console.log(e);
        this.ciuOrigen.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    CiudadSelectedDes(e) {
        // console.log(e);
        this.ciuDestino.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    SucursalSelected(e) {
        // console.log(e);
        this.sucursal.selectedItem.SubCode = e.itemData.SubCode;
    }

}