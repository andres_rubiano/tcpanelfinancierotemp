import { Component, Input, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';
import { FacturasService } from 'app/servicios/facturas.service';
import { FiltroRemesasInterface } from 'app/interfaces/filtro-remesas.interface';
import { RemesasService } from 'app/servicios/remesas.service';

const now = new Date();

@Component({
    selector: 'filtro-remesas-cliente',
    templateUrl: './filtro-remesas-cliente.component.html',
    styleUrls: ['./filtro-remesas-cliente.component.scss']
})
export class FiltroRemesasClienteComponent implements OnInit {

    @Input() clienteCod: string;
    filtroModel: FiltroRemesasInterface;
    isCollapsed = false;

    constructor(public rs: RemesasService, public us: Utils) {
        this.filtroModel = new FiltroRemesasInterface();
    }

    ngOnInit() {
        // console.log('prov inyectado' + this.proveedorCodigo);
        this.filtroModel.FechaFinal = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        const anoIni = now.getMonth() === 0 ? now.getFullYear() - 1 : now.getFullYear();
        const mesIni = now.getMonth() === 0 ? 12 : now.getMonth();
        this.filtroModel.FechaInicial = { year: anoIni, month: mesIni, day: now.getDate() };
        this.Buscar(this.filtroModel);
    }

    Buscar(model: FiltroRemesasInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            // console.log("model filtro remesas");
            // console.log(model);

            this.rs.buscandoRemesasCli.next(true);

            this.isCollapsed = true;
            const filtro = new FiltroRemesasInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                this.clienteCod, '', '', '', '', '', '');
            // console.log('filtro remesas cli');
            // console.log(filtro);

            this.rs.ObtenerRemesas(filtro, true).then(() => this.rs.buscandoRemesasCli.next(false),
                (error) => notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000));

        } catch (error) {
            notify('Error obteniendo remesas! - Descripcion: ' + error, 'Error', 6000);
            this.rs.buscandoRemesasCli.next(false);
        }
    }
}
