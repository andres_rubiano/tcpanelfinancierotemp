import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FiltroGuiasInterface } from 'app/interfaces/filtro-guias.interface';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import { CatalogosService } from 'app/servicios/catalogos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { GuiasService } from 'app/servicios/guias.service';

@Component({
    selector: 'filtro-guias',
    templateUrl: './filtro-guias.component.html',
    styleUrls: ['./filtro-guias.component.scss']
})
export class FiltroGuiasComponent implements OnInit {

    @ViewChild('ciuOrigen') ciuOrigen;
    @ViewChild('ciuDestino') ciuDestino;
    isCollapsed = false;
    filtroModel: FiltroGuiasInterface;
    ciudades: Array<CiudadInterface> = [];

    constructor(public cs: CatalogosService, public us: Utils, public gs: GuiasService) {
        cs.CargarCatalogos();
        this.filtroModel = new FiltroGuiasInterface();
    }

    ngOnInit() {
        this.cs.ciudades.subscribe(data => this.ciudades = data);
    }

    Buscar(model: FiltroGuiasInterface) {
        // console.log('model guias');
        // console.log(model);

        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            // console.log("model filtro guias");
            // console.log(model);

            if (model.ClienteCodigo === '' && model.ClienteNombre === '') {
                notify('Debe suministrar un Cliente (Codigo y/o Nombre)', 'Warning', 5000);
                return;
            }

            this.isCollapsed = true;
            this.gs.buscandoGuias.next(true);

            // console.log("model filtro Guias");
            // console.log(model);
            const filtro = new FiltroGuiasInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                model.ClienteCodigo, model.ClienteNombre, model.LineaNegocioCodigo, model.LineaNegocioDescripcion,
                model.CiudadOrigenCodigo, model.CiudadDestinoCodigo, model.Referencia3);
            // console.log("filtro Guias");
            // console.log(filtro);

            this.gs.ObtenerGuias(filtro).then(() => {
                this.gs.buscandoGuias.next(false);
                this.gs.guiasSeleccionadas.next([]);
            },
                (error) => notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000));

        } catch (error) {
            notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000);
            this.gs.buscandoGuias.next(false);
        }
    }

    CiudadSelectedOri(e) {
        this.ciuOrigen.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    CiudadSelectedDes(e) {
        this.ciuDestino.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }
}
