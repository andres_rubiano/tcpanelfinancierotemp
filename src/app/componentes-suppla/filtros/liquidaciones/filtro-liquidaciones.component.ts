import { FiltroLiquidacionesInterface } from 'app/interfaces/filtro-liquidaciones.interface';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'filtro-liquidaciones',
    templateUrl: './filtro-liquidaciones.component.html',
    styleUrls: ['./filtro-liquidaciones.component.scss']
})
export class FiltroLiquidacionesComponent implements OnInit {

    filtroModel: FiltroLiquidacionesInterface;
    isCollapsed: boolean = false;

    constructor(public ls: LiquidacionesService, public us: Utils) { }

    ngOnInit() {
        this.filtroModel = new FiltroLiquidacionesInterface();
        this.ls.refrescarLiquidaciones.subscribe((refrescar) => {
            if(refrescar){
                this.Buscar(this.filtroModel);
            }
        });
    }

    Buscar(model: FiltroLiquidacionesInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify("La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.", "Warning", 6000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify("La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.", "Warning", 6000);
                return;
            }
            this.isCollapsed = true;
            this.ls.buscandoLiquidaciones.next(true);
            // console.log("model filtro Liquidaciones");
            // console.log(model);
            let filtro = new FiltroLiquidacionesInterface(
                model.FechaInicial != undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : "",
                model.FechaFinal != undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : "",
                model.ProveedorCodigo, model.ProveedorNombre);
            // console.log("filtro Liquidaciones");
            // console.log(filtro);

            this.ls.ObtenerLiquidaciones(filtro).then(() => this.ls.buscandoLiquidaciones.next(false),
                (error) => notify("Error obteniendo Liquidaciones! - Descripcion: " + error, "Error", 6000))
                .catch((error) => notify("Error obteniendo Liquidaciones! - Descripcion: " + error, "Error", 6000));
            this.ls.detalleLiquidacion.next([]);
        } catch (error) {
            notify("Error obteniendo Liquidaciones! - Descripcion: " + error, "Error", 6000);
            this.ls.buscandoLiquidaciones.next(false);
        }
    }
}