import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FiltroFacturasInterface } from 'app/interfaces/filtro-facturas.interface';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FacturasService } from 'app/servicios/facturas.service';

@Component({
    selector: 'filtro-facturas',
    templateUrl: './filtro-facturas.component.html',
    styleUrls: ['./filtro-facturas.component.scss']
})
export class FiltroFacturasComponent implements OnInit {

    isCollapsed = false;
    filtroModel: FiltroFacturasInterface;

    constructor(public us: Utils, public fs: FacturasService) {
        this.filtroModel = new FiltroFacturasInterface();
    }

    ngOnInit() {
        this.fs.refrescarFacturas.subscribe(val => {
            if (val) {
                this.Buscar(this.filtroModel);
            }
        });
    }

    Buscar(model) {
        try {
            this.filtroModel = model;
            this.fs.buscandoFacturas.next(true);
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            // console.log("model filtro facturas");
            // console.log(model);


            this.isCollapsed = true;
            this.fs.buscandoFacturas.next(true);

            // console.log("model filtro facturas");
            // console.log(model);
            const filtro = new FiltroFacturasInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                model.ClienteCodigo, model.ClienteNombre);
            // console.log("filtro facturas");
            // console.log(filtro);

            this.fs.ObtenerFacturas(filtro).then(() => {
                this.fs.buscandoFacturas.next(false);
            },
                (error) => notify('Error obteniendo facturas! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo facturas! - Descripcion: ' + error, 'Error', 6000));

        } catch (error) {
            notify('Error obteniendo facturas! - Descripcion: ' + error, 'Error', 6000)
        }
    }
}
