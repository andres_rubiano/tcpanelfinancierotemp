import { TarifasFiltroCliComponent } from 'app/componentes-suppla/filtros/tarifas-filtro-cli/tarifas-filtro-cli.component';
import { Component, Input, OnInit, ViewChild, ElementRef, Output } from '@angular/core';
import { TipoBusquedaEnum } from 'app/enumeraciones/tipo-busqueda.enum';
import { ProveedoresService } from 'app/servicios/proveedores.service';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ClientesService } from 'app/servicios/clientes.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { debounceTime } from 'rxjs/operator/debounceTime';
import notify from 'devextreme/ui/notify';
import { Subject } from 'rxjs/Subject';
import { debounce } from 'rxjs/operator/debounce';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'filtro-clientes-proveedores',
    templateUrl: './filtro-clientes-proveedores.component.html',
    styleUrls: ['./filtro-clientes-proveedores.component.scss']
})
export class FiltroClientesProveedoresComponent implements OnInit {

    alertType: string;
    labelText: string;

    @ViewChild('nombre') nombre: ElementRef;
    @ViewChild('codigo') codigo: ElementRef;
    @Input() tipoBusqueda: TipoBusquedaEnum;

    constructor(public cs: ClientesService, public ps: ProveedoresService, public as: ArticulosService,
        public ts: TarifasService, public tfc: TarifasFiltroCliComponent, public ms: ModelosService) { }

    ngOnInit() {
        if (this.tipoBusqueda === TipoBusquedaEnum.Cliente) {
            this.labelText = 'cliente'
        } else { this.labelText = 'proveedor' }
    }

    Buscar(codigo, nombre, esquema) {
        this.ts.isCollapsedObs.next(true);

        if (!codigo.value && !nombre.value && this.tipoBusqueda === TipoBusquedaEnum.Cliente) {
            notify(`Debe suministrar Código y/o Nombre no pueden ir vacíos simultaneamente.`, 'Warning', 5000);
            this.nombre.nativeElement.classList.add('error-campo-filtro')
            this.codigo.nativeElement.classList.add('error-campo-filtro');
            this.cs.buscandoClientes.next(false);
            this.cs.clientesLista.next([]);
            return;
        } else {
            this.nombre.nativeElement.classList.remove('error-campo-filtro')
            this.codigo.nativeElement.classList.remove('error-campo-filtro');
        }

        if (this.tipoBusqueda === TipoBusquedaEnum.Cliente) {
            this.ts.clienteCodigoSelected.next(codigo.value || '');
            this.ts.nuevaBusquedaCli.next(true);
            this.ts.esquemaCliCodigoSelected.next('');
            this.ts.modeloCliCodigoSelected.next('')
            this.ts.articuloCliCodigoSelected.next('');
            this.ts.tarifasCliList.next([]);
            this.ms.modelosCliLista.next([]);
            this.as.articulosCliLista.next([]);
            this.cs.buscandoClientes.next(true);
            this.cs.CargarClientesLista(codigo.value, nombre.value, esquema.value).then(val => {

                // console.log('lista clientes');
                // console.log(val);
                const data = val;
                if (data.length < 1) {
                    notify(`No se encontraron registros para estos criterios de busqueda!`, 'Info', 2000);
                }
                this.cs.clientesLista.next(data);
                this.cs.buscandoClientes.next(false);

            }, (err) => alert(err)).catch(err => alert(err));
        } else if (this.tipoBusqueda === TipoBusquedaEnum.Proveedor) {
            this.ts.proveedorCodigoSelected.next(codigo.value || '');
            this.ts.nuevaBusquedaProv.next(true);
            this.ts.esquemaProvCodigoSelected.next('');
            this.ts.modeloProvCodigoSelected.next('')
            this.ts.articuloProvCodigoSelected.next('');            
            this.ts.tarifasProvList.next([]);
            this.ps.buscandoProveedores.next(true);
            this.ms.modelosProvLista.next([]);
            this.as.articulosProvLista.next([]);
            this.ps.CargarProveedoresLista(codigo.value, nombre.value, esquema.value).then(val => {
                const data = val;
                if (data.length < 1) {
                    notify(`No se encontraron registros para estos criterios de busqueda!`, 'Info', 2000);
                }
                this.ps.buscandoProveedores.next(false);
                this.ps.proveedoresLista.next(data);
            });
        }
    }
}
