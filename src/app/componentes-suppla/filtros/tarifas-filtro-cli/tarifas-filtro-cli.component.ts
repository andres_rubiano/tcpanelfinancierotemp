import { FiltroRequeridoInterface } from 'app/interfaces/filtro-requerido.interface';
import { FiltroTarifaInterface } from 'app/interfaces/filtro-tarifas.interface';
import { NombresFiltrosEnum } from 'app/enumeraciones/nombres-filtros.enum';
import { VehiculoInterface } from 'app/interfaces/vehiculo.interface';
import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { EmpaqueInterface } from 'app/interfaces/empaque.interface';
import { CatalogosService } from 'app/servicios/catalogos.service';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import { TarifaInterface } from 'app/interfaces/tarifa.interface';
import { TarifasService } from 'app/servicios/tarifas.service';
import { RutaInterface } from 'app/interfaces/ruta.interface';
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tarifas-filtro-cli',
    templateUrl: './tarifas-filtro-cli.component.html',
    styleUrls: ['./tarifas-filtro-cli.component.scss']
})
export class TarifasFiltroCliComponent implements OnInit {

    @ViewChild('ciuOrigen') ciuOrigen;
    @ViewChild('ciuDestino') ciuDestino;
    @Output() ocultarTbl: EventEmitter<boolean> = new EventEmitter();
    public filtroModel: FiltroTarifaInterface;
    isCollapsed = true;
    ciudades: Array<CiudadInterface> = [];
    vehiculos: Array<VehiculoInterface>;
    rutas: Array<RutaInterface>;
    empaques: Array<EmpaqueInterface>;
    model = new TarifaInterface();
    display: boolean;
    shown: boolean;
    titleBtnExpandir: string;

    //#region observables request tarifa
    tipoEsquema: TipoEsquemaEnum;
    esquemaCodigoSelected: string;
    clienteCodigoSelected: string;
    proveedorCodigoSelected: string;
    modeloCodigoSelected: string;
    articuloCodigoSelected: string;
    //#endregion

    //#region filtros requeridos
    filtrosTarifas: Array<FiltroRequeridoInterface>;
    ciudadOrigenReq: number;
    ciudadDestinoReq: number;
    vehiculoReq: number;
    rutaReq: number;
    empaqueReq: number;
    minReq: number;
    maxReq: number;
    valorReq: number;
    fechaIniReq: number;
    fechaFinReq: number;
    estadoReq: number;
    //#endregion

    constructor(public cs: CatalogosService, public ts: TarifasService, public us: Utils) {
        cs.CargarCatalogos();
        this.filtroModel = new FiltroTarifaInterface();
    }

    ngOnInit() {
        this.ts.nuevaBusquedaCli.subscribe(bool => {
            if (bool) {
                this.ResetearFiltros();
            }
        });
        this.ts.isCollapsedObs.subscribe(bool => this.isCollapsed = bool);
        this.cs.ciudades.subscribe(data => {
            this.ciudades = data;
            // console.log(data);

        });
        this.cs.vehiculos.subscribe(data => this.vehiculos = data);
        this.cs.rutas.subscribe(data => this.rutas = data);
        this.cs.empaques.subscribe(data => this.empaques = data);

        // subscripcion observables request tarifa
        this.ts.tipoEsquema.subscribe(data => this.tipoEsquema = data);
        this.ts.esquemaCliCodigoSelected.subscribe(data => this.esquemaCodigoSelected = data);
        this.ts.clienteCodigoSelected.subscribe(data => this.clienteCodigoSelected = data);
        this.ts.modeloCliCodigoSelected.subscribe(data => this.modeloCodigoSelected = data);
        this.ts.articuloCliCodigoSelected.subscribe(data => this.articuloCodigoSelected = data);

        this.ts.filtrosTarifasCli.subscribe(filtros => {
            this.filtrosTarifas = filtros;
            this.AplicarRequeridoEnFiltros(this.filtrosTarifas);
        });

    }

    // private ObtenerTarifas(ciudadOrigen, ciudadDestino, vehiculo, ruta, empaque, min, max, valor, fechaIni, fechaFin, estado) {
    private ObtenerTarifas(filtroModel) {
        // console.log('filtroModel', filtroModel);
        if (filtroModel.FechaInicial === undefined || filtroModel.FechaFinal === undefined) {
            return;
        }
        if (!this.us.ValidaFormatoFecha(filtroModel.FechaInicial)) {
            notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
            return;
        }
        if (!this.us.ValidaFormatoFecha(filtroModel.FechaFinal)) {
            notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
            return;
        }
        this.us.ValidaRangoFechas(filtroModel.FechaInicial, filtroModel.FechaFinal).then(res => {
            if (!res) {
                notify('El rango de fechas no puede ser mayor a un año!', 'Warning', 6000);
                return;
            }
            this.ts.buscandoTarifasCli.next(true);
            const model = new TarifaInterface(0, this.esquemaCodigoSelected, this.clienteCodigoSelected, this.proveedorCodigoSelected,
                this.modeloCodigoSelected, this.articuloCodigoSelected, null,
                this.ciuOrigen.selectedItem == null || this.ciuOrigen.selectedItem.CiudadCodigo === undefined ? "" : this.ciuOrigen.selectedItem.CiudadCodigo, null,
                this.ciuDestino.selectedItem == null || this.ciuDestino.selectedItem.CiudadCodigo === undefined ? "" : this.ciuDestino.selectedItem.CiudadCodigo,
                null, filtroModel.TipoVehiculoCodigo, null, filtroModel.TipoRutaCodigo, null, filtroModel.EmpaqueOficialCodigo, null,
                // min.value, max.value, valor.value,
                filtroModel.FechaInicial == undefined || filtroModel.FechaInicial == '' ? '' : this.us.GetJsonDateFrom_jsDate(1, filtroModel.FechaInicial),
                filtroModel.FechaFinal == undefined || filtroModel.FechaFinal == '' ? '' : this.us.GetJsonDateFrom_jsDate(2, filtroModel.FechaFinal),
                filtroModel.Estado);
            // console.log(this.us.ConvertJsonDateToString(this.us.GetJsonDateFromYYYYMMDD(1,fechaFin.value)));

            // console.log("model");
            // console.log(model);
            if (this.esquemaCodigoSelected === '') {
                notify('Debe seleccionar un artículo para poder consultar sus tarifas!', 'Warning', 5000);
                this.ts.buscandoTarifasCli.next(false);
                return;
            }
            this.ts.ObtenerTarifas(model)
                .then(list => {
                    if (list.length === 0) {
                        notify('No hay tarifas disponibles para estos criterios de busqueda!', 'Info', 2000)
                    } else if (list.length > 15) {
                        this.ocultarTbl.emit(true);
                        this.shown = true;
                        this.display = !this.display;
                        this.titleBtnExpandir = 'Expandir Tabla Clientes';
                    }
                    //console.log('next ', list);
                    this.ts.tarifasCliList.next(list);
                    this.isCollapsed = true;
                    this.ts.buscandoTarifasCli.next(false);
                }).catch(err => { console.error(err); this.ts.buscandoTarifasCli.next(false); });
        });
    }

    CiudadSelectedOri(e) {
        this.ciuOrigen.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    CiudadSelectedDes(e) {
        this.ciuDestino.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    ExpandirTarifas() {
        if (this.display) {
            this.ocultarTbl.emit(false);
            this.titleBtnExpandir = 'Contraer Tabla Clientes';
        } else {
            this.ocultarTbl.emit(true);
            this.titleBtnExpandir = 'Expandir Tabla Clientes';
        }
        this.display = !this.display;
    }

    AplicarRequeridoEnFiltros(filtros) {
        // console.log("filtros");
        // console.log(filtros);
        this.ResetearFiltros();
        filtros.forEach(fltr => {
            switch (fltr.ParametroDescripcion) {
                case NombresFiltrosEnum.CiudadOrigenCodigo:
                    fltr.CoincidenciaExacta ? this.ciudadOrigenReq = 1 : this.ciudadOrigenReq = 2;
                    break;
                case NombresFiltrosEnum.CiudadDestinoCodigo:
                    fltr.CoincidenciaExacta ? this.ciudadDestinoReq = 1 : this.ciudadDestinoReq = 2;
                    break;
                case NombresFiltrosEnum.TipoVehiculoCodigo:
                    fltr.CoincidenciaExacta ? this.vehiculoReq = 1 : this.vehiculoReq = 2;
                    break;
                case NombresFiltrosEnum.TipoRutaCodigo:
                    fltr.CoincidenciaExacta ? this.rutaReq = 1 : this.rutaReq = 2;
                    break;
                case NombresFiltrosEnum.EmpaqueOficialCodigo:
                    fltr.CoincidenciaExacta ? this.empaqueReq = 1 : this.empaqueReq = 2;
                    break;
                case NombresFiltrosEnum.RangoMinimo:
                    fltr.CoincidenciaExacta ? this.minReq = 1 : this.minReq = 2;
                    break;
                case NombresFiltrosEnum.RangoMaximo:
                    fltr.CoincidenciaExacta ? this.maxReq = 1 : this.maxReq = 2;
                    break;
                case NombresFiltrosEnum.Valor:
                    fltr.CoincidenciaExacta ? this.valorReq = 1 : this.valorReq = 2;
                    break;
                case NombresFiltrosEnum.FechaInicial:
                    fltr.CoincidenciaExacta ? this.fechaIniReq = 1 : this.fechaIniReq = 2;
                    break;
                case NombresFiltrosEnum.FechaFinal:
                    fltr.CoincidenciaExacta ? this.fechaFinReq = 1 : this.fechaFinReq = 2;
                    break;
                case NombresFiltrosEnum.Estado:
                    fltr.CoincidenciaExacta ? this.estadoReq = 1 : this.estadoReq = 2;
                    break;

                default:
                    break;
            }
        });
    }

    Limpiar() {
        this.filtroModel = new FiltroTarifaInterface();
        this.ResetearFiltros();
    }

    public ResetearFiltros() {
        this.ciudadOrigenReq = 0;
        this.ciudadDestinoReq = 0;
        this.vehiculoReq = 0;
        this.rutaReq = 0;
        this.empaqueReq = 0;
        this.minReq = 0;
        this.maxReq = 0;
        this.valorReq = 0;
        this.fechaIniReq = 0;
        this.fechaFinReq = 0;
        this.estadoReq = 0;
    }
}
