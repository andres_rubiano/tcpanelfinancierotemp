import { Component, Input, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';
import { FiltroGuiasInterface } from 'app/interfaces/filtro-guias.interface';
import { GuiasService } from 'app/servicios/guias.service';
import { FacturasService } from 'app/servicios/facturas.service';

const now = new Date();

@Component({
    selector: 'filtro-guias-cliente',
    templateUrl: './filtro-guias-cliente.component.html',
    styleUrls: ['./filtro-guias-cliente.component.scss']
})
export class FiltroGuiasClienteComponent implements OnInit {

    @Input() clienteCod: string;
    filtroModel: FiltroGuiasInterface;
    isCollapsed = false;

    constructor(public fs: FacturasService, public gs: GuiasService, public us: Utils) {
        this.filtroModel = new FiltroGuiasInterface();
    }

    ngOnInit() {
        // console.log('prov inyectado' + this.proveedorCodigo);
        this.filtroModel.FechaFinal = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        const anoIni = now.getMonth() === 0 ? now.getFullYear() - 1 : now.getFullYear();
        const mesIni = now.getMonth() === 0 ? 12 : now.getMonth();
        this.filtroModel.FechaInicial = { year: anoIni, month: mesIni, day: now.getDate() };
        this.Buscar(this.filtroModel);
    }

    Buscar(model: FiltroGuiasInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 5000);
                return;
            }
            // console.log("model filtro guias");
            // console.log(model);

            this.gs.buscandoGuiasCli.next(true);

            this.isCollapsed = true;
            const filtro = new FiltroGuiasInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                this.clienteCod, '', '', '', '', '', '');
            // console.log("filtro guias");
            // console.log(filtro);

            this.gs.ObtenerGuias(filtro, true).then(() => this.gs.buscandoGuiasCli.next(false),
                (error) => notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000));

        } catch (error) {
            notify('Error obteniendo guias! - Descripcion: ' + error, 'Error', 6000);
            this.gs.buscandoGuiasCli.next(false);
        }
    }
}
