import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { SucursalInterface } from 'app/interfaces/sucursal.interface';
import { CostosService } from 'app/servicios/costos.service';
import { Component, Input, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';

const now = new Date();

@Component({
    selector: 'filtro-costos-proveedor',
    templateUrl: './filtro-costos-proveedor.component.html',
    styleUrls: ['./filtro-costos-proveedor.component.scss']
})
export class FiltroCostosProveedorComponent implements OnInit {

    @Input() proveedorCodigo: string;
    filtroModel: FiltroCostosInterface;
    isCollapsed = false;

    constructor(public ls: LiquidacionesService, public cos: CostosService, public us: Utils) {
        this.filtroModel = new FiltroCostosInterface();
    }

    ngOnInit() {
        // console.log('prov inyectado' + this.proveedorCodigo);
        this.filtroModel.FechaFinal = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        const anoIni = now.getMonth() === 0 ? now.getFullYear() - 1 : now.getFullYear();
        const mesIni = now.getMonth() === 0 ? 12 : now.getMonth();
        this.filtroModel.FechaInicial = { year: anoIni, month: mesIni, day: now.getDate() };
        this.Buscar(this.filtroModel);
    }

    Buscar(model: FiltroCostosInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            // console.log("model filtro costos");
            // console.log(model);

            this.cos.buscandoCostosProv.next(true);

            this.isCollapsed = true;
            const filtro = new FiltroCostosInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                this.proveedorCodigo, '', '', '', '', '', '');
            // console.log("filtro costos");
            // console.log(filtro);

            this.cos.ObtenerCostos(filtro, true).then(() => this.cos.buscandoCostosProv.next(false),
                (error) => notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000));

        } catch (error) {
            notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000);
            this.cos.buscandoCostosProv.next(false);
        }
    }
}
