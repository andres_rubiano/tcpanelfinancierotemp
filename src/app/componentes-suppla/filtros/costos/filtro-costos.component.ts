import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { SucursalInterface } from 'app/interfaces/sucursal.interface';
import { CatalogosService } from 'app/servicios/catalogos.service';
import { CiudadInterface } from 'app/interfaces/ciudad.interface';
import { CostosService } from 'app/servicios/costos.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { Subject } from 'rxjs/Subject';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'filtro-costos',
    templateUrl: './filtro-costos.component.html',
    styleUrls: ['./filtro-costos.component.scss']
})
export class FiltroCostosComponent implements OnInit {

    @ViewChild('ciuOrigen') ciuOrigen;
    @ViewChild('ciuDestino') ciuDestino;
    @ViewChild('sucursal') sucursal;
    filtroModel: FiltroCostosInterface;
    isCollapsed = false;

    sucursales: Array<SucursalInterface> = [];
    ciudades: Array<CiudadInterface> = [];
    sucursalSelected: string;
    _gridSelectedRowKeysS: number[] = [3];

    constructor(public cs: CatalogosService, public cos: CostosService, public us: Utils) {
        cs.CargarCatalogos();
        this.filtroModel = new FiltroCostosInterface();
    }

    ngOnInit() {
        this.cs.ciudades.subscribe(data => this.ciudades = data);
        this.cs.sucursales.subscribe(data => this.sucursales = data);
    }

    Buscar(model: FiltroCostosInterface) {
        try {
            if (!this.us.ValidaFormatoFecha(model.FechaInicial)) {
                notify('La fecha Inicial no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            if (!this.us.ValidaFormatoFecha(model.FechaFinal)) {
                notify('La fecha Final no cumple el formato (aaaa-mm-dd) o no es una fecha válida.', 'Warning', 6000);
                return;
            }
            // console.log("model filtro costos");
            // console.log(model);

            if (model.ProveedorCodigo === '' && model.ProveedorNombre === '') {
                notify('Debe suministrar un Proveedor (Codigo y/o Nombre)', 'Warning', 6000);
                return;
            }

            this.cos.buscandoCostos.next(true);
            this.isCollapsed = true;
            const filtro = new FiltroCostosInterface(
                model.FechaInicial !== undefined ? this.us.GetJsonDateFrom_jsDate(1, model.FechaInicial) : '',
                model.FechaFinal !== undefined ? this.us.GetJsonDateFrom_jsDate(2, model.FechaFinal) : '',
                model.ProveedorCodigo, model.ProveedorNombre, model.ArticuloDeTarifaCodigo, model.Vehiculo,
                // tslint:disable-next-line:max-line-length
                this.ciuOrigen.selectedItem == null || this.ciuOrigen.selectedItem.CiudadCodigo === undefined ? '' : this.ciuOrigen.selectedItem.CiudadCodigo,
                this.ciuDestino.selectedItem == null || this.ciuDestino.selectedItem.CiudadCodigo === undefined ? '' : this.ciuDestino.selectedItem.CiudadCodigo,
                // tslint:disable-next-line:max-line-length
                this.sucursal.selectedItem == null || this.sucursal.selectedItem.SubCode === undefined ? '' : this.sucursal.selectedItem.SubCode);
            // console.log("filtro costos");
            // console.log(filtro);

            this.cos.ObtenerCostos(filtro).then(() => this.cos.buscandoCostos.next(false),
                (error) => notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000))
                .catch((error) => notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000));
        } catch (error) {
            notify('Error obteniendo costos! - Descripcion: ' + error, 'Error', 6000);
            this.cos.buscandoCostos.next(false);
        }
    }

    CiudadSelectedOri(e) {
        // console.log(e);
        this.ciuOrigen.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    CiudadSelectedDes(e) {
        // console.log(e);
        this.ciuDestino.selectedItem.CiudadCodigo = e.itemData.CiudadCodigo;
    }

    SucursalSelected(e) {
        // console.log(e);
        this.sucursal.selectedItem.SubCode = e.itemData.SubCode;
    }
}
