import { TarifasFiltroProvComponent } from 'app/componentes-suppla/filtros/tarifas-filtro-prov/tarifas-filtro-prov.component';
import { ProveedoresService } from 'app/servicios/proveedores.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'tabla-proveedores',
    templateUrl: './tabla-proveedores.component.html',
    styleUrls: ['./tabla-proveedores.component.scss']
})
export class TablaProveedoresComponent implements OnInit {

    @Input() proveedoresLista: Array<any>; 
    @ViewChild('gridPro') dataGridProv;  
    mostrarFiltro:boolean = false;
    heightTabla:number;
    withAnimationVisible = false;
    idOnMouseOver: string;
    esqDescripcion: string;

    constructor(public ps:ProveedoresService, public ms: ModelosService, public as:ArticulosService, 
        public ts: TarifasService, public tfp:TarifasFiltroProvComponent) { 
    }

    ngOnInit() { 
        this.ps.buscandoProveedores.subscribe(val => {
            if (val) {
                this.dataGridProv.instance.beginCustomLoading("Cargando...");
            } else {
                this.dataGridProv.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    ObtenerModelosPorProveedor() {          
        this.ts.modeloProvCodigoSelected.next('');
        this.ms.selectedRowProv.next(-1);
        let row = this.dataGridProv.selectedRowKeys;        
        this.ts.tarifasProvList.next([]);
        if (row != undefined && row[0] != undefined) {
            this.ts.proveedorCodigoSelected.next(row[0].ProveedorCodigo);
            this.ts.esquemaProvCodigoSelected.next(row[0].EsquemaCodigo);
        this.ms.modelosProvLista.next(row[0].Modelos);
        }
        this.as.articulosProvLista.next([]);
        this.ts.articuloProvCodigoSelected.next('');
        this.tfp.ResetearFiltros();
        this.ts.isCollapsedObs.next(true);        
        this.ts.tarifasProvList.next([]);
    }    
    
    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 5;
    }
    
    toggleWithAnimation(esq) {
        if (esq != undefined) {
            this.idOnMouseOver = '#' + esq.ConfiguracionId;
            this.esqDescripcion = esq.EsquemaDescripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }

    calculateCellValueEsquema(data) {
        return data;
    }
    
    onCellPrepared(e) {
        if (e.rowType == 'header')
            e.cellElement.css('background-color', '#edf5ff');
    }
}