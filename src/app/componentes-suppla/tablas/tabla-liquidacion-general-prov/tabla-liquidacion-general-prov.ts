
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { Utils } from 'app/tools/utils';
import notify from 'devextreme/ui/notify';
import { DxTextBoxComponent } from 'devextreme-angular';

@Component({
    selector: 'tabla-liq-gral-prov',
    templateUrl: `./tabla-liquidacion-general-prov.html`
})
export class TablaLiquidacionGeneralProvComponent {

    @ViewChild('textRef') textRef: DxTextBoxComponent;
    @Input() liqGeneral;

    constructor(public liq: LiquidacionesService, public us: Utils) { }

    ActualizarReferenciaExterna() {
        console.log();
        
        this.liq.ActualizarReferenciaExterna(this.liqGeneral.Numero, this.textRef.value).then((resp: any) => {
            if (resp.TypeEnum === 3) {
                notify('Error actualizando numero de factura proveedor, Descripcion: ' + resp.Message, 'Error', 10000);
            } else if (resp.TypeEnum === 1) {
                notify('Numero de factura proveedor actualizado exitosamente!', 'Success', 3000);
            }
            this.liq.refrescarLiquidacionesDet.next(true);
        });
    }

}
