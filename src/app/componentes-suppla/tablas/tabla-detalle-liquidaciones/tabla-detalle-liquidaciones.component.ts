import { PopupAccionConfirmacionComponent } from 'app/componentes-suppla/popups/accion-confirmacion/accion-confirmacion.component';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { TC_BASE_URL, PORCENTAJE_ALTO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { log } from 'util';
import { AuthenticacionService } from '../../../servicios/autenticacion.service';

@Component({
    selector: 'tabla-detalle-liquidaciones',
    templateUrl: './tabla-detalle-liquidaciones.component.html',
    styleUrls: ['./tabla-detalle-liquidaciones.component.scss']
})
export class TablaDetalleLiquidacionesComponent implements OnInit {


    @ViewChild('gridLiquidacionesDet') gridLiquidacionesDet;
    @ViewChild('popConf') confirmacion: PopupAccionConfirmacionComponent;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;
    mostrarAddCostosViaje = false;
    mostrarAddCostos = false;
    mostrarRemoverCostos = false;
    mostrarCerrarLiq = false;
    mostrarAnularLiq = false;
    mostrarImpresion = false;
    popupVisible: boolean;
    popupVisibleConfirm: boolean;
    popupVisibleNota: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    widthPopup: number;
    liqNumero: string;
    liqId: string;
    provCod: string;
    provNombre: string;
    totalLiquidar: number;
    mostrarBtnCrearNotaProv: boolean;
    infoCrearNotaProv: any;

    constructor(public ls: LiquidacionesService, public us: Utils, public cs: CostosService
        , private authSrvc: AuthenticacionService) { }

    ngOnInit() {
        this.widthPopup = window.screen.width / 1.2
        this.ls.estadoLiquidacion.subscribe(val => this.BotonesPorEstado(val));
        this.ls.detalleLiquidacionNum.subscribe(val => this.liqNumero = val);
        this.ls.detalleLiquidacionId.subscribe(val => this.liqId = val);
        this.ls.detalleLiquidacionProvSelected.subscribe(val => this.provCod = val);
        this.ls.detalleLiquidacionProvSelectedNombre.subscribe(val => this.provNombre = val);
        this.ls.totalLiquidacionesDet.subscribe(val => this.totalLiquidar = val);
        this.ls.buscandoLiquidacionesDet.subscribe(val => {
            if (val) {
                this.gridLiquidacionesDet.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridLiquidacionesDet.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 1.55;
        this.widthTabla = window.screen.width / 2.15;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddCostosViaje:
                    this.popupVisibleConfirm = false;
                    this.RecalcularTotal();
                    this.cs.costosPorProveedor.next([]);
                    break;
                case OperacionesAConfirmar.RemoverCostos:
                    this.gridLiquidacionesDet.instance.beginCustomLoading('Removiendo...');
                    this.popupVisibleConfirm = false;
                    const detallesSelec = [];
                    this.gridLiquidacionesDet.selectedRowKeys.forEach(det => {
                        detallesSelec.push(det.Id);
                    });

                    this.cs.RemoverCostos(detallesSelec).then((res) => {
                        // console.log(res);
                        if (res.TypeEnum === 1) {
                            notify('Costos removidos exitosamente!', 'Success', 3000);
                            this.ls.refrescarLiquidacionesDet.next(true);
                            this.RecalcularTotal();
                            this.gridLiquidacionesDet.instance.endCustomLoading();
                        } else if (res.TypeEnum === 3) {
                            notify('Error removiendo costos. Descripción: ' + res.Message, 'Error', 6000);
                            this.gridLiquidacionesDet.instance.endCustomLoading();
                        }
                    });
                    break;
                case OperacionesAConfirmar.AnularLiquidacion:
                    this.popupVisibleConfirm = false;
                    this.ls.AnularLiquidacion(this.liqNumero).then((resp) => {
                        if (resp.TypeEnum === 1) {
                            notify('Liquidación anulada exitosamente!', 'Success', 3000);
                            this.ls.refrescarLiquidaciones.next(true);
                            this.mostrarBtnCrearNotaProv = false;
                        } else if (resp.TypeEnum === 3) {
                            notify('Error anulando Liquidación. Descripción: ' + resp.Message, 'Error', 6000);
                        }
                    });
                    break;
                case OperacionesAConfirmar.CerrarLiquidacion:
                    this.popupVisibleConfirm = false;
                    this.ls.CerrarLiquidacion(this.liqNumero).then((resp) => {
                        // console.log("resp.TypeEnum");
                        // console.log(resp.TypeEnum);

                        if (resp.TypeEnum === 1) {
                            notify('Liquidación cerrada exitosamente!', 'Success', 3000);
                            this.ls.refrescarLiquidaciones.next(true);
                            this.ls.refrescarLiquidacionesDet.next(true);
                            this.mostrarBtnCrearNotaProv = false;
                        } else if (resp.TypeEnum === 3) {
                            notify('Error cerrando Liquidación. Descripción: ' + resp.Message, 'Error', 6000);
                        }
                    });
                    break;

                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    AddCostosViaje() {
        this.popupVisible = true;
    }

    RemoverCostos() {
        if (this.gridLiquidacionesDet.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos un detalle!', 'Warning', 5000);
            return;
        }
        this.pregunta = 'Realmente desea remover costos de la liquidación #' + this.liqNumero + '?';
        this.operacion = OperacionesAConfirmar.RemoverCostos;
        this.popupVisibleConfirm = true;
    }

    AgregarNota(costoId: string) {
        this.ls.ObtenerInfoNotaProveedor(costoId).then(resp => {
            if (resp.Message.TypeEnum === 3) {
                notify('Error: ' + resp.Message.Message, 'Error', 7000);
                return;
            } else if (resp.Message.TypeEnum === 1) {
                this.infoCrearNotaProv = resp;
                this.popupVisibleNota = true;
            }
        });
    }

    NotaCreada(e) {
        if (e) {
            this.popupVisibleNota = false;
            this.ls.refrescarLiquidacionesDet.next(true);
        }
    }

    Imprimir() {
        // tslint:disable-next-line:max-line-length
        const url = `${TC_BASE_URL}/SICLONET/ReportView.aspx?settings=liquidationId=${this.liqId}&report=FormatLiquidation&usercode=${this.authSrvc.GetCurrentUser()}`;
        window.open(url, '_blank');
    }

    AnularLiquidacion() {
        this.pregunta = 'Realmente desea anular la liquidación #' + this.liqNumero + '?';
        this.operacion = OperacionesAConfirmar.AnularLiquidacion;
        this.popupVisibleConfirm = true;
    }

    CerrarLiquidacion() {
        this.pregunta = 'Realmente desea cerrar la liquidación #' + this.liqNumero + '?';
        this.operacion = OperacionesAConfirmar.CerrarLiquidacion;
        this.popupVisibleConfirm = true;
    }

    BotonesPorEstado(val: EstadosLiquidacionEnum) {
        switch (val) {
            case EstadosLiquidacionEnum.Anulado:
                this.mostrarBtnCrearNotaProv = false;
                this.mostrarAddCostosViaje = false;
                this.mostrarAddCostos = false;
                this.mostrarRemoverCostos = false;
                this.mostrarCerrarLiq = false;
                this.mostrarAnularLiq = false;
                this.mostrarImpresion = false;
                break;
            case EstadosLiquidacionEnum.Abierto:
                this.mostrarBtnCrearNotaProv = true;
                this.mostrarAddCostosViaje = true;
                this.mostrarAddCostos = true;
                this.mostrarRemoverCostos = true;
                this.mostrarCerrarLiq = true;
                this.mostrarAnularLiq = true;
                this.mostrarImpresion = true;
                break;
            case EstadosLiquidacionEnum.Preliquidado:
                this.mostrarBtnCrearNotaProv = false;
                this.mostrarAddCostosViaje = true;
                this.mostrarAddCostos = true;
                this.mostrarRemoverCostos = true;
                this.mostrarCerrarLiq = true;
                this.mostrarAnularLiq = false;
                this.mostrarImpresion = true;
                break;
            case EstadosLiquidacionEnum.Verificado:
                this.mostrarBtnCrearNotaProv = false;
                this.mostrarAddCostosViaje = false;
                this.mostrarAddCostos = false;
                this.mostrarRemoverCostos = false;
                this.mostrarCerrarLiq = true;
                this.mostrarAnularLiq = false;
                this.mostrarImpresion = true;
                break;
            case EstadosLiquidacionEnum.Liquidado:
            case EstadosLiquidacionEnum.Cerrado:
                this.mostrarBtnCrearNotaProv = false;
                this.mostrarAddCostosViaje = false;
                this.mostrarAddCostos = false;
                this.mostrarRemoverCostos = false;
                this.mostrarCerrarLiq = false;
                this.mostrarAnularLiq = false;
                this.mostrarImpresion = true;
                break;

            default:
                break;
        }
    }

    RecalcularTotal(): void {
        let recalcularTotal = 0;
        this.dataSource.forEach(row => {
            recalcularTotal += row.Total;
        });
        this.ls.totalLiquidacionesDet.next(recalcularTotal);
    }
}
