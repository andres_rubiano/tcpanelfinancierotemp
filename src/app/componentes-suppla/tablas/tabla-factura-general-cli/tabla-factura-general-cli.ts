
import { Component, Input } from '@angular/core';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tabla-fact-gral-cli',
    templateUrl: `./tabla-factura-general-cli.html`
})
export class TablaFacturaGeneralCliComponent {

    @Input() factGeneral;

    constructor(public us: Utils) { }

}
