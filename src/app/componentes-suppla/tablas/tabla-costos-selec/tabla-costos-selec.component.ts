import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { TC_BASE_URL, PORCENTAJE_ALTO } from 'app/config/config';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { DetalleViajeService } from 'app/servicios/detalle-viaje.service';

@Component({
    selector: 'tabla-costos-selec',
    templateUrl: './tabla-costos-selec.component.html',
    styleUrls: ['./tabla-costos-selec.component.scss']
})
export class TablaCostosSelecComponent implements OnInit {

    @ViewChild('gridCostos') gridCostos;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    mostrarFiltro: boolean = false;
    heightTabla: number;
    popupVisible: boolean = false;
    costosValorTotal: number;
    tcUrl = TC_BASE_URL;

    constructor(public cs: CostosService, public ms: ManifiestosService, public us: Utils, public dvs: DetalleViajeService) { }

    ngOnInit() {
        this.cs.costosSeleccionadosValor.subscribe(val => this.costosValorTotal = val);
        this.cs.buscandoCostos.subscribe(val => {
            if (val) {
                this.gridCostos.instance.beginCustomLoading("Cargando...");
            } else {
                this.gridCostos.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / 2.1;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }

    }

    RemoverSelec() {
        if (this.gridCostos.selectedRowKeys.length === 0) {
            notify('Debe seleccionar los costos a remover!', 'Warning', 6000);
            return;
        }
        this.gridCostos.selectedRowKeys.forEach(row => {
            this.dataSource.forEach(rowDs => {
                if (rowDs === row) {
                    this.dataSource.splice(this.dataSource.findIndex(x => x.Viaje === rowDs.Viaje), 1);
                }
            });
        });
        this.cs.costosSeleccionados.next(this.dataSource);
        let costosSeleccionadosValor = 0;
        this.dataSource.forEach((costo: any) => {
            costosSeleccionadosValor += parseInt(costo.Costo);
        });
        this.cs.costosSeleccionadosValor.next(costosSeleccionadosValor);
    }

    Detalle(viajeNum) {
        // console.log("viajeNum");
        // console.log(viajeNum.data.Viaje);
        this.ms.DetalleViaje(viajeNum.data.Viaje).then(det => {
            if (det) {
                this.dvs.detalle = det;
                this.popupVisible = true;
            }
        });
    }
    calculateCellValueDetalle(data) {
        return data;
    }

    LiquidarCostos() {
        if (this.gridCostos.dataSource.length === 0) {
            notify('No hay costos seleccionados para liquidar!', 'Warning', 6000);
            return;
        }
        const costosAliquidar: Array<string> = [];
        this.dataSource.forEach(csto => {
            costosAliquidar.push(csto.CostoId);
        });
        // console.log("viajesAliquidar");
        // console.log(viajesAliquidar);

        this.cs.LiquidarCostos(costosAliquidar).then(res => {
            // console.log('response liquidar costos');
            // console.log(res);
            if (res.CrearLiquidacionResult.Message.TypeEnum === 1) {
                this.cs.costosPorLiquidar.next([]);
                this.cs.costosSeleccionados.next([]);
                notify('Costos enviados a liquidación exitosamente! No. Transacción ' +
                    res.CrearLiquidacionResult.Message.TransactionNumber, 'Success', 5000);
            } else {
                notify('Error liquidando costos!' + res.CrearLiquidacionResult.Message.Message, 'Error', 5000);
            }
        });
    }
}