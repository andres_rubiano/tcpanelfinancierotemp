import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FacturasService } from 'app/servicios/facturas.service';
import { EstadosFacturacionEnum } from 'app/enumeraciones/estados-facturacion.enum';

@Component({
    selector: 'tabla-facturas',
    templateUrl: './tabla-facturas.component.html',
    styleUrls: ['./tabla-facturas.component.scss']
})
export class TablaFacturasComponent implements OnInit {

    @ViewChild('gridFacturas') gridFacturas;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    filaActual: any;
    mostrarFiltro = false;
    heightTabla: number;
    popupVisible = false;
    tcUrl = TC_BASE_URL;

    constructor(public fs: FacturasService, public us: Utils) { }

    ngOnInit() {
        this.calculeHeightTabla();
        this.fs.buscandoFacturas.subscribe(val => {
            if (val) {
                this.gridFacturas.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridFacturas.instance.endCustomLoading();
            }
        });
        this.fs.refrescarDetalle.subscribe(val => {
            if (val) {
                this.DetalleFactura(this.filaActual);
            }
        });
    }

    DetalleFactura(row) {
        if (row && row.values[0]) {
            this.fs.buscandoFacturasDet.next(true);
            this.filaActual = row;
            this.fs.detalleFacturaCliSelectedCod.next(row.data.ClienteCodigo);
            this.fs.detalleFacturaCliSelectedNombre.next(row.data.ClienteNombre);
            this.fs.detalleFacturaId.next(row.data.Id);
            this.fs.detalleFacturaNumero.next(row.values[0]);
            this.ObtenerEstadoFacturacion(row.data.Estado);
            this.fs.DetalleFactura(row.values[0]).then(det => {
                if (det) {
                    if (det.length === 0) {
                        notify('No hay detalle disponible para esta factura!', 'Info', 2000);
                        this.fs.totalFacturasDet.next(0);
                        this.fs.detalleFacturas.next([]);
                        this.fs.buscandoFacturasDet.next(false);
                        return;
                    }
                    let total = 0;
                    det.Detalle.forEach(dt => {
                        total += parseInt(dt.IngresoTotal);
                    });
                    this.fs.totalFacturasDet.next(total);
                    this.fs.detalleFacturas.next(det);
                }
                this.fs.buscandoFacturasDet.next(false);
            }, (error) => {
                notify('Error obteniendo detalle! ' + error, 'Error', 6000);
                this.fs.buscandoFacturasDet.next(false);
            }).catch((error) => {
                notify('Error obteniendo detalle! ' + error, 'Error', 6000);
                this.fs.buscandoFacturasDet.next(false);
            });
        }
    }

    ObtenerEstadoFacturacion(estado: string) {
        switch (estado) {
            case EstadosFacturacionEnum.Anulada.toString():
                this.fs.estadoFacturacion.next(EstadosFacturacionEnum.Anulada);
                break;
            case EstadosFacturacionEnum.Definitiva.toString():
                this.fs.estadoFacturacion.next(EstadosFacturacionEnum.Definitiva);
                break;
            case EstadosFacturacionEnum.PreFacturada.toString():
                this.fs.estadoFacturacion.next(EstadosFacturacionEnum.PreFacturada);
                break;
            default:
                break;
        }
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
