import { Component, OnInit, Input, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { TC_BASE_URL } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { TablaDetalleFacturasComponent } from 'app/componentes-suppla/tablas/tabla-detalle-facturas/tabla-detalle-facturas.component';
import { RemesasService } from 'app/servicios/remesas.service';

@Component({
    selector: 'tabla-remesas-cliente',
    templateUrl: './tabla-remesas-cliente.component.html',
    styleUrls: ['./tabla-remesas-cliente.component.scss']
})
export class TablaRemesasClienteComponent implements OnInit {

    @ViewChild('gridRemesasCli') gridRemesasCli;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Input() factNumero: string;
    @Output() remesasCliSelec: EventEmitter<any[]> = new EventEmitter();
    mostrarFiltro = false;
    heightTabla: number;
    popupVisible = false;
    popupVisibleConfirm: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;
    ingresosLista: any;

    constructor(public rs: RemesasService, public us: Utils,
        public tdfc: TablaDetalleFacturasComponent) { }

    ngOnInit() {
        this.rs.buscandoRemesasCli.subscribe(val => {
            if (val) {
                this.gridRemesasCli.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridRemesasCli.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    Respuesta(e) {
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddRemesas:
                    this.popupVisibleConfirm = false;
                    this.remesasCliSelec.emit(this.gridRemesasCli.selectedRowKeys);
                    this.tdfc.popupVisibleRemesas = false;
                    this.tdfc.Respuesta(e);
                    this.dataSource = [];
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    AdicionarRemesas() {
        if (this.gridRemesasCli.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos una remesa!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea agregar remesas a la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.AddRemesas;
        this.popupVisibleConfirm = true;
    }

    Detalle(data) {
        this.rs.DetalleRemesasIngresos(data.Numero, true).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }
    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 2.5;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {e.cellElement.css('background-color', '#edf5ff')}
    }
}
