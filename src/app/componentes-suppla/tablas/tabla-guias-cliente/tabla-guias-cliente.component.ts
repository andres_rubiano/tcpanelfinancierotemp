import { Component, OnInit, Input, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { TC_BASE_URL } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { GuiasService } from 'app/servicios/guias.service';
import { TablaDetalleFacturasComponent } from 'app/componentes-suppla/tablas/tabla-detalle-facturas/tabla-detalle-facturas.component';

@Component({
    selector: 'tabla-guias-cliente',
    templateUrl: './tabla-guias-cliente.component.html',
    styleUrls: ['./tabla-guias-cliente.component.scss']
})
export class TablaGuiasClienteComponent implements OnInit {

    @ViewChild('gridGuiasCli') gridGuiasCli;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Input() factNumero: string;
    @Output() guiasCliSelec: EventEmitter<any[]> = new EventEmitter();
    mostrarFiltro = false;
    heightTabla: number;
    popupVisible = false;
    popupVisibleConfirm: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;
    ingresosLista: any;

    constructor(public gs: GuiasService, public us: Utils,
        public tdfc: TablaDetalleFacturasComponent) { }

    ngOnInit() {
        this.gs.buscandoGuiasCli.subscribe(val => {
            if (val) {
                this.gridGuiasCli.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridGuiasCli.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    Respuesta(e) {
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddGuias:
                    this.popupVisibleConfirm = false;
                    this.guiasCliSelec.emit(this.gridGuiasCli.selectedRowKeys);
                    this.tdfc.popupVisibleGuias = false;
                    this.tdfc.Respuesta(e);
                    this.dataSource = [];
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    AdicionarGuias() {
        if (this.gridGuiasCli.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos una guia!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea agregar guias a la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.AddGuias;
        this.popupVisibleConfirm = true;
    }

    Detalle(data) {
        this.gs.DetalleGuiaIngresos(data.Numero, true).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }
    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 2.5;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {e.cellElement.css('background-color', '#edf5ff')}
    }
}
