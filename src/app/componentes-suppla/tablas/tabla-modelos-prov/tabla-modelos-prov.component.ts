import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'tabla-modelos-prov',
    templateUrl: './tabla-modelos-prov.component.html',
    styleUrls: ['./tabla-modelos-prov.component.scss']
})
export class TablaModelosProvComponent implements OnInit {

    @Input() modelosLista: Array<any>;
    _selectedRow: number;
    withAnimationVisible = false;
    idOnMouseOver: string;
    modDescripcion: string;
    
    //#region observables request tarifa
    tipoEsquema: TipoEsquemaEnum;
    esquemaCodigoSelected: string;
    proveedorCodigoSelected: string;
    modeloCodigoSelected: string;
    //#endregion

    constructor(public ms: ModelosService, public as: ArticulosService,
        public ts: TarifasService) { }

    ngOnInit() {  
      this.ms.selectedRowProv.subscribe(val => this._selectedRow = val);    
      this.ms.selectedRowProv.next(-1);
      //subscripcion observables request tarifa
      this.ts.tipoEsquema.subscribe(data => this.tipoEsquema = data);
      this.ts.esquemaProvCodigoSelected.subscribe(data => this.esquemaCodigoSelected = data);
      this.ts.proveedorCodigoSelected.subscribe(data => this.proveedorCodigoSelected = data);
      this.ts.modeloProvCodigoSelected.subscribe(data => this.modeloCodigoSelected = data);
    }

    ObtenerArticulosPorModelo(index) {
        this.as.selectedRowProv.next(-1)
        this.ms.selectedRowProv.next(index);
        this.ts.tarifasProvList.next([]);   
        this.ts.articuloProvCodigoSelected.next('');    
        this.ts.modeloProvCodigoSelected.next(this.modelosLista[index].Codigo)
        this.as.articulosProvLista.next(this.modelosLista[index].Articulos);
    }
    
    ExportarTarifasAsoc(mod,index){
        this.ObtenerArticulosPorModelo(index);
        let filtro = {
            EsquemaCodigo: this.esquemaCodigoSelected,
            TipoEsquema: TipoEsquemaEnum.Proveedor,
            ClienteCodigo: "",
            ProveedorCodigo:this.proveedorCodigoSelected,
            ModeloCodigo:this.modeloCodigoSelected
        };
        this.ms.ObtenerTarifasPorModelo(filtro).then((resp) => {
            // console.log(resp);
            if(resp.ObtenerTarifasPorModeloResult.TypeEnum == "1"){
                notify("El proceso de exportación ha sido enviado a la cola de tareas, revise el listado de descargas!","Success",5000);
            } else {
                notify("Error al exportar tarifas por modelo","Error",6000);
            }
        });
    }

    toggleWithAnimation(mod) {
        if (mod != undefined) {
            this.idOnMouseOver = '#'+mod.Codigo;
            this.modDescripcion = mod.Descripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }
}