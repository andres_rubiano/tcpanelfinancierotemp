import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { ArticulosService } from 'app/servicios/articulos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'tabla-articulos-cli',
    templateUrl: './tabla-articulos-cli.component.html',
    styleUrls: ['./tabla-articulos-cli.component.scss']
})
export class TablaArticulosCliComponent implements OnInit {

    @Input() articulosLista: Array<any> = [];
    _selectedRow: number;
    modelSelected: string;
    withAnimationVisible = false;
    idOnMouseOver: string;
    artDescripcion: string;

    //#region observables request tarifa
    tipoEsquema: TipoEsquemaEnum;
    esquemaCodigoSelected: string;
    clienteCodigoSelected: string;
    modeloCodigoSelected: string;
    articuloCodigoSelected: string;
    //#endregion


    constructor(public as: ArticulosService,
        public ts: TarifasService) { }

    ngOnInit() {
        this.as.selectedRowCli.subscribe(val => this._selectedRow = val);
        this.as.selectedRowCli.next(-1);
        this.ts.modeloCliCodigoSelected.subscribe(val => this.modelSelected = val);
        //subscripcion observables request tarifa
        this.ts.tipoEsquema.subscribe(data => this.tipoEsquema = data);
        this.ts.esquemaCliCodigoSelected.subscribe(data => this.esquemaCodigoSelected = data);
        this.ts.clienteCodigoSelected.subscribe(data => this.clienteCodigoSelected = data);
        this.ts.modeloCliCodigoSelected.subscribe(data => this.modeloCodigoSelected = data);
        this.ts.articuloCliCodigoSelected.subscribe(data => this.articuloCodigoSelected = data);
    }

    ObtenerTarifasPorArticulo(index) {
        this.as.selectedRowCli.next(index);
        this.ts.articuloCliCodigoSelected.next(this.articulosLista[index].Codigo);
        this.ts.isCollapsedObs.next(false);
        this.ts.tarifasCliList.next([]);
        this.ts.RequeridosFiltrosPorTarifa(this.modelSelected, this.articulosLista[index].Codigo)
            .then(filtros => this.ts.filtrosTarifasCli.next(filtros));
    }

    ExportarTarifasAsoc(art, index) {
        this.as.selectedRowCli.next(index);
        this.ts.articuloCliCodigoSelected.next(this.articulosLista[index].Codigo);
        let filtro = {
            EsquemaCodigo: this.esquemaCodigoSelected,
            TipoEsquema: TipoEsquemaEnum.Cliente,
            ClienteCodigo: this.clienteCodigoSelected,
            ProveedorCodigo: "",
            ModeloCodigo: this.modeloCodigoSelected,
            ArticuloCodigo: this.articuloCodigoSelected
        };
        this.as.ObtenerTarifasPorArticulo(filtro).then((resp) => {
            // console.log(resp);

            if (resp.ObtenerTarifasPorArticuloResult.TypeEnum == "1") {
                notify("El proceso de exportación ha sido enviado a la cola de tareas, revise el listado de descargas!", "Success", 5000);
            } else {
                notify("Error al exportar tarifas por articulo", "Error", 6000);
            }
        });
    }

    toggleWithAnimation(art) {
        if (art != undefined) {
            this.idOnMouseOver = '#' + art.Codigo;
            this.artDescripcion = art.Descripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }
}