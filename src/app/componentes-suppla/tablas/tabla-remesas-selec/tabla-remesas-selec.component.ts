import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { RemesasService } from 'app/servicios/remesas.service';
import notify from 'devextreme/ui/notify';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';

@Component({
    selector: 'tabla-remesas-selec',
    templateUrl: './tabla-remesas-selec.component.html',
    styleUrls: ['./tabla-remesas-selec.component.scss']
})
export class TablaRemesasSelecComponent implements OnInit {

    @ViewChild('gridRemesasSel') gridRemesasSel;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
    heightTabla: number;
    popupVisible = false;
    remesasValorTotal: number;
    ingresosLista: any;
    popupVisibleConfirm = false;
    popupVisibleObservacion = false;
    observacionTxt: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;

    constructor(public rs: RemesasService) {
    }

    ngOnInit() {
        this.calculeHeightTabla();
        this.rs.remesasSeleccionadosValor.subscribe(val => this.remesasValorTotal = val);
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    Detalle(data) {
        this.rs.DetalleRemesasIngresos(data.Numero).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }

    RemoverSelec() {
        if (this.gridRemesasSel.selectedRowKeys.length === 0) {
            notify('Debe seleccionar las remesas a remover!', 'Warning', 5000);
            return;
        }

        this.gridRemesasSel.selectedRowKeys.forEach(row => {
            this.dataSource.forEach(rowDs => {
                if (rowDs === row) {
                    this.dataSource.splice(this.dataSource.findIndex(x => x.Numero === rowDs.Numero), 1);
                }
            });
        });
        this.rs.remesasSeleccionadas.next(this.dataSource);

        let remesasSeleccionadosValor = 0;
        this.dataSource.forEach((costo: any) => {
            remesasSeleccionadosValor += parseInt(costo.Total);
        });
        this.rs.remesasSeleccionadosValor.next(remesasSeleccionadosValor);
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.Facturar:
                    this.observacionTxt = '';
                    this.popupVisibleObservacion = true;
                    break;

                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    ObservacionOK() {
        this.popupVisibleConfirm = false;
        this.popupVisibleObservacion = false;
        const remesas = [];
        this.dataSource.forEach(remesa => {
            remesas.push(remesa.Numero.toString());
        });
        this.rs.FacturarRemesas(remesas, this.observacionTxt).then((resp: any) => {
            // console.log('resp fact remesas', resp.FacturarRemesasResult);
            if (resp.FacturarRemesasResult.Message.TypeEnum === 3) {
                notify('Error Facturando remesas, Descripcion: ' +
                    resp.FacturarRemesasResult.Message.Message, 'Error', 10000);

                if (resp.FacturarRemesasResult.DTO && resp.FacturarRemesasResult.DTO.length > 0) {
                    notify('Novedades: ' + resp.FacturarRemesasResult.DTO.toString(), 'Warning', 10000);
                }
            } else if (resp.FacturarRemesasResult.Message.TypeEnum === 1) {
                const transNum = resp.FacturarRemesasResult.Message.TransactionNumber;
                notify('Remesas Facturadas Exitosamente!. No. Transaccion: ' + transNum, 'Success', 3000);
                this.refreshData.emit(true);
                this.rs.remesasSeleccionadosValor.next(0);
            }
        });
        this.popupVisibleConfirm = false;
    }

    FacturarRemesas() {
        if (!this.dataSource || this.dataSource.length === 0) {
            notify('No hay remesas seleccionadas para facturar!', 'Warning', 5000);
            return;
        }
        this.pregunta = 'Realmente desea Facturar las remesas seleccionadas?';
        this.operacion = OperacionesAConfirmar.Facturar;
        this.popupVisibleConfirm = true;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
