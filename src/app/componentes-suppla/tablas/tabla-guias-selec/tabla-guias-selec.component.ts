import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { GuiasService } from 'app/servicios/guias.service';
import notify from 'devextreme/ui/notify';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';

@Component({
    selector: 'tabla-guias-selec',
    templateUrl: './tabla-guias-selec.component.html',
    styleUrls: ['./tabla-guias-selec.component.scss']
})
export class TablaGuiasSelecComponent implements OnInit {

    @ViewChild('gridGuiasSel') gridGuiasSel;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
    heightTabla: number;
    popupVisible = false;
    guiasValorTotal: number;
    tcUrl = TC_BASE_URL;
    ingresosLista: any;
    popupVisibleConfirm = false;
    popupVisibleObservacion = false;
    observacionTxt: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;

    constructor(public gs: GuiasService) {
    }

    ngOnInit() {
        this.calculeHeightTabla();
        this.gs.guiasSeleccionadosValor.subscribe(val => this.guiasValorTotal = val);
    }

    RemoverSelec() {
        if (this.gridGuiasSel.selectedRowKeys.length === 0) {
            notify('Debe seleccionar las guias a remover!', 'Warning', 5000);
            return;
        }

        this.gridGuiasSel.selectedRowKeys.forEach(row => {
            this.dataSource.forEach(rowDs => {
                if (rowDs === row) {
                    this.dataSource.splice(this.dataSource.findIndex(x => x.Numero === rowDs.Numero), 1);
                }
            });
        });
        this.gs.guiasSeleccionadas.next(this.dataSource);
        let guiasSeleccionadosValor = 0;
        this.dataSource.forEach((costo: any) => {
            guiasSeleccionadosValor += parseInt(costo.Total);
        });
        this.gs.guiasSeleccionadosValor.next(guiasSeleccionadosValor);
    }

    Detalle(data) {
        this.gs.DetalleGuiaIngresos(data.Numero).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.Facturar:
                    this.observacionTxt = '';
                    this.popupVisibleObservacion = true;
                    break;

                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    ObservacionOK() {
        this.popupVisibleConfirm = false;
        this.popupVisibleObservacion = false;
        const guias = [];
        this.dataSource.forEach(guia => {
            guias.push(guia.Numero.toString());
        });
        this.gs.FacturarGuias(guias, this.observacionTxt).then((resp: any) => {
            // console.log('resp fact guias', resp.FacturarGuiasResult);
            if (resp.FacturarGuiasResult.Message.TypeEnum === 3) {
                notify('Error Facturando guias, Descripcion: ' +
                    resp.FacturarGuiasResult.Message.Message, 'Error', 10000);

                if (resp.FacturarGuiasResult.DTO && resp.FacturarGuiasResult.DTO.length > 0) {
                    notify('Novedades: ' + resp.FacturarGuiasResult.DTO.toString(), 'Warning', 10000);
                }
            } else if (resp.FacturarGuiasResult.Message.TypeEnum === 1) {
                const transNum = resp.FacturarGuiasResult.Message.TransactionNumber;
                notify('Guias Facturadas Exitosamente!. No. Transaccion: ' + transNum, 'Success', 3000);
                this.refreshData.emit(true);
                this.gs.guiasSeleccionadosValor.next(0);
            }
        });
    }

    FacturarGuias() {
        if (!this.dataSource || this.dataSource.length === 0) {
            notify('No hay guias seleccionadas para facturar!', 'Warning', 5000);
            return;
        }
        this.pregunta = 'Realmente desea Facturar las guias seleccionadas?';
        this.operacion = OperacionesAConfirmar.Facturar;
        this.popupVisibleConfirm = true;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
