import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'tabla-detalle-guias-ingresos',
    templateUrl: './tabla-detalle-guias-ingresos.component.html',
    styleUrls: ['./tabla-detalle-guias-ingresos.component.scss']
})
export class TablaDetalleGuiasIngresosComponent implements OnInit {

    @Input() ingresosLista:any;
    heightTabla: number;

    constructor() { }

    ngOnInit() { 
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 4.2;
    }    
    
}