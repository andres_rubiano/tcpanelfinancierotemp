import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { DetalleViajeService } from 'app/servicios/detalle-viaje.service';

@Component({
    selector: 'tabla-costos',
    templateUrl: './tabla-costos.component.html',
    styleUrls: ['./tabla-costos.component.scss']
})
export class TablaCostosComponent implements OnInit {

    @ViewChild('gridCostos') gridCostos;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    mostrarFiltro = false;
    heightTabla: number;
    popupVisible = false;
    tcUrl = TC_BASE_URL;

    constructor(public cs: CostosService, public ms: ManifiestosService, public us: Utils,
        public dvs: DetalleViajeService) { }

    ngOnInit() {
        this.cs.buscandoCostos.subscribe(val => {
            if (val) {
                this.gridCostos.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridCostos.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    PasarCostosSelec() {
        // console.log(this.grid.selectedRowKeys);
        if (this.gridCostos.selectedRowKeys.length === 0) {
            notify('No hay costos seleccionados para pasar!', 'Warning', 6000);
            return;
        }
        this.cs.costosSeleccionados.next(this.gridCostos.selectedRowKeys);
        let costosSeleccionadosValor = 0;
        this.gridCostos.selectedRowKeys.forEach((costo: any) => {
            costosSeleccionadosValor += parseInt(costo.Costo);
        });
        this.cs.costosSeleccionadosValor.next(costosSeleccionadosValor);
    }

    Detalle(viajeNum) {
        // console.log('viajeNum', viajeNum.data.Viaje);
        this.ms.DetalleViaje(viajeNum.data.Viaje).then(det => {
            if (det) {
                this.dvs.detalle = det;
                this.popupVisible = true;
            } else {
                notify('No hay detalle disponible', 'Warning', 3000);
            }
        });
    }
    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
