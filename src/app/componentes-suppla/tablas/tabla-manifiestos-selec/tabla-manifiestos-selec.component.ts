import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tabla-manifiestos-selec',
    templateUrl: './tabla-manifiestos-selec.component.html',
    styleUrls: ['./tabla-manifiestos-selec.component.scss']
})
export class TablaManifiestosSelecComponent implements OnInit {

    @ViewChild('gridManifSel') gridManif;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    mostrarFiltro = false;
    heightTabla: number;
    viajeDetalle: any;
    popupVisible = false;
    tcUrl = TC_BASE_URL;

    constructor(public ms: ManifiestosService, public us: Utils) { }

    ngOnInit() {
        this.ms.buscandoManifiestos.subscribe(val => {
            if (val) {
                this.gridManif.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridManif.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    LiquidarManifiestos() {
        if (this.gridManif.dataSource.length === 0) {
            notify('No hay manifiestos seleccionados para liquidar!', 'Warning', 6000);
            return;
        }
        const viajesAliquidar: Array<string> = [];
        this.dataSource.forEach(vje => {
            viajesAliquidar.push(vje.Viaje);
        });
        // console.log("viajesAliquidar");
        // console.log(viajesAliquidar);

        this.ms.LiquidarManifiestos(viajesAliquidar).then(res => {
            // console.log('response liquidar manifiestos');
            // console.log(res);
            if (res.TypeEnum === 1) {
                this.ms.manifPorLiquidar.next([]);
                this.ms.manifSeleccionados.next([]);
                notify('Viajes con manifiesto enviados a liquidación exitosamente!', 'Success', 5000);
            } else {
                notify('Error liquidando viajes con manifiesto!', 'Error', 5000);
            }
        });
    }

    Detalle(viajeNum) {
        // console.log("viajeNum");
        // console.log(viajeNum.data.Viaje);
        this.ms.DetalleViaje(viajeNum.data.Viaje).then(det => {
            if (det) {
                this.viajeDetalle = det;
                this.popupVisible = true;
            }
        });
    }

    PasarManifSelec() {
        this.ms.manifSeleccionados.next(this.gridManif.selectedRowKeys);
    }

    RemoverSelec() {
        if (this.gridManif.selectedRowKeys.length === 0) {
            notify('Debe seleccionar los manifiestos a remover!', 'Warning', 6000);
            return;
        }
        this.gridManif.selectedRowKeys.forEach(row => {
            this.dataSource.forEach(rowDs => {
                if (rowDs === row) {
                    this.dataSource.splice(this.dataSource.findIndex(x => x.Viaje === rowDs.Viaje), 1);
                }
            });
        });
        this.ms.manifSeleccionados.next(this.dataSource);
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
