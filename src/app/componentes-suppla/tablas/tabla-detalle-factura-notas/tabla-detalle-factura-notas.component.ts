import { PopupAccionConfirmacionComponent } from 'app/componentes-suppla/popups/accion-confirmacion/accion-confirmacion.component';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { TC_BASE_URL, PORCENTAJE_ALTO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FacturasService } from 'app/servicios/facturas.service';

@Component({
    selector: 'tabla-detalle-factura-notas',
    templateUrl: './tabla-detalle-factura-notas.component.html',
    styleUrls: ['./tabla-detalle-factura-notas.component.scss']
})
export class TablaDetalleFacturaNotasComponent implements OnInit {

    @ViewChild('gridFacturaDetNotas') gridFacturaDetNotas;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;
    notaNumero: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    popupVisibleConfirm = false;

    constructor(public us: Utils, public fs: FacturasService) { }

    ngOnInit() {
        this.calculeHeightTabla();
    }

    EliminarNota(notaNumero) {
        this.notaNumero = notaNumero;
        this.pregunta = 'Realmente desea eliminar la nota #' + notaNumero + '?';
        this.operacion = OperacionesAConfirmar.RemoverNota;
        this.popupVisibleConfirm = true;
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.RemoverNota:
                    this.fs.EliminarNotaCliente(this.notaNumero).then(res => {
                        if (res.TypeEnum === 1) {
                            notify('Nota eliminada exitosamente!', 'Success', 3000);
                            this.fs.refrescarDetalle.next(true);
                        } else if (res.TypeEnum === 3) {
                            notify('Error eliminando nota. Descripción: ' + res.Message, 'Error', 6000);
                        }
                    });
                    this.popupVisibleConfirm = false;
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 1.55;
        this.widthTabla = window.screen.width / 2.15;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
