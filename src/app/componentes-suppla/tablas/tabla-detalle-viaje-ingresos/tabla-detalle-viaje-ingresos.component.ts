import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'tabla-detalle-viaje-ingresos',
    templateUrl: './tabla-detalle-viaje-ingresos.component.html',
    styleUrls: ['./tabla-detalle-viaje-ingresos.component.scss']
})
export class TablaDetalleViajeIngresosComponent implements OnInit {

    @Input() ingresosLista:any 
    heightTabla: number;

    constructor() { }

    ngOnInit() { 
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 4;
    }
    calculateCellValueFecha(data){
        let monthNames = [
            "Ene", "Feb", "Mar",
            "Abr", "May", "Jun", "Jul",
            "Ago", "Sep", "Oct",
            "Nov", "Dic"
        ];
        let date = new Date(parseInt(data.FechaCreacion.replace('/Date(', '')));

        let monthIndex = date.getMonth();
        let year = date.getFullYear().toString();
        let hour = date.getHours().toString().length == 1 ? '0' + date.getHours().toString() : date.getHours().toString();
        let min = date.getMinutes().toString().length == 1 ? '0' + date.getMinutes().toString() : date.getMinutes().toString();
        let seg = date.getSeconds().toString().length == 1 ? '0' + date.getSeconds().toString() : date.getSeconds().toString();
        let finalDate = `${date.getDate().toString()}/${monthNames[monthIndex]}/${year} ${hour}:${min}:${seg}`;
        return finalDate;
    }
}