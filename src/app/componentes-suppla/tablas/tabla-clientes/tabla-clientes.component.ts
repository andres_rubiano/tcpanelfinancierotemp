import { TarifasFiltroCliComponent } from '../../filtros/tarifas-filtro-cli/tarifas-filtro-cli.component'
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ClientesService } from 'app/servicios/clientes.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { TablaModelosCliComponent } from 'app/componentes-suppla/tablas/tabla-modelos-cli/tabla-modelos-cli.component';

@Component({
    selector: 'tabla-clientes',
    templateUrl: './tabla-clientes.component.html',
    styleUrls: ['./tabla-clientes.component.scss']
})
export class TablaClientesComponent implements OnInit {

    @ViewChild('gridCli') dataGridCli : DxDataGridComponent;
    clientesLista: Array<any>;
    heightTabla: number;
    defaultVisible = false;
    mostrarFiltro: boolean = false;
    withAnimationVisible = false;0
    idOnMouseOver: string;
    esqDescripcion: string;

    constructor(public cs: ClientesService, public ms: ModelosService, public as: ArticulosService,
        public ts: TarifasService, public tfc: TarifasFiltroCliComponent) { }

    ngOnInit() {       
        this.cs.buscandoClientes.subscribe(val => {
            if (val) {
                this.dataGridCli.instance.beginCustomLoading("Cargando...");
            } else {
                this.dataGridCli.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    toggleDefault() {
        this.defaultVisible = !this.defaultVisible;
    }

    toggleWithAnimation(esq) {
        if (esq != undefined) {
            this.idOnMouseOver = '#' + esq.ConfiguracionId;
            this.esqDescripcion = esq.EsquemaDescripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }

    calculateCellValueEsquema(data) {
        return data;
    }

    ObtenerModelosPorCliente() {   
        this.ts.modeloCliCodigoSelected.next('');
        this.ms.selectedRowCli.next(-1);
        let row = this.dataGridCli.selectedRowKeys;
        this.ts.tarifasCliList.next([]);
        if (row != undefined && row[0] != undefined) {
            this.ts.clienteCodigoSelected.next(row[0].ClienteCodigo);
            this.ts.esquemaCliCodigoSelected.next(row[0].EsquemaCodigo);
            this.ms.modelosCliLista.next(row[0].Modelos);
        }
        this.as.articulosCliLista.next([]);
        this.ts.articuloCliCodigoSelected.next('');
        this.tfc.ResetearFiltros();
        this.ts.isCollapsedObs.next(true);
        this.ts.tarifasCliList.next([]);
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 5;
    }

    onCellPrepared(e) {
        if (e.rowType == 'header')
            e.cellElement.css('background-color', '#edf5ff');
    }
}