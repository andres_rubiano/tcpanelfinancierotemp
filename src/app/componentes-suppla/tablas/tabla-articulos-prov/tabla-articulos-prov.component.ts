import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { ArticulosService } from 'app/servicios/articulos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'tabla-articulos-prov',
    templateUrl: './tabla-articulos-prov.component.html',
    styleUrls: ['./tabla-articulos-prov.component.scss']
})
export class TablaArticulosProvComponent implements OnInit {

    @Input() articulosLista: Array<any> = [];
    _selectedRow: number;
    modelSelected: string;
    withAnimationVisible = false;
    idOnMouseOver: string;
    artDescripcion: string;
    //#region observables request tarifa
    tipoEsquema: TipoEsquemaEnum;
    esquemaCodigoSelected: string;
    proveedorCodigoSelected: string;
    modeloCodigoSelected: string;
    articuloCodigoSelected: string;
    //#endregion
    constructor(public as: ArticulosService,
        public ts: TarifasService) { }

    ngOnInit() {
        this.as.selectedRowProv.subscribe(val => this._selectedRow = val)
        this.as.selectedRowProv.next(-1);
        this.ts.modeloProvCodigoSelected.subscribe(val => this.modeloCodigoSelected = this.modelSelected = val);
        //subscripcion observables request tarifa
        this.ts.tipoEsquema.subscribe(data => this.tipoEsquema = data);
        this.ts.esquemaProvCodigoSelected.subscribe(data => this.esquemaCodigoSelected = data);
        this.ts.proveedorCodigoSelected.subscribe(data => this.proveedorCodigoSelected = data);
        this.ts.articuloProvCodigoSelected.subscribe(data => this.articuloCodigoSelected = data);
    }

    ObtenerTarifasPorArticulo(index) {
        this.as.selectedRowProv.next(index);
        this.ts.articuloProvCodigoSelected.next(this.articulosLista[index].Codigo);
        this.ts.isCollapsedObs.next(false);
        this.ts.tarifasProvList.next([]);
        this.ts.RequeridosFiltrosPorTarifa(this.modelSelected, this.articulosLista[index].Codigo)
            .then(filtros => this.ts.filtrosTarifasProv.next(filtros));
    }

    ExportarTarifasAsoc(mod, index) {
        this.as.selectedRowProv.next(index);
        this.ts.articuloProvCodigoSelected.next(this.articulosLista[index].Codigo);
        let filtro = {
            EsquemaCodigo: this.esquemaCodigoSelected,
            TipoEsquema: TipoEsquemaEnum.Proveedor,
            ClienteCodigo: "",
            ProveedorCodigo: this.proveedorCodigoSelected,
            ModeloCodigo: this.modeloCodigoSelected,
            ArticuloCodigo: this.articuloCodigoSelected
        };
        this.as.ObtenerTarifasPorArticulo(filtro).then((resp) => {
            // console.log(resp);

            if (resp.ObtenerTarifasPorArticuloResult.TypeEnum == "1") {
                notify("El proceso de exportación ha sido enviado a la cola de tareas, revise el listado de descargas!", "Success", 5000);
            } else {
                notify("Error al exportar tarifas por articulo", "Error", 6000);
            }
        });
    }

    toggleWithAnimation(art) {
        if (art != undefined) {
            this.idOnMouseOver = '#' + art.Codigo;
            this.artDescripcion = art.Descripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }
}