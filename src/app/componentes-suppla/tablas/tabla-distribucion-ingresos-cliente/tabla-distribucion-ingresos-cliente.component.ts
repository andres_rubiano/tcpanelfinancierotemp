import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { TC_BASE_URL } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { BROWSER_SANITIZATION_PROVIDERS } from '@angular/platform-browser/src/browser';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'tabla-distribucion-ingresos-cliente',
    templateUrl: './tabla-distribucion-ingresos-cliente.component.html',
    styleUrls: ['./tabla-distribucion-ingresos-cliente.component.scss']
})
export class TablaDistribucionIngresosClienteComponent implements OnInit {

    @ViewChild('gridDistIngresosCli') gridDistIngresosCli;
    @Input() dataSource: Array<any>;
    @Input() heightTabla: number;
    @Input() widthTabla: number;
    @Output() ingresosYobservacion: EventEmitter<any> = new EventEmitter();
    dataSourceModified: Array<any>;
    popupVisibleConfirm: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;
    observacion: string;
    totalAModificar = 0.0;

    constructor() { }

    ngOnInit() {
    }

    Respuesta(e) {
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddNota:
                    this.popupVisibleConfirm = false;
                    this.ingresosYobservacion.emit({ obs: this.observacion, modificados: this.dataSourceModified });
                    this.totalAModificar = 0;
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    this.totalAModificar = 0;
                    break;
            }
        }
    }

    CrearNota() {
        let seAgregoIngreso = false;
        this.dataSourceModified = [];
        this.gridDistIngresosCli.dataSource.forEach(row => {
            if (row.Valor !== 0 && row.Valor !== '' && row.Valor !== null) {
                this.dataSourceModified.push(row);
                seAgregoIngreso = true;
            }
        });
        if (!seAgregoIngreso) {
            notify('Debe modificar al menos un ingreso!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea crear la nota?';
        this.operacion = OperacionesAConfirmar.AddNota;
        this.popupVisibleConfirm = true;

    }

    onCellPrepared(e) {
        if (e.rowType === 'header') { e.cellElement.css('background-color', '#edf5ff') }
    }

    onRowUpdated(e) {
        if (e.data.Valor === '' || e.data.Valor === null) {
            e.data.Valor = 0;
            return;
        }
        if (e.key.IngresoTotal > 0) {
            if (e.data.Valor < (e.key.IngresoTotal * -1)) {
                notify('No puede restar un valor mayor al Costo de Referencia ($ ' + e.key.IngresoTotal + ')', 'Warning', 5000);
                e.key.Valor = 0;
            }
        }

        this.ObtenerTotalAModificar();
    }

    ObtenerTotalAModificar() {
        this.totalAModificar = 0;
        this.dataSource.forEach(row => {
            this.totalAModificar += row.Valor;
        });
    }

    BeginEditDeleteZeroFromCell(e) {
        if (e.data.Valor === 0) {
            e.data.Valor = null;
        }
    }

}
