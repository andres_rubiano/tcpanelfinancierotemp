import { TarifasFiltroProvComponent } from 'app/componentes-suppla/filtros/tarifas-filtro-prov/tarifas-filtro-prov.component';
import { FiltroRequeridoInterface } from 'app/interfaces/filtro-requerido.interface';
import { FiltroTarifaInterface } from 'app/interfaces/filtro-tarifas.interface';
import { NombresFiltrosEnum } from 'app/enumeraciones/nombres-filtros.enum';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { TarifaInterface } from 'app/interfaces/tarifa.interface';
import { TarifasService } from 'app/servicios/tarifas.service';
import { CurrencyPipe } from '@angular/common';

@Component({
    selector: 'tabla-tarifas-prov',
    templateUrl: './tabla-tarifas-prov.component.html',
    styleUrls: ['./tabla-tarifas-prov.component.scss']
})
export class TablaTarifasProvComponent implements OnInit {

    @Input() tarifasLista: Array<TarifaInterface>;
    @Input() tarifasCant: number = 0;
    @Input() heightTabla: number;
    @Input() filasNumero: number = 15;
    @ViewChild('gridTar') dataGridTar;
    filtrosTarifas: Array<FiltroRequeridoInterface>;
    ciudadOrigenReq: number;
    ciudadDestinoReq: number;
    vehiculoReq: number;
    rutaReq: number;
    empaqueReq: number;
    minReq: number;
    maxReq: number;
    valorReq: number;
    fechaIniReq: number;
    fechaFinReq: number;
    estadoReq: number;
    mostrarFiltro: boolean = false;

    constructor(public ts: TarifasService, public tfc:TarifasFiltroProvComponent) {

    }

    ngOnInit() {
        this.ts.buscandoTarifasCli.subscribe(val => {
            if (val) {
                this.dataGridTar.instance.beginCustomLoading('Cargando...');
            } else {
                this.dataGridTar.instance.endCustomLoading();
            }
        });
        this.ts.exportarTarifas.subscribe(val => {
            if (val) {
                this.dataGridTar.instance.exportToExcel(false);
            }
        });
        this.ts.filtrosTarifasProv.subscribe(filtros => {
            this.filtrosTarifas = filtros;
        });
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 2.5;
    }

    OnSelected(e) {
        this.ts.tarifasSelectedProv.next(e);
    }

    onCellPrepared(e) {
        if (e.rowType == 'header')
            e.cellElement.css('background-color', '#edf5ff');
    }

    public ResetearFiltros() {
        this.ciudadOrigenReq = 0;
        this.ciudadDestinoReq = 0;
        this.vehiculoReq = 0;
        this.rutaReq = 0;
        this.empaqueReq = 0;
        this.minReq = 0;
        this.maxReq = 0;
        this.valorReq = 0;
        this.fechaIniReq = 0;
        this.fechaFinReq = 0;
        this.estadoReq = 0;
    }

}