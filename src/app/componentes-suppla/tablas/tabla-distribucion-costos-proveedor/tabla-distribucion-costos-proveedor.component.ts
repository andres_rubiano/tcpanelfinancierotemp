import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { TC_BASE_URL } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';

@Component({
    selector: 'tabla-distribucion-costos-proveedor',
    templateUrl: './tabla-distribucion-costos-proveedor.component.html',
    styleUrls: ['./tabla-distribucion-costos-proveedor.component.scss']
})
export class TablaDistribucionCostosProveedorComponent implements OnInit {

    @ViewChild('gridDistCosProv') gridDistCosProv;
    @Input() dataSource: Array<any>;
    @Input() heightTabla: number;
    @Input() widthTabla: number;
    @Output() costosYobservacion: EventEmitter<any> = new EventEmitter();
    dataSourceModified: Array<any>;
    popupVisibleConfirm: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;
    observacion: string;
    totalAModificar = 0.0;

    constructor(public us: Utils) { }

    ngOnInit() {
    }

    Respuesta(e) {
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddNota:
                    this.popupVisibleConfirm = false;
                    this.costosYobservacion.emit({ obs: this.observacion, modificados: this.dataSourceModified });
                    this.totalAModificar = 0;
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    this.totalAModificar = 0;
                    break;
            }
        }
    }

    CrearNota() {
        let seAgregoCosto = false;
        this.dataSourceModified = [];
        this.gridDistCosProv.dataSource.forEach(row => {
            if (row.Valor !== 0 && row.Valor !== '' && row.Valor !== null) {
                this.dataSourceModified.push(row);
                seAgregoCosto = true;
            }
        });

        if (!seAgregoCosto) {
            notify('Debe modificar al menos un costo!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea crear la nota?';
        this.operacion = OperacionesAConfirmar.AddNota;
        this.popupVisibleConfirm = true;

    }

    onCellPrepared(e) {
        if (e.rowType === 'header') { e.cellElement.css('background-color', '#edf5ff') }
    }

    onRowUpdated(e) {
        if (e.data.Valor === '' || e.data.Valor === null) {
            e.data.Valor = 0;
            return;
        }
        if (e.key.ValorReferencia > 0) {
            if (e.data.Valor < (e.key.ValorReferencia * -1)) {
                notify('No puede restar un valor mayor al Costo de Referencia ($ ' + e.key.ValorReferencia + ')', 'Warning', 5000);
                e.key.Valor = 0;
                return;
            }
        }
        this.ObtenerTotalAModificar();
    }

    BeginEditDeleteZeroFromCell(e) {
        if (e.data.Valor === 0) {
            e.data.Valor = null;
        }
    }

    ObtenerTotalAModificar() {
        this.totalAModificar = 0;
        this.dataSource.forEach(row => {
            this.totalAModificar += row.Valor;
        });
    }
}
