import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { FacturasService } from 'app/servicios/facturas.service';
import { EstadosFacturacionEnum } from 'app/enumeraciones/estados-facturacion.enum';
import { GuiasService } from 'app/servicios/guias.service';
import { FiltroGuiasInterface } from 'app/interfaces/filtro-guias.interface';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { RemesasService } from 'app/servicios/remesas.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { AuthenticacionService } from '../../../servicios/autenticacion.service';

@Component({
    selector: 'tabla-detalle-facturas',
    templateUrl: './tabla-detalle-facturas.component.html',
    styleUrls: ['./tabla-detalle-facturas.component.scss']
})
export class TablaDetalleFacturasComponent implements OnInit, AfterViewInit {

    @ViewChild('gridFacturasDet') gridFacturasDet: DxDataGridComponent;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;
    popupVisibleGuias = false;
    popupVisibleRemesas = false;
    popupVisibleConfirm = false;
    popupVisibleNota: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    facturaId: string;
    totalFacturar: number;
    infoCrearNotaCli: any;
    estadoFactura: EstadosFacturacionEnum;
    esGuia = false;

    // mostrar botones
    mostrarAddNota = false;
    mostrarAddGuia = false;
    mostrarAddRemesa = false;
    mostrarRemoverGuia = false;
    mostrarRemoverRemesa = false;
    mostrarImpresion = false;
    mostrarAnularFact = false;
    mostrarVolDef = false;

    // datos agregar guia/remesa
    cliNombre: string;
    cliCod: string;
    factNumero: string;

    constructor(public fs: FacturasService, public us: Utils, private gs: GuiasService,
        private rs: RemesasService, private authSrvc: AuthenticacionService) { }

    ngOnInit() {
        this.calculeHeightTabla();
        this.fs.detalleFacturaNumero.subscribe(num => this.factNumero = num);
        this.fs.detalleFacturaCliSelectedCod.subscribe(cod => this.cliCod = cod);
        this.fs.detalleFacturaCliSelectedNombre.subscribe(nom => this.cliNombre = nom);
        this.fs.detalleFacturaId.subscribe(id => this.facturaId = id);
        this.fs.totalFacturasDet.subscribe(val => this.totalFacturar = val);
        this.fs.estadoFacturacion.subscribe(val => this.estadoFactura = val);
        this.fs.buscandoFacturasDet.subscribe(val => {
            if (val) {
                this.gridFacturasDet.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridFacturasDet.instance.endCustomLoading();
            }
        });
    }

    ngAfterViewInit(): void {
        this.BotonesPorEstado(this.estadoFactura);
    }

    onContentReady() {
        const data = this.gridFacturasDet.dataSource;
        if (data && data.length > 0) {
            const BreakException = {};
            try {
                data.forEach(row => {
                    if (row.Origen.toString().toLowerCase() === 'guia') {
                        this.esGuia = true;
                        this.BotonesPorEstado(this.estadoFactura);
                        throw BreakException;
                    }
                    this.esGuia = false;
                    this.BotonesPorEstado(this.estadoFactura);
                });
            } catch (e) {
                if (e !== BreakException) { throw e };
            }
        } else {
            this.esGuia = false;
            this.BotonesPorEstado(this.estadoFactura);
        }
    }

    MostrarAddGuias() {
        this.popupVisibleGuias = true;
    }

    MostrarAddRemesas() {
        this.popupVisibleRemesas = true;
    }

    RemoverGuias() {
        if (this.gridFacturasDet.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos una guia!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea remover estas guias de la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.RemoverGuias;
        this.popupVisibleConfirm = true;
    }

    RemoverRemesas() {
        if (this.gridFacturasDet.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos una remesa!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea remover estas remesas de la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.RemoverRemesas;
        this.popupVisibleConfirm = true;
    }

    Imprimir() {
        // tslint:disable-next-line:max-line-length
        const url = `${TC_BASE_URL}/SICLONET/ReportView.aspx?settings=invoiceId=${this.facturaId}&report=FormatInvoiceDetails&usercode=${this.authSrvc.GetCurrentUser()}`;
        window.open(url, '_blank');
    }

    AnularFact() {
        this.pregunta = 'Realmente desea anular la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.AnularFactura;
        this.popupVisibleConfirm = true;
    }

    VolverDefinitiva() {
        this.pregunta = 'Realmente desea volver definitiva la factura #' + this.factNumero + '?';
        this.operacion = OperacionesAConfirmar.VolverDefinitiva;
        this.popupVisibleConfirm = true;
    }

    AgregarNota(costoId: string) {

        if (this.gridFacturasDet.selectedRowKeys.length === 0) {
            notify('Debe seleccionar almenos un ingreso para poder crear la nota!', 'Warning', 3000);
            return;
        }
        const costoCliColumnaValor = [];
        this.gridFacturasDet.selectedRowKeys.forEach(row => {
            const obj = {
                Origen: row.Origen,
                Referencia: row.Referencia,
                Articulo: row.Articulo,
                ArticuloCodigo: row.ArticuloCodigo,
                IngresoTotal: row.IngresoTotal,
                Valor: 0
            };
            costoCliColumnaValor.push(obj);
        });
        this.infoCrearNotaCli = {
            codigoCliente: this.cliCod,
            nombreCliente: this.cliNombre,
            costosCli: costoCliColumnaValor
        };

        this.popupVisibleNota = true;
    }

    NotaCreada(e) {
        if (e) {
            this.popupVisibleNota = false;
            this.fs.refrescarDetalle.next(true);
        }
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddGuias:
                    this.popupVisibleConfirm = false;
                    this.gs.guiasCliente.next([]);
                    break;

                case OperacionesAConfirmar.AddRemesas:
                    this.popupVisibleConfirm = false;
                    this.rs.remesasCliente.next([]);
                    break;

                case OperacionesAConfirmar.RemoverGuias:
                    this.popupVisibleConfirm = false;
                    let guias = '';
                    this.gridFacturasDet.selectedRowKeys.forEach(guia => {
                        guias.length === 0
                            ? guias = guia.Id
                            : guias += ',' + guia.Id;
                    });

                    this.fs.RemoverGuiasAFactura(guias).then(resp => {
                        const res = resp.RemoverDetalleFacturaOrigenGuiaResult;
                        if (res.TypeEnum === 3) {
                            notify('Error removiendo detalle!. Descripcion: '
                                + res.Message, 'Error', 6000);
                            return;
                        }

                        notify('Detalle removido exitosamente!. No. Transaccion: '
                            + res.TransactionNumber, 'Success', 3000);

                        this.fs.refrescarDetalle.next(true);
                        this.RecalcularTotal();
                    });
                    break;
                case OperacionesAConfirmar.RemoverRemesas:
                    this.popupVisibleConfirm = false;
                    let remesas = '';
                    this.gridFacturasDet.selectedRowKeys.forEach(remesa => {
                        remesas.length === 0
                            ? remesas = remesa.Id
                            : remesas += ',' + remesa.Id;
                    });

                    this.fs.RemoverRemesasAFactura(remesas).then(resp => {
                        const res = resp.RemoverDetalleFacturaOrigenRemesaResult;
                        if (res.TypeEnum === 3) {
                            notify('Error removiendo detalle!. Descripcion: '
                                + res.Message, 'Error', 6000);
                            return;
                        }

                        notify('Detalle removido exitosamente!. No. Transaccion: '
                            + res.TransactionNumber, 'Success', 3000);
                        this.fs.refrescarDetalle.next(true);
                        this.RecalcularTotal();
                    });
                    break;
                case OperacionesAConfirmar.AnularFactura:
                    this.popupVisibleConfirm = false;
                    this.fs.AnularFactura(this.factNumero).then((resp: any) => {
                        const res = resp.AnularFacturaResult;
                        if (res.TypeEnum === 3) {
                            notify('Error anulando factura!. Descripcion: '
                                + res.Message, 'Error', 6000);
                            return;
                        }

                        notify('Factura anulada exitosamente!. No. Transaccion: '
                            + res.TransactionNumber, 'Success', 3000);

                        this.BotonesPorEstado(EstadosFacturacionEnum.Anulada);
                        this.fs.refrescarFacturas.next(true);
                    });
                    break;
                case OperacionesAConfirmar.VolverDefinitiva:
                    this.popupVisibleConfirm = false;
                    this.fs.VolverDefinitiva(this.factNumero).then((resp: any) => {
                        const res = resp.EstablecerFacturaComoDefinitivaResult;
                        if (res.TypeEnum === 3) {
                            notify('Error volviendo definitiva factura!. Descripcion: '
                                + res.Message, 'Error', 6000);
                            return;
                        }

                        notify('Factura definitiva exitosamente!. No. Transaccion: '
                            + res.TransactionNumber, 'Success', 3000);
                        this.BotonesPorEstado(EstadosFacturacionEnum.Definitiva);
                        this.fs.refrescarFacturas.next(true);
                        this.fs.refrescarDetalle.next(true);
                        this.mostrarAddNota = false;
                    });
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    BotonesPorEstado(val: EstadosFacturacionEnum) {
        switch (val) {
            case EstadosFacturacionEnum.Anulada:
                this.mostrarAddNota = false;
                this.mostrarAddGuia = false;
                this.mostrarAddRemesa = false;
                this.mostrarRemoverGuia = false;
                this.mostrarRemoverRemesa = false;
                this.mostrarImpresion = false;
                this.mostrarAnularFact = false;
                this.mostrarVolDef = false;
                break;
            case EstadosFacturacionEnum.Definitiva:
                this.mostrarAddNota = false;
                this.mostrarAddGuia = false;
                this.mostrarAddRemesa = false;
                this.mostrarRemoverGuia = false;
                this.mostrarRemoverRemesa = false;
                this.mostrarImpresion = true;
                this.mostrarAnularFact = false;
                this.mostrarVolDef = false;
                break;
            case EstadosFacturacionEnum.PreFacturada:
                if (this.esGuia) {
                    this.mostrarAddGuia = true;
                    this.mostrarAddRemesa = false;
                    this.mostrarRemoverGuia = true;
                    this.mostrarRemoverRemesa = false;
                } else {
                    this.mostrarAddGuia = false;
                    this.mostrarAddRemesa = true;
                    this.mostrarRemoverGuia = false;
                    this.mostrarRemoverRemesa = true;
                }

                this.mostrarAddNota = true;
                this.mostrarImpresion = true;
                this.mostrarAnularFact = true;
                this.mostrarVolDef = true;
                break;
            default:
                break;
        }
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 1.55;
        this.widthTabla = (window.screen.width / 2.15);
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }

    RecalcularTotal(): void {
        let recalcularTotal = 0;
        this.dataSource.forEach(row => {
            recalcularTotal += row.Total;
        });
        this.fs.totalFacturasDet.next(recalcularTotal);
    }
}
