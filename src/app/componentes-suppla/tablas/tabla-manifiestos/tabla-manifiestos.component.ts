import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { DetalleViajePopupComponent } from 'app/componentes-suppla/popups/detalle-viaje/detalle-viaje-popup.component';
import { DetalleViajeService } from 'app/servicios/detalle-viaje.service';

@Component({
    selector: 'tabla-manifiestos',
    templateUrl: './tabla-manifiestos.component.html',
    styleUrls: ['./tabla-manifiestos.component.scss']
})
export class TablaManifiestosComponent implements OnInit {

    @ViewChild('gridManif') gridManif;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;
    mostrarFiltro = false;
    popupVisible = false;
    tcUrl = TC_BASE_URL;

    constructor(public ms: ManifiestosService, public us: Utils, public dvs: DetalleViajeService) { }

    ngOnInit() {
        this.ms.buscandoManifiestos.subscribe(val => {
            if (val) {
                this.gridManif.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridManif.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    Detalle(viajeNum) {
        // console.log("viajeNum");
        // console.log(viajeNum.data.Viaje);
        this.ms.DetalleViaje(viajeNum.data.Viaje).then(det => {
            // console.log('det');
            // console.log(det);
            if (det) {
                this.dvs.detalle = det;
                this.popupVisible = true;
            }else {
                notify('No hay detalle disponible', 'Warning', 3000);
            }
        });
    }

    PasarManifSelec() {
        // console.log(this.grid.selectedRowKeys);
        if (this.gridManif.selectedRowKeys.length === 0) {
            notify('No hay manifiestos seleccionados para pasar!', 'Warning', 6000);
            return;
        }
        this.ms.manifSeleccionados.next(this.gridManif.selectedRowKeys);
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
