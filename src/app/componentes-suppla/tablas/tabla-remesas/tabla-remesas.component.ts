import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { RemesasService } from 'app/servicios/remesas.service';
import notify from 'devextreme/ui/notify';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tabla-remesas',
    templateUrl: './tabla-remesas.component.html',
    styleUrls: ['./tabla-remesas.component.scss']
})
export class TablaRemesasComponent implements OnInit {

    @ViewChild('gridRemesas') gridRemesas;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
    heightTabla: number;
    mostrarFiltro = false;
    popupVisible = false;
    popupVisibleConfirm = false;
    popupVisibleFRB = false;
    observacionTxt: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    ingresosLista: any;
    tcUrl = TC_BASE_URL;
    remesasTxt: string;

    constructor(public rs: RemesasService, public us: Utils) {
    }

    ngOnInit() {
        this.calculeHeightTabla();
        this.rs.buscandoRemesas.subscribe(val => {
            if (val) {
                this.gridRemesas.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridRemesas.instance.endCustomLoading();
            }
        });
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    Detalle(data) {
        this.rs.DetalleRemesasIngresos(data.Numero).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.MarcarNoFacturable:
                    const remesas = [];
                    this.gridRemesas.selectedRowKeys.forEach(remesa => {
                        remesas.push(remesa.Numero.toString());
                    });
                    this.rs.EstablecerRemesasNoFacturables(remesas).then((resp: any) => {
                        // console.log('resp no fact remesas', resp.EstablecerRemesasComoNoFacturableResult);
                        if (resp.EstablecerRemesasComoNoFacturableResult.TypeEnum === 3) {
                            notify('Error marcando no Facturables, Descripcion: ' +
                                resp.EstablecerRemesasComoNoFacturableResult.Message, 'Error', 7000);
                        } else if (resp.EstablecerRemesasComoNoFacturableResult.TypeEnum === 1) {
                            notify('Remesas Marcadas Exitosamente!', 'Success', 3000);
                            this.refreshData.emit(true);
                        }
                    });
                    this.popupVisibleConfirm = false;
                    break;

                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    PopupFacturarBloque() {
        this.popupVisibleFRB = true;
    }

    FacturarBloque() {
        this.us.ValidaFormatoFacturacionEnBloque(this.remesasTxt.trim()).then(val => {
            if (val) {
                notify('Elimine los espacios en blanco en la cadena de numeros de remesas!', 'Error', 7000);
                return;
            }
            const remesas = [];
            this.remesasTxt.trim().split(',').forEach(remesa => {
                remesas.push(remesa);
            });

            this.rs.FacturarRemesas(remesas, this.observacionTxt).then((resp: any) => {
                // console.log('resp fact remesas', resp.FacturarRemesasResult);
                if (resp.FacturarRemesasResult.Message.TypeEnum === 3) {
                    notify('Error Facturando remesas, Descripcion: ' +
                        resp.FacturarRemesasResult.Message.Message, 'Error', 10000);

                    if (resp.FacturarRemesasResult.DTO && resp.FacturarRemesasResult.DTO.length > 0) {
                        let novedades = '';
                        resp.FacturarRemesasResult.DTO.forEach(novedad => {
                            novedades.length === 0 ?
                                novedades = `${novedad.Numero}: ${novedad.Novedad}` :
                                novedades += `, ${novedad.Numero}: ${novedad.Novedad}`;
                        });
                        notify('Novedades: ' + novedades, 'Warning', 10000);
                    }
                } else if (resp.FacturarRemesasResult.Message.TypeEnum === 1) {
                    const transNum = resp.FacturarRemesasResult.Message.TransactionNumber;
                    notify('Remesas Facturadas Exitosamente!. No. Transaccion: ' + transNum, 'Success', 3000);
                    this.dataSource = [];
                    this.remesasTxt = '';
                    this.observacionTxt = '';
                    this.popupVisibleFRB = false;
                }
            });
        });
    }

    MarcarNoFacturable() {
        this.pregunta = 'Realmente desea marcar como NO Facturable las remesas seleccionadas?';
        this.operacion = OperacionesAConfirmar.MarcarNoFacturable;
        this.popupVisibleConfirm = true;
    }

    PasarRemesasSelec() {
        if (this.gridRemesas.selectedRowKeys.length === 0) {
            notify('No hay remesas seleccionadas para pasar!', 'Warning', 5000);
            return;
        }
        this.rs.remesasSeleccionadas.next(this.gridRemesas.selectedRowKeys);
        let remesasSeleccionadosValor = 0;
        this.gridRemesas.selectedRowKeys.forEach((valor: any) => {
            remesasSeleccionadosValor += parseInt(valor.Total);
        });
        this.rs.remesasSeleccionadosValor.next(remesasSeleccionadosValor);
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
    
    onHiddenPopup() {
        this.remesasTxt = '';
        this.observacionTxt = '';
    }
}
