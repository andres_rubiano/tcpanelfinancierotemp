import { PopupAccionConfirmacionComponent } from 'app/componentes-suppla/popups/accion-confirmacion/accion-confirmacion.component';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { TC_BASE_URL, PORCENTAJE_ALTO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { log } from 'util';

@Component({
    selector: 'tabla-detalle-liquidaciones-notas',
    templateUrl: './tabla-detalle-liquidaciones-notas.component.html',
    styleUrls: ['./tabla-detalle-liquidaciones-notas.component.scss']
})
export class TablaDetalleLiquidacionesNotasComponent implements OnInit {

    @ViewChild('gridLiquidacionesDetNotas') gridLiquidacionesDetNotas;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;
    notaNumero: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    popupVisibleConfirm = false;

    constructor(public us: Utils, public ls: LiquidacionesService) { }

    ngOnInit() {
        this.calculeHeightTabla();
    }

    EliminarNota(notaNumero) {
        this.notaNumero = notaNumero;
        this.pregunta = 'Realmente desea eliminar la nota #' + notaNumero + '?';
        this.operacion = OperacionesAConfirmar.RemoverNota;
        this.popupVisibleConfirm = true;
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.RemoverNota:
                    this.ls.EliminarNotaProveedor(this.notaNumero).then(res => {
                        if (res.TypeEnum === 1) {
                            notify('Nota eliminada exitosamente!', 'Success', 3000);
                            this.ls.refrescarLiquidacionesDet.next(true);
                        } else if (res.TypeEnum === 3) {
                            notify('Error eliminando nota. Descripción: ' + res.Message, 'Error', 6000);
                        }
                    });
                    this.popupVisibleConfirm = false;
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 1.55;
        this.widthTabla = window.screen.width / 2.15;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
