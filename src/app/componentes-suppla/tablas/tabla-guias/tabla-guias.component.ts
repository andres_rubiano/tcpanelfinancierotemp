import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { GuiasService } from 'app/servicios/guias.service';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tabla-guias',
    templateUrl: './tabla-guias.component.html',
    styleUrls: ['./tabla-guias.component.scss']
})
export class TablaGuiasComponent implements OnInit {

    @ViewChild('gridGuias') gridGuias: DxDataGridComponent;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
    heightTabla: number;
    mostrarFiltro = false;
    popupVisible = false;
    popupVisibleConfirm = false;
    popupVisibleFGB = false;
    observacionTxt: string;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    ingresosLista: any;
    tcUrl = TC_BASE_URL;
    guiasTxt: string;

    constructor(public gs: GuiasService, public us: Utils) {
    }

    ngOnInit() {
        this.calculeHeightTabla();
        this.gs.buscandoGuias.subscribe(val => {
            if (val) {
                this.gridGuias.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridGuias.instance.endCustomLoading();
            }
        });
    }

    Detalle(data) {
        this.gs.DetalleGuiaIngresos(data.Numero).then(ingr => {
            if (ingr) {
                this.popupVisible = true;
                this.ingresosLista = ingr;
            }
        });
    }

    Respuesta(e) {
        // console.log('respuesta e ');
        // console.log(e);
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.MarcarNoFacturable:
                    const guias = [];
                    this.gridGuias.selectedRowKeys.forEach(guia => {
                        guias.push(guia.Numero.toString());
                    });
                    this.gs.EstablecerGuiasNoFacturables(guias).then((resp: any) => {
                        // console.log('resp no fact guias', resp.EstablecerGuiasComoNoFacturableResult);
                        if (resp.EstablecerGuiasComoNoFacturableResult.TypeEnum === 3) {
                            notify('Error marcando no Facturables, Descripcion: ' +
                                resp.EstablecerGuiasComoNoFacturableResult.Message, 'Error', 7000);
                        } else if (resp.EstablecerGuiasComoNoFacturableResult.TypeEnum === 1) {
                            notify('Guias Marcadas Exitosamente!', 'Success', 3000);
                            this.refreshData.emit(true);
                        }
                    });
                    this.popupVisibleConfirm = false;
                    break;

                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    PopupFacturarBloque() {
        this.popupVisibleFGB = true;
    }

    FacturarBloque() {
        this.us.ValidaFormatoFacturacionEnBloque(this.guiasTxt.trim()).then(val => {
            if (val) {
                notify('Elimine los espacios en blanco en la cadena de numeros de guias!', 'Error', 7000);
                return;
            }
            const guias = [];
            this.guiasTxt.trim().split(',').forEach(guia => {
                if (guia.length > 0) { guias.push(guia); }
            });
            this.gs.FacturarGuias(guias, this.observacionTxt).then((resp: any) => {
                if (resp.FacturarGuiasResult.Message.TypeEnum === 3) {
                    notify('Error Facturando guias, Descripcion: ' +
                        resp.FacturarGuiasResult.Message.Message, 'Error', 10000);
                    if (resp.FacturarGuiasResult.DTO && resp.FacturarGuiasResult.DTO.length > 0) {
                        let novedades = '';
                        resp.FacturarGuiasResult.DTO.forEach(novedad => {
                            novedades.length === 0 ?
                                novedades = `${novedad.Numero}: ${novedad.Novedad}` :
                                novedades += `, ${novedad.Numero}: ${novedad.Novedad}`;
                        });
                        notify('Novedades: ' + novedades, 'Warning', 10000);
                    }
                } else if (resp.FacturarGuiasResult.Message.TypeEnum === 1) {
                    const transNum = resp.FacturarGuiasResult.Message.TransactionNumber;
                    notify('Guias Facturadas Exitosamente!. No. Transaccion: ' + transNum, 'Success', 3000);
                    this.dataSource = [];
                    this.guiasTxt = '';
                    this.observacionTxt = '';
                    this.popupVisibleFGB = false;
                }
            });
        });
    }

    MarcarNoFacturable() {
        this.pregunta = 'Realmente desea marcar como NO Facturable las guias seleccionadas?';
        this.operacion = OperacionesAConfirmar.MarcarNoFacturable;
        this.popupVisibleConfirm = true;
    }

    PasarGuiasSelec() {
        if (this.gridGuias.selectedRowKeys.length === 0) {
            notify('No hay guias seleccionadas para pasar!', 'Warning', 5000);
            return;
        }
        this.gs.guiasSeleccionadas.next(this.gridGuias.selectedRowKeys);
        let guiasSeleccionadosValor = 0;
        this.gridGuias.selectedRowKeys.forEach((valor: any) => {
            guiasSeleccionadosValor += parseInt(valor.Total);
        });
        this.gs.guiasSeleccionadosValor.next(guiasSeleccionadosValor);
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }

    onHiddenPopup() {
        this.guiasTxt = '';
        this.observacionTxt = '';
    }
}
