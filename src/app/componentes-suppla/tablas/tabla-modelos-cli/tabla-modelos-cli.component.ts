import { TipoEsquemaEnum } from 'app/enumeraciones/tipo-esquema.enum';
import { ArticulosService } from 'app/servicios/articulos.service';
import { ModelosService } from 'app/servicios/modelos.service';
import { TarifasService } from 'app/servicios/tarifas.service';
import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Subject } from 'rxjs';

@Component({
    selector: 'tabla-modelos-cli',
    templateUrl: './tabla-modelos-cli.component.html',
    styleUrls: ['./tabla-modelos-cli.component.scss']
})
export class TablaModelosCliComponent implements OnInit {

    @Input() modelosLista: Array<any>;
    _selectedRow: number;
    withAnimationVisible = false;
    idOnMouseOver: string;
    modDescripcion: string;

    //#region observables request tarifa
    tipoEsquema: TipoEsquemaEnum;
    esquemaCodigoSelected: string;
    clienteCodigoSelected: string;
    proveedorCodigoSelected: string;
    modeloCodigoSelected: string;
    //#endregion

    constructor(public ms: ModelosService, public as: ArticulosService,
        public ts: TarifasService) { }

    ngOnInit() {
        this.ms.selectedRowCli.subscribe(val => this._selectedRow = val);
        this.ms.selectedRowCli.next(-1);
        // subscripcion observables request tarifa
        this.ts.tipoEsquema.subscribe(data => this.tipoEsquema = data);
        this.ts.esquemaCliCodigoSelected.subscribe(data => this.esquemaCodigoSelected = data);
        this.ts.clienteCodigoSelected.subscribe(data => this.clienteCodigoSelected = data);
        this.ts.modeloCliCodigoSelected.subscribe(data => this.modeloCodigoSelected = data);
    }

    ObtenerArticulosPorModelo(index) {
        this.as.selectedRowCli.next(-1)
        this.ms.selectedRowCli.next(index);
        this.ts.tarifasCliList.next([]);
        this.ts.articuloCliCodigoSelected.next('');
        this.ts.modeloCliCodigoSelected.next(this.modelosLista[index].Codigo)
        this.as.articulosCliLista.next(this.modelosLista[index].Articulos);
    }

    ExportarTarifasAsoc(mod, index) {
        this.ObtenerArticulosPorModelo(index);
        let filtro = {
            EsquemaCodigo: this.esquemaCodigoSelected,
            TipoEsquema: TipoEsquemaEnum.Cliente,
            ClienteCodigo: this.clienteCodigoSelected,
            ProveedorCodigo: "",
            ModeloCodigo: this.modeloCodigoSelected
        };
        this.ms.ObtenerTarifasPorModelo(filtro).then((resp) => {
            // console.log(resp);
            if (resp.ObtenerTarifasPorModeloResult.TypeEnum == "1") {
                notify("El proceso de exportación ha sido enviado a la cola de tareas, revise el listado de descargas!", "Success", 5000);
            } else {
                notify("Error al exportar tarifas por modelo", "Error", 6000);
            }
        });
    }

    toggleWithAnimation(mod) {
        if (mod != undefined) {
            this.idOnMouseOver = '#' + mod.Codigo;
            this.modDescripcion = mod.Descripcion;
            this.withAnimationVisible = !this.withAnimationVisible;
        }
    }
}