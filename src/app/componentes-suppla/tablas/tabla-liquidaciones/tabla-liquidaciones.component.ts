import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { TC_BASE_URL, PORCENTAJE_ALTO, PORCENTAJE_ANCHO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';

@Component({
    selector: 'tabla-liquidaciones',
    templateUrl: './tabla-liquidaciones.component.html',
    styleUrls: ['./tabla-liquidaciones.component.scss']
})
export class TablaLiquidacionesComponent implements OnInit {

    @ViewChild('gridLiquidaciones') gridLiquidaciones;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    filaActual: any;
    mostrarFiltro = false;
    heightTabla: number;
    popupVisible = false;
    tcUrl = TC_BASE_URL;

    constructor(public ls: LiquidacionesService, public us: Utils) { }

    ngOnInit() {
        this.ls.refrescarLiquidacionesDet.subscribe((refrescarDet) => {
            if (refrescarDet) {
                this.Detalle(this.filaActual);
            }
        });
        this.ls.buscandoLiquidaciones.subscribe(val => {
            if (val) {
                this.gridLiquidaciones.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridLiquidaciones.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    Detalle(row) {
        if (row && row[0]) {
            this.ls.buscandoLiquidacionesDet.next(true);
            this.filaActual = row;
            // console.log("proveedor selected");
            // console.log(row[0].ProveedorCodigo);
            this.ObtenerEstadoLiquidacion(row[0].Estado);
            this.ls.detalleLiquidacionNum.next(row[0].Numero);
            this.ls.detalleLiquidacionId.next(row[0].LiquidacionId);
            this.ls.detalleLiquidacionProvSelected.next(row[0].ProveedorCodigo);
            this.ls.detalleLiquidacionProvSelectedNombre.next(row[0].ProveedorNombre);
            this.ls.DetalleLiquidacion(row[0].Numero).then(det => {
                // console.log('det liqs', det)
                if (det) {
                    if (det.length === 0) {
                        notify('No hay detalle disponible para esta liquidación!', 'Info', 2000);
                        this.ls.totalLiquidacionesDet.next(0);
                        this.ls.detalleLiquidacion.next([]);
                        this.ls.buscandoLiquidacionesDet.next(false);
                        return;
                    }
                    let total = 0;
                    det.DetallesLiquidacionDto.forEach(dt => {
                        total += parseInt(dt.Total);
                    });
                    this.ls.totalLiquidacionesDet.next(total);
                    this.ls.detalleLiquidacion.next(det);
                    this.ls.buscandoLiquidacionesDet.next(false);
                    this.popupVisible = true;
                }
            });
        }
    }

    ObtenerEstadoLiquidacion(estado: string) {
        switch (estado) {
            case EstadosLiquidacionEnum.Abierto.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Abierto);
                break;
            case EstadosLiquidacionEnum.Anulado.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Anulado);
                break;
            case EstadosLiquidacionEnum.Cerrado.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Cerrado);
                break;
            case EstadosLiquidacionEnum.Liquidado.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Liquidado);
                break;
            case EstadosLiquidacionEnum.Preliquidado.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Preliquidado);
                break;
            case EstadosLiquidacionEnum.Verificado.toString():
                this.ls.estadoLiquidacion.next(EstadosLiquidacionEnum.Verificado);
                break;

            default:
                break;
        }
    }

    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / PORCENTAJE_ALTO;
        this.widthTabla = window.screen.width / PORCENTAJE_ANCHO;
    }

    onCellPrepared(e) {
        if (e.rowType == 'header')
            e.cellElement.css('background-color', '#edf5ff');
    }
}