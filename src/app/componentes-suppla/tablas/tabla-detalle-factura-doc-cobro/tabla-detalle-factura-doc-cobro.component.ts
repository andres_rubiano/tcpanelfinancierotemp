import { PopupAccionConfirmacionComponent } from 'app/componentes-suppla/popups/accion-confirmacion/accion-confirmacion.component';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { EstadosLiquidacionEnum } from 'app/enumeraciones/estados-liquidacion.enum';
import { FiltroCostosInterface } from 'app/interfaces/filtro-costos.interface';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { TC_BASE_URL, PORCENTAJE_ALTO } from 'app/config/config';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CostosService } from 'app/servicios/costos.service';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { log } from 'util';

@Component({
    selector: 'tabla-detalle-factura-doc-cobro',
    templateUrl: './tabla-detalle-factura-doc-cobro.component.html',
    styleUrls: ['./tabla-detalle-factura-doc-cobro.component.scss']
})
export class TablaDetalleFacturaDocCobroComponent implements OnInit {

    @ViewChild('gridFacturaDetDocCobro') gridFacturaDetDocCobro;
    @Input() dataSource: Array<any>;
    @Input() widthTabla: number;
    heightTabla: number;

    constructor(public us: Utils) { }

    ngOnInit() {
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 1.55;
        this.widthTabla = window.screen.width / 2.15;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') {
            e.cellElement.css('background-color', '#edf5ff');
        }
    }
}
