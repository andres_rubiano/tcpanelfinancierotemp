import { TablaDetalleLiquidacionesComponent } from 'app/componentes-suppla/tablas/tabla-detalle-liquidaciones/tabla-detalle-liquidaciones.component';
import { Component, OnInit, Input, ViewChild, ViewChildren, Output, EventEmitter } from '@angular/core';
import { ManifiestosService } from 'app/servicios/manifiestos.service';
import { CostosService } from 'app/servicios/costos.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { TC_BASE_URL } from 'app/config/config';
import notify from 'devextreme/ui/notify';
import { Utils } from 'app/tools/utils';
import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { DetalleViajeService } from 'app/servicios/detalle-viaje.service';

@Component({
    selector: 'tabla-costos-proveedor',
    templateUrl: './tabla-costos-proveedor.component.html',
    styleUrls: ['./tabla-costos-proveedor.component.scss']
})
export class TablaCostosProveedorComponent implements OnInit {

    @ViewChild('gridCostosProv') gridCostos;
    @Input() dataSource: Array<any>;
    @Input() liqNumero: string;
    @Output() costosProvSelec: EventEmitter<any[]> = new EventEmitter();
    mostrarFiltro = false;
    heightTabla: number;
    widthTabla: number;
    popupVisible = false;
    popupVisibleConfirm: boolean;
    pregunta: string;
    operacion: OperacionesAConfirmar;
    tcUrl = TC_BASE_URL;

    constructor(public cs: CostosService, public ms: ManifiestosService, public us: Utils,
        public tdlc: TablaDetalleLiquidacionesComponent, public dvs: DetalleViajeService) { }

    ngOnInit() {
        this.cs.buscandoCostosProv.subscribe(val => {
            if (val) {
                this.gridCostos.instance.beginCustomLoading('Cargando...');
            } else {
                this.gridCostos.instance.endCustomLoading();
            }
        });
        this.calculeHeightTabla();
    }

    Respuesta(e) {
        if (!e.respuesta) {
            this.popupVisibleConfirm = false;
        } else {
            switch (e.tipoOpe) {
                case OperacionesAConfirmar.AddCostosViaje:
                    this.popupVisibleConfirm = false;
                    this.costosProvSelec.emit(this.gridCostos.selectedRowKeys);
                    this.tdlc.popupVisible = false;
                    this.tdlc.Respuesta(e);
                    break;
                default:
                    this.popupVisibleConfirm = false;
                    break;
            }
        }
    }

    AdicionarCostos() {
        if (this.gridCostos.selectedRowKeys.length === 0) {
            notify('Debe seleccionar al menos un costo!', 'Warning', 5000);
            return;
        }

        this.pregunta = 'Realmente desea agregar costos a la liquidación #' + this.liqNumero + '?';
        this.operacion = OperacionesAConfirmar.AddCostosViaje;
        this.popupVisibleConfirm = true;

    }

    Detalle(viajeNum) {
        // console.log("viajeNum");
        // console.log(viajeNum.data.Viaje);
        this.ms.DetalleViaje(viajeNum.data.Viaje).then(det => {
            if (det) {
                this.dvs.detalle = det;
                this.popupVisible = true;
            }
        });
    }
    calculateCellValueDetalle(data) {
        return data;
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 2.5;
        this.widthTabla = window.screen.width / 1.27;
    }

    onCellPrepared(e) {
        if (e.rowType === 'header') { e.cellElement.css('background-color', '#edf5ff') }
    }
}
