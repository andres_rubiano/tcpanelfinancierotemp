import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'tabla-detalle-remesas-ingresos',
    templateUrl: './tabla-detalle-remesas-ingresos.component.html',
    styleUrls: ['./tabla-detalle-remesas-ingresos.component.scss']
})
export class TablaDetalleRemesasIngresosComponent implements OnInit {

    @Input() ingresosLista:any;
    heightTabla: number;

    constructor() { }

    ngOnInit() { 
        this.calculeHeightTabla();
    }

    calculeHeightTabla() {
        this.heightTabla = window.screen.height / 4.2;
    }    
    
}