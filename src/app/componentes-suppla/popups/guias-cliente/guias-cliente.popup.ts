import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { GuiasService } from 'app/servicios/guias.service';
import { FacturasService } from 'app/servicios/facturas.service';

@Component({
    selector: 'guias-cliente-popup',
    templateUrl: './guias-cliente.popup.html',
    styleUrls: ['./guias-cliente.popup.scss']
})
export class GuiasClientePopupComponent implements OnInit {

    @Input() factNumero: string;
    @Input() clienteCod: Array<string>;
    @Input() clienteNombre: Array<string>;

    guiasLista: Array<any>;

    constructor(public gs: GuiasService, public fs: FacturasService) { }

    ngOnInit() {
        this.gs.guiasCliente.subscribe(list => {
            if (list.length === 0) {
                notify('No se encontraron guias para estos criterios de busqueda!', 'Info', 2000);
                return;
            }
            this.guiasLista = list;
        });
    }

    GetGuiasSelec(e) {
        const guias = [];
        e.forEach(guia => {
            guias.push(guia.Numero.toString());
        });

        this.fs.AgregarGuiasAFactura(this.factNumero, guias).then((res) => {
            // console.log(res);
            if (res.AdicionarGuiaAFacturaResult.Message.TypeEnum === 1) {
                notify('Guias agregadas exitosamente. No. Transaccion: '
                    + res.AdicionarGuiaAFacturaResult.Message.TransactionNumber, 'Success', 3000);
                this.fs.refrescarDetalle.next(true);
            } else if (res.AdicionarGuiaAFacturaResult.Message.TypeEnum === 3) {
                notify('Error adicionando guias. Descripción: '
                    + res.AdicionarGuiaAFacturaResult.Message.Message, 'Error', 6000);
            }
        });
    }
}
