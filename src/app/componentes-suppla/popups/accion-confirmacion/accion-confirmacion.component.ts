import { OperacionesAConfirmar } from 'app/enumeraciones/operaciones-confirmar.enum';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'popup-accion-confirmacion',
    templateUrl: './accion-confirmacion.component.html',
    styleUrls: ['./accion-confirmacion.component.scss']
})
export class PopupAccionConfirmacionComponent implements OnInit {

    @Input() pregunta: string;
    @Input() operacion: OperacionesAConfirmar;

    @Output() respuesta: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() { }

    Cancelar() {
        this.respuesta.emit({respuesta: false, tipoOpe: this.operacion});
    }

    OK() {
        this.respuesta.emit({respuesta: true, tipoOpe: this.operacion});
    }

}