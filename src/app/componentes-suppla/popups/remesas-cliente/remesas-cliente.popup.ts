import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { FacturasService } from 'app/servicios/facturas.service';
import { RemesasService } from 'app/servicios/remesas.service';

@Component({
    selector: 'remesas-cliente-popup',
    templateUrl: './remesas-cliente.popup.html',
    styleUrls: ['./remesas-cliente.popup.scss']
})
export class RemesasClientePopupComponent implements OnInit {

    @Input() factNumero: string;
    @Input() clienteCod: Array<string>;
    @Input() clienteNombre: Array<string>;

    remesasLista: Array<any>;

    constructor(public rs: RemesasService, public fs: FacturasService) { }

    ngOnInit() {
        this.rs.remesasCliente.subscribe(list => {
            if (list.length === 0) {
                notify('No se encontraron remesas para estos criterios de busqueda!', 'Info', 2000);
                return;
            }
            this.remesasLista = list;
        });
    }

    GetRemesasSelec(e) {
        const remesas = [];
        e.forEach(remesa => {
            remesas.push(remesa.Numero.toString());
        });

        this.fs.AgregarRemesasAFactura(this.factNumero, remesas).then((res) => {
            // console.log(res);
            if (res.AdicionarRemesaAFacturaResult.Message.TypeEnum === 1) {
                notify('Remesas agregadas exitosamente. No. Transaccion: '
                    + res.AdicionarRemesaAFacturaResult.Message.TransactionNumber, 'Success', 3000);
                this.fs.refrescarDetalle.next(true);
            } else if (res.AdicionarRemesaAFacturaResult.Message.TypeEnum === 3) {
                notify('Error adicionando remesas. Descripción: '
                    + res.AdicionarRemesaAFacturaResult.Message.Message, 'Error', 6000);
            }
        });
    }
}
