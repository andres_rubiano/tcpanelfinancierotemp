import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import notify from 'devextreme/ui/notify';
import { AuthenticacionService } from '../../../servicios/autenticacion.service';

@Component({
    selector: 'crear-nota-prov-popup',
    templateUrl: './crear-nota-prov.popup.html',
    styleUrls: ['./crear-nota-prov.popup.scss']
})
export class CrearNotaProvPopupComponent {

    @Input() info: any;
    @Input() liqNum: string;
    @Output() notaCreada: EventEmitter<boolean> = new EventEmitter();

    constructor(public ls: LiquidacionesService, private authSrvc: AuthenticacionService) {
    }

    CrearNota(data) {
        const dataLiq = {
            liquidacionNumero: this.liqNum,
            notaProveedorDto: {
                ProveedorCodigo: this.info.DTO.ProveedorCodigo,
                ProveedorNombre: this.info.DTO.ProveedorNombre,
                ViajeNumero: this.info.DTO.ViajeNumero,
                Vehiculo: this.info.DTO.Vehiculo,
                Observaciones: data.obs,
                Detalle: data.modificados
            },
            codigoUsuario: this.authSrvc.GetCurrentUser()
        };

        this.ls.CrearNotaProveedor(dataLiq).then(resp => {
            // console.log('resp', resp);
            if (resp.TypeEnum === 3) {
                notify('Error: ' + resp.Message, 'Error', 7000);
                return;
            } else if (resp.TypeEnum === 1) {
                notify('Nota creada exitosamente: No. ' + resp.TransactionNumber, 'Success', 2000);
                this.notaCreada.emit(true);
            }
        });
    }

}
