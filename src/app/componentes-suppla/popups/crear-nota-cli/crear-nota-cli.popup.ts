import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { log } from 'util';
import { FacturasService } from 'app/servicios/facturas.service';
import { AuthenticacionService } from '../../../servicios/autenticacion.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'crear-nota-cli-popup',
    templateUrl: './crear-nota-cli.popup.html',
    styleUrls: ['./crear-nota-cli.popup.scss']
})
export class CrearNotaCliPopupComponent implements OnInit {

    @Input() info: any;
    @Input() factNum: string;
    @Output() notaCreada: EventEmitter<boolean> = new EventEmitter();

    constructor(private fs: FacturasService, private authSrvc: AuthenticacionService) { }

    ngOnInit() {
    }

    CrearNota(data) {
        const detalle = [];
        data.modificados.forEach(row => {
            const rowTemp = {
                Origen: row.Origen,
                Referencia: row.Referencia,
                ArticuloCodigo: row.ArticuloCodigo,
                IngresoTotal: row.IngresoTotal,
                Valor: row.Valor
            };
            detalle.push(rowTemp);
        });
        const dataFact = {
            notaCliente: {
                FacturaNumero: this.factNum,
                Observaciones: data.obs,
                Detalle: detalle
            },
            codigoUsuario: this.authSrvc.GetCurrentUser()
        };

        this.fs.CrearNotaCliente(dataFact).then(resp => {
            // console.log('resp', resp);
            if (resp.TypeEnum === 3) {
                notify('Error: ' + resp.Message, 'Error', 7000);
                return;
            } else if (resp.TypeEnum === 1) {
                notify('Nota creada exitosamente: No. ' + resp.TransactionNumber, 'Success', 2000);
                this.notaCreada.emit(true);
            }
        });
    }
}
