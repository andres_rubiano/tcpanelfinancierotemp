import { LiquidacionesService } from 'app/servicios/liquidaciones.service';
import { CostosService } from 'app/servicios/costos.service';
import { Component, OnInit, Input } from '@angular/core';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'costos-proveedor-popup',
    templateUrl: './costos-proveedor.popup.html',
    styleUrls: ['./costos-proveedor.popup.scss']
})
export class CostosProveedorPopupComponent implements OnInit {

    @Input() liqNumero: string;
    @Input() provCod: Array<string>;
    @Input() provNombre: Array<string>;

    proveedorCodigo: string;
    costosLista: Array<any>;

    constructor(public cs: CostosService, public ls: LiquidacionesService) { }

    ngOnInit() {
        this.cs.costosPorProveedor.subscribe(list => {
            if (list.length === 0) {
                notify(`No se encontraron costos para el provedor "${this.provNombre}" en este rango de fechas, 
                    modifique el rango para buscar mas costos!`, 'Info', 3000);
            }
            this.costosLista = list
        });
    }

    GetCostosSelec(e) {
        // console.log("Event emitter costos");
        // console.log(e);

        // console.log('costos seleccionados para agregar');
        // console.log(e);
        const costosIds = [];
        e.forEach(costo => {
            costosIds.push(costo.CostoId);
        });
        this.cs.AdicionarCostosViaje(this.liqNumero, costosIds).then((res) => {
            // console.log(res);
            if (res.TypeEnum === 1) {
                notify('Costos agregados exitosamente!', 'Success', 3000);
                this.ls.refrescarLiquidacionesDet.next(true);
            } else if (res.TypeEnum === 3) {
                notify('Error adicionando costos. Descripción: ' + res.Message, 'Error', 6000);
            }
        });
    }
}
