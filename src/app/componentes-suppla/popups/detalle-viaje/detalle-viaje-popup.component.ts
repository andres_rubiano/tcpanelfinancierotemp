import { Component } from '@angular/core';
import { Utils } from 'app/tools/utils';
import { DetalleViajeService } from 'app/servicios/detalle-viaje.service';

@Component({
    selector: 'detalle-viaje-popup',
    templateUrl: './detalle-viaje-popup.component.html',
    styleUrls: ['./detalle-viaje-popup.component.scss']
})
export class DetalleViajePopupComponent{

    constructor(public us:Utils, public dvs:DetalleViajeService) { }


}