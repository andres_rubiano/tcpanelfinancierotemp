export enum EstadosFacturacionEnum {
    Anulada = <any>'Anulada', // nada
    Definitiva = <any>'Definitiva', // solo habilita impresion
    PreFacturada = <any>'Pre-Facturada' // agregar, remover, imprimir, anular, definitiva
}
