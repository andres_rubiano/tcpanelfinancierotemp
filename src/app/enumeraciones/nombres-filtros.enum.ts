export enum NombresFiltrosEnum{
    EmpaqueOficialCodigo = <any>'EmpaqueOficialCodigo',
    CiudadDestinoCodigo = <any>'CiudadDestinoCodigo',
    CiudadOrigenCodigo = <any>'CiudadOrigenCodigo',
    TipoVehiculoCodigo = <any>'TipoVehiculoCodigo',
    TipoRutaCodigo = <any>'TipoRutaCodigo',
    FechaInicial = <any>'FechaInicial',
    RangoMinimo = <any>'RangoMinimo',
    RangoMaximo = <any>'RangoMaximo',
    FechaFinal = <any>'FechaFinal',
    Estado = <any>'Estado',
    Valor = <any>'Valor'
}