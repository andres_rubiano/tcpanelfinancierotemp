export enum OperacionesAConfirmar {
    MarcarNoFacturable = <any>'MarcarNoFacturable',
    AnularLiquidacion = <any>'AnularLiquidacion',
    AnularFactura = <any>'AnularFactura',
    CerrarLiquidacion = <any>'CerrarLiquidacion',
    AddCostosViaje = <any>'AddCostosViaje',
    AddGuias = <any>'AddGuias',
    AddRemesas = <any>'AddRemesas',
    AddNota = <any>'AddNota',
    RemoverNota = <any>'RemoverNota',
    RemoverCostos = <any>'RemoverCostos',
    RemoverGuias = <any>'RemoverGuias',
    RemoverRemesas = <any>'RemoverRemesas',
    AddCostos = <any>'AddCostos',
    Facturar = <any>'Facturar',
    VolverDefinitiva = <any>'VolverDefinitiva'
}
