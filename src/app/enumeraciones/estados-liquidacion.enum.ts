export enum EstadosLiquidacionEnum {
    Verificado = <any>'Verificado', // solo habilita impresion y cerrar liq
    Liquidado = <any>'Liquidado', // solo habilita impresion
    Preliquidado = <any>'Preliquidado', // habilita todo menos Anular
    Cerrado = <any>'Cerrado', // solo habilita impresion
    Anulado = <any>'Anulado', // deshabilita todo
    Abierto = <any>'Abierto' // habilita todo
}
